###############################################################################################
# $Id$
###############################################################################################
#
# This file contains the version of Civil. For each release it should be updated to reflect the
# new version along with any subversions etc.
#
###############################################################################################

version = '0.84'


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
