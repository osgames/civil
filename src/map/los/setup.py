from distutils.core import setup, Extension

module1 = Extension('ccivil', sources = ['ccivil.c'])

setup(name='ccivil',
      version = '1.0',
      description = 'Optimized C LOS functions for Civil',
      ext_modules = [module1])
