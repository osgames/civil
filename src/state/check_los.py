###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
from pygame.locals import *

import scenario
import properties
import state
import own_unit

class CheckLos (state.State):
    """
    This class is a state that takes care of letting the user check line of sight (los) for the
    current unit to various positions on the map. A line is drawn from the unit position to the
    current mouse position. This state will track the mouse.

    Clicking a mouse button or pressing 'Escape' terminates the LOS-checking.
    """

    # define a shared base cursor
    cursor = None

    def __init__ (self):
        """Initializes the instance. Sets default values for all needed members."""

        # call superclass constructor
        state.State.__init__ (self)

        # do we have a cursor already loaded? 
        if not CheckLos.cursor:
            # nope, so load it. First get the filenames
            datafile = properties.state_check_los_cursor_data
            maskfile = properties.state_check_los_cursor_mask

            # now load it
            CheckLos.cursor = pygame.cursors.load_xbm ( datafile, maskfile )

        # set our own cursor cursor
        pygame.mouse.set_cursor ( *CheckLos.cursor )
         
        # set defaults
        self.name = "check_los"
        
        # set the keymap too
        self.keymap [ (K_ESCAPE, KMOD_NONE) ] = self.cancel

        # we want mouse motion events
        self.wantmousemotion = 1

        # define the help text too
        self.helptext = [ "Line of sight test",
                          " ",
                          "arrow keys - scroll map",
                          "F1 - show this help text",
                          "F10 - toggle fullscreen mode",
                          "F12 - save a screenshot",
                          "esc - cancel the los test" ]
        

    def handleMouseMotion (self, event):
        """This method handles mouse motion events. Checks the current unit and mouse positions and
        traces a LOS to the current mouse position. If the position can be seen by the unit then a
        green line is drawn in the 'los' playfield layer, and if not, then a red line is drawn.

        Returns None to indicate that a new state should not be activated."""
        # get the selected unit
        unit = self.getSelectedUnit ()
        
        # get the selected unit's position
        unitx, unity = unit.getPosition ()

        # the click is on the main playfield, so get the clicked coordinate
        globalx, globaly = self.toGlobal ( event.pos )

        # check visibility
        last_seen = scenario.map.traceLos (start=(unitx, unity), end=(globalx,globaly),
                                           max_distance=unit.sightRange, debug=1)

        # did we see it?
        if last_seen == (globalx,globaly):
            # yep, we see all the way
            sees = 1
        else:
            sees = 0

        print "CheckLos.handleMouseMotion: sees:", sees


    def handleLeftMousePressed (self, event):
        """Handles an event when the left mouse button is pressed. This will immediately cancel the
        state and return to the OwnUnit state."""
        return self.cancel ()


    def handleMidMousePressed (self, event):
        """Handles an event when the middle mouse button is pressed. This will immediately cancel the
        state and return to the OwnUnit state."""
        return self.cancel ()
    

    def handleRightMousePressed (self, event):
        """Handles an event when the right mouse button is pressed. This will immediately cancel the
        state and return to the OwnUnit state."""
        return self.cancel ()
    

    def cancel (self):
        """Callback triggered when the user presses the 'escape' key. Cancels the moving and makes a
        OwnUnit state active again."""
        # return a new state
        return own_unit.OwnUnit ()

        
##     def handleLeftMousePressed (self, event):
##         """Method for handling a mouse pressed. Checks wether the mouse was clicked in the map or in
##         the panel. Only clicks in the map will do anything useful, all other clicks are
##         ignored.

##         If the click is in the map then LOS to that position is checked for the given unit.
##         """

##         # terminate the state
##         return self.cancel ()

    
##         # get the selected unit
##         unit = self.getSelectedUnit ()

##         # the click is on the main playfield, so get the clicked coordinate
##         globalx, globaly = self.toGlobal ( event.pos )

##         # get unit position
##         unitx, unity = unit.getPosition ()

##         # check visibility
##         last_seen = scenario.map.traceLos (start=unit.getPosition(), end=(globalx,globaly),
##                                            max_distance=unit.sightRange, debug=1)

##         print "CheckLos.handleLeftMousePressed: los from %d,%d to %d,%d" % (unitx,unity,globalx,globaly)

##         # did we see it?
##         if last_seen == (globalx,globaly):
##             # yep, we see all the way
##             print "    visibility is 1"

##             # reset the coordinate to None to make drawing easier
##             last_seen = None

##         else:
##             print "    visibility is 0"
##             print "    last visible position: (%d, %d)" % ( last_seen[0], last_seen[1] )

##         hexCoord  = scenario.map.pointToHex2 ((globalx,globaly))
##         hexCenter = scenario.map.hexToPoint (hexCoord)
##         hex=scenario.map.getHex(hexCoord[0],hexCoord[1])
        
##         print "    hex of point (%d, %d) is (%d, %d)" % (globalx, globaly, hexCoord[0], hexCoord[1])
##         print "    terrain type at point is %s" % scenario.map.getTerrain((globalx,globaly)).getType()
##         tercodes=''
##         for triang in hex.template.terrains:
##             tercodes += triang.getCode()
##         print "    iconId is %d  terrains: %s" %(hex.getIconId(), tercodes)
##         print "    height of hex is %d" % scenario.map.hexHeight( hexCoord[0], hexCoord[1] )

##         # try to find the los debugging layer. we may not have such a layer if we're not debugging
##         layer = scenario.playfield.getLayer ( "los_debug" )

##         # did we get it?
##         if layer == None:
##             # no such layer, not debugging
##             return None

##         # assign the los lines. we may or may not have a mid point
##         layer.setLosLines ( unit.getPosition(), (globalx, globaly), last_seen )

##         # repaint the playfield now that we have changed
##         scenario.playfield.needRepaint ()

##         # do not change state, remain in this until escaped
##         return None
 
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
