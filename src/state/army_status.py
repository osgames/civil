###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
from pygame.locals       import *

import scenario
import properties
import state

class ArmyStatus (state.State):
    """
    This class is a state that is used to drive the army status dialog. This class just brings the
    layer to the top and takes care of relaying mouse presses to it. It also close the dialog (hides
    the layer) when needed.
    
    This state manages events, and forwards all clicks to the playfield layer that paints the
    browser. When closed this state resumes the previous state.
    """

    def __init__ (self, caller):
        """Initializes the instance. Sets default values for all needed member."""

        # call superclass constructor
        state.State.__init__ (self)

        # set default cursor
        self.setDefaultCursor ()
       
        # set defaults
        self.name = "army_status"
        
        # store the calling state
        self.caller = caller
         
        # set the keymap too
        self.keymap [ (K_ESCAPE, KMOD_NONE) ] = self.close

        # find the layer
        self.army_status_layer = scenario.playfield.getLayer ( "army_status" )

        # update the shown info
        self.army_status_layer.updateUnits ()
        
        # and make it visible
        scenario.playfield.setVisible ( self.army_status_layer )


    def handleLeftMousePressed (self, event):
        """Handles a click with the left mouse button. This method checks wether the 'Close' button
        was clicked, and if it was then closes the help browser. Lets the layer handle the click if
        it wasn't in the button. Restores the previous state when closed."""

        # get event position
        xev, yev = event.pos

        # was ok pressed? ask the layer about it
        if self.army_status_layer.isOkPressed ( xev, yev ):
            # yep, closed, so close and return whatever state we had before
            return self.close ()

        # no 'Close' clicked, see if there's something else that needs to be take care of
        if self.army_status_layer.handleLeftMousePressed ( xev, yev ):
            # we need an update now of the playfield
            scenario.playfield.needRepaint ()
         

    def close (self):
        """Closes the state without doing anything. Hides the dialog and repaints the
        playfield. This method is used from the keybindings."""
        
        # find the layer and hide it
        scenario.playfield.setVisible ( self.army_status_layer, 0 )

        # return the previous state
        return self.caller


        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
