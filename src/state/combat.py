###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.cursors
import scenario
import properties
import net.connection
from pygame.locals      import *

import state

class Combat (state.State):
    """
    This class is a baseclass for a state that manages combat. It contains a few methods that are
    shared among combat classes. It should never be instantiated directly. It defines no keymap,
    help texts or cursors.
    """


    def __init__ (self):
        """Initializes the instance. Does nothing useful."""
        # call superclass constructor
        state.State.__init__ (self)


    def findTarget (self, x, y):
        """Checks for an enemy unit at the given position or within a short distance from the position and
       returns its id. If no enemy unit was found at the position then -1 is returned."""

        # get the units
        units = scenario.info.units
        
        # loop over all units
        for unit in units.values ():
            # get the unit's position
            (unitx, unity) = unit.getPosition ()
            
            # calculate the square of distance from the given point to the unit
            distance = (x - unitx) ** 2 + (y - unity) ** 2

            # is the distance within picking range?
            if distance < 100:
                # is this unit an own unit or enemy unit?
                if unit.getOwner () != scenario.local_player_id:
                    # they're the target
                    return unit.getId ()

        # no unit found
        return -1
 
   
    def inRange (self, unit, target):
        """Is the target in range? """
        
        x1, y1 = unit.getPosition()
        x2, y2 = target.getPosition()

        # do a simple squared range calculation (no square roots)
        if ((x2-x1) ** 2 + (y2-y1) ** 2) > unit.getWeapon().getRange() ** 2:
            # out of range
            return 0

        # we're in range
        return 1


        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
