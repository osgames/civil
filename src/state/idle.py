###############################################################################################
# $Id$
###############################################################################################

#import os
#import sys
import pygame
from pygame.locals import *

import scenario
import properties

import state

class Idle (state.State):
    """
    This class is the default state in the game. It will only activate other states depending on
    input events. It does handle some basic common functionality such as:

    * scrolling with the keyboard
    
    This state will activate the following other states:

    * enemy_unit if an enemy unit is clicked.
    * own_unit if an own unit is clicked.

    * idle is kept if no other state is activated.
    """

    def __init__ (self):
        """Initializes the instance. Sets default values for all needed members."""

        # call superclass constructor
        state.State.__init__ (self)
        
        # set default cursor
        self.setDefaultCursor ()

        # set defaults
        self.name = "idle"

        # no selected unit
        if self.clearSelectedUnits ():
            scenario.dispatcher.emit ( 'unit_selected', None )
       
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
