###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.cursors
from pygame.locals      import *
import scenario
import properties

import combat
import own_unit
import messages

from plan.assault       import Assault


class AssaultUnit (combat.Combat):
    """
    This class is a state that takes care of letting the user click on an enemy unit to be assaulted.
    This state should only be activated when an own unit is selected. When the user has clicked in
    the map a state OwnUnit is activated.
    """

    # define a shared base cursor
    cursor = None


    def __init__ (self):
        """Initializes the instance. Sets default values for all needed members."""

        # call superclass constructor
        combat.Combat.__init__ (self)

        # do we have a cursor already loaded? 
        if not AssaultUnit.cursor:
            # nope, so load it. First get the filenames
            datafile = properties.state_assault_cursor_data
            maskfile = properties.state_assault_cursor_mask

            # now load it
            AssaultUnit.cursor = pygame.cursors.load_xbm ( datafile, maskfile )

        # set our own cursor cursor
        pygame.mouse.set_cursor ( *AssaultUnit.cursor )
         
        # set defaults
        self.name = "assault_unit"
        
        # set the keymap too
        self.keymap [ (K_ESCAPE, KMOD_NONE) ] = self.cancel

        # define the help text too
        self.helptext = [ "Assault enemy unit",
                          " ",
                          "arrow keys - scroll map",
                          "F1 - show this help text",
                          "F10 - toggle fullscreen mode",
                          "F12 - save a screenshot",
                          "esc - cancel the order" ]


    def cancel (self):
        """Callback triggered when the user presses the 'escape' key. Cancels the moving and makes a
        OwnUnit state active again."""
        # return a new state
        return own_unit.OwnUnit ()

                  
         
    def handleLeftMousePressed (self, event):
        """Method for handling a mouse pressed. Checks wether the mouse was clicked in the map or in
        the panel. Only clicks in the map will do anything useful, all other clicks are
        ignore. Accepts the movement target if a click is made in the map and return a new 'OwnUnit'
        if the unit will be moved.
        
        Sends off the command to the server if we're the client and enqueues the same command to the
        server command queue if we're the server.
        """

        # no changed units yet
        changed = 0

        # get event position
        x, y = event.pos

        # the click is on the main playfield, so get the clicked coordinate
        globalx, globaly = self.toGlobal ( (x, y) )

        # find the id of the targetted unit (if any)
        targetid = self.findTarget (globalx,globaly)

        # did we find any unit?
        if targetid == -1:
            # no target unit in clicked position, let player try again
            return self
        
        # loop over all selected unit
        for unit in self.getSelectedUnits ():
            # check morale, fatigue
            if unit.getMorale ().checkAssault () == 0 or unit.getFatigue ().checkAssault () == 0:
                # either the morale is too low or the fatigue is too high, we won't do combat
                scenario.messages.add ( '%s can not assault' % unit.getName (), messages.ERROR )
                continue

            # can the unit assault?
            if not unit.getMode ().canAssault ():
                # nope, the unit mode prohibits it, next unit
                scenario.messages.add ( '%s can not assault' % unit.getName (), messages.ERROR )
                continue

            # morale and fatigue ok, create a new 'assault' plan 
            plan = Assault ( unitid=unit.getId (), targetid=targetid )

            # send off the plan to the server
            scenario.connection.send ( plan.toString () )

            # add the plan last among the unit's plans
            unit.getPlans ().append ( plan )
            
            # at least one changed unit
            changed = 1

        # any channged units?
        if changed:
            # we have changed some units
            scenario.dispatcher.emit ( 'units_changed', self.getSelectedUnits () )
 
        # the units have now got a target
        return own_unit.OwnUnit ()
 
  
##     def canAssault (self, unit, target):
##         """Checks wether 'unit' can assault 'target'. The mode, morale and fatigue must be ok for an
##         assault. If all is ok then None is returned and if skirmish is not possible then an error
##         message is returned. The message is suitable for showing to the player.

##         Note that the mode needs not be checked, as it's always ok if we get this far at all."""

##         # morale ok?

##         # fatigue ok
        
##         # all is ok
##         return None


        

        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
