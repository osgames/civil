###############################################################################################
# $Id$
###############################################################################################

import sys
import scenario
import properties
import pygame
from pygame.locals    import *

import state


class Help (state.State):
    """
    This class is a state that is used to show a simple help text on the screen. It will add a new
    layer to the playfield and use it to display a dialog with some help text. The user can click a
    button 'Ok' to return to the previous state pr press 'Escape'.
    """

    def __init__ (self, labels, caller):
        """Initializes the instance. Sets default values for all needed member."""

        # call superclass constructor
        state.State.__init__ (self)

        # set default cursor
        self.setDefaultCursor ()
       
        # set defaults
        self.name = "help_state"
        
        # store the calling state
        self.caller = caller

        
        # set the keymap too
        self.keymap = { (K_ESCAPE, KMOD_NONE): self.close }

        # build up the admin menu. this is the extra commands that can be triggered from the little
        # admin menu in the unit info window. this should ensure that we can't recursively add more
        # help windows by pressing F1 while the help window is active. we only allow screenshots to
        # be taken
        self.adminmenukeymap = [ ( 'save a screenshot', K_F12, KMOD_NONE  ) ]

        # TODO: what about the help texts? are they needed?


        # find the help layer
        self.helplayer = scenario.playfield.getLayer ( "help" )

        # set the labels it should show
        self.helplayer.setLabels ( labels )

        # and make it visible
        scenario.playfield.setVisible ( self.helplayer )


    def handleLeftMousePressed (self, event):
        """Handles a click with the left mouse button. This method checks wether the 'Ok' button was
        clicked, and if it was then terminates this state. If the button was not clicked then
        nothing will be done. Activates the previous state."""

        # get event position
        xev, yev = event.pos

        # was ok pressed?
        if not self.helplayer.isOkPressed ( xev, yev ):
            # nope, go away
            return None

        # find the layer and hide it
        scenario.playfield.setVisible ( self.helplayer, 0 )

        # return the previous state
        return self.caller


    def close (self):
        """Closes the state, i.e. returns to the previous state. """
        
        # find the layer and hide it
        scenario.playfield.setVisible ( self.helplayer, 0 )

        # return the previous state
        return self.caller



        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
