###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.cursors
import scenario
import properties
from pygame.locals           import *

from plan.move_fast import MoveFast

import move_unit
import own_unit
import messages

class MoveUnitFast (move_unit.MoveUnit):
    """
    This class is a state that takes care of letting the user click on a position in the map where a
    unit should be moved in a fast pace. This state should only be activated when an own unit is
    selected. When the user has clicked in the map and the unit has started to move a state OwnUnit
    is activated.

    A packet 'movefast' will also be sent to the server if we're the client or just added to the
    queue of data if we're the server.
    """

    # define a shared base cursor
    cursor = None


    def __init__ (self):
        """Initializes the instance. Sets default values for all needed members."""

        # call superclass constructor
        move_unit.MoveUnit.__init__ (self)
 
        # do we have a cursor already loaded? 
        if not MoveUnitFast.cursor:
            # nope, so load it. First get the filenames
            datafile = properties.state_movefast_cursor_data
            maskfile = properties.state_movefast_cursor_mask

            # now load it
            MoveUnitFast.cursor = pygame.cursors.load_xbm ( datafile, maskfile )

        # set our own cursor cursor
        pygame.mouse.set_cursor ( *MoveUnitFast.cursor )
        
        # set defaults
        self.name = "move_unit_fast"

        # set the keymap too
        self.keymap [ (K_ESCAPE, KMOD_NONE) ] = self.cancel

        # define the help text too
        self.helptext = [ "Move a unit fast",
                          " ",
                          "arrow keys - scroll map",
                          "F1 - show this help text",
                          "F10 - toggle fullscreen mode",
                          "F12 - save a screenshot",
                          "esc - cancel the order" ]

        
    def __sendMovementPlan (self, unit, x, y):
        """Creates a movement plan and sends it on the socket. This is overridden from Move to
        create MoveFast plans."""
        # create a new plan
        plan = MoveFast ( unit.getId(), x, y )

        # and send it
        scenario.connection.send ( plan.toString () )

        # add the plan last among the unit's plans
        unit.getPlans ().append ( plan )


    def __canMove__(self, unit):
        """Overridden since we need canMoveFast()."""
        if not unit.getMode ().canMoveFast () or not unit.getFatigue ().checkMove ():
            return 0
        return 1
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
