###############################################################################################
# $Id$
###############################################################################################

import sys
import scenario
import properties
import pygame
from pygame.locals import *

import idle
import state


class EnemyUnit (state.State):
    """
    This class is a state that provides basic handling of enemy units. This state should be
    activated when the player clicks on an enemy unit or some other way makes an enemy unit
    active. Very little can be done with an enemy unit, apart from showing some limited information
    about it in the panel.

    This state will activate the following other states:

    * idle if the player clicks in the terrain somewhere.
    * own_unit if an own unit is clicked.

    * enemy_unit is kept if another enemy unit is clicked on.
    """

    def __init__ (self):
        """Initializes the instance. Sets default values for all needed members."""
        
        # call superclass constructor
        state.State.__init__ (self)

        # set default cursor
        self.setDefaultCursor ()
        
        # set defaults
        self.name = "enemy_unit"

        # define the help text too
        self.helptext = [ "An enemy unit is selected",
                          " ",
                          "arrow keys - scroll map",
                          "F1 - show this help text",
                          "F10 - toggle fullscreen mode",
                          "F12 - save a screenshot",
                          "Alt s - save the game" ]

      
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
