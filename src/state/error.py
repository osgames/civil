###############################################################################################
# $Id$
###############################################################################################

import sys
import scenario
import properties
import constants
import pygame
import pygame.event
from pygame.locals       import *

import state

class Error (state.State):
    """
    This class is a state that is used when the network connection to the other player has somehow
    died. It will ask the player wether an emergency save should be done or not. After the player
    has answered the question the game will exit.

    The emergency save should be just like any other save game, and can be loaded later to resume
    the game.
    
    This state will not transfer into any other state when it's done, instead the game exits.
    """

    def __init__ (self):
        """Initializes the instance. Sets default values for all needed member."""

        # call superclass constructor
        state.State.__init__ (self)

        # set default cursor
        self.setDefaultCursor ()
       
        # set defaults
        self.name = "error"

        # Call us once
        self.done = 0
        self.callMeSoon()


    def handleEvent (self, event):
        """Event handler, used only once.
        This is really a little kludge. This is only
        called for the server, and only once.""" 

        if self.done:
            return State.handleEvent(self, event)

        self.done = 1
        
        # ask wether the player wants to save the current state
        if self.askQuestion ( ['General, it seems the connection to the remote',
                               'player has been lost!',
                               ' ',
                               'Do you wish to save the game?'] ):
            # yes, perform the save
            self.save ()

        # we're no longer playing a game
        scenario.playing = constants.GAME_ENDED
 
        # store the type of end game, which as a crash
        scenario.end_game_type = constants.CRASH

        # keep this state active
        return None


    def save (self):
        """Saves the current game in an emergency save file."""
        print "Error.save: do nothing"
 
        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
