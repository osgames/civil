###############################################################################################
# $Id$
###############################################################################################

import sys
import scenario
import properties
import pygame
from pygame.locals       import *

import plan.change_combat_policy  

import state
import own_unit

class ToggleFeatures (state.State):
    """
    This class is a state that is used to drive the dialog layer ToggleFeatures. It shows the layer
    and handles the mousepress events for the dialog.
    """

    def __init__ (self, oldstate):
        """Initializes the instance. Sets default values for all needed member."""

        # call superclass constructor
        state.State.__init__ (self)

        # store the old state
        self.oldstate = oldstate

        # set default cursor
        self.setDefaultCursor ()
       
        # set defaults
        self.name = "toggle_features"
         
        # set the keymap too
        self.keymap [ (K_ESCAPE, KMOD_NONE) ] = self.close

        # find the question layer
        self.layer = scenario.playfield.getLayer ( "toggle_features" )

        # update it so that it knows what layers it should control
        self.layer.setLayers ( ( ("Map labels",          "locations" ),
                                 ("Objectives",          "objectives" ),
                                 ("Own unit symbols",    "own_units_symbols" ),
                                 ("Own unit icons",      "own_units_icons" ),
                                 ("Enemy unit symbols",  "enemy_units_symbols" ),
                                 ("Enemy unit icons",    "enemy_units_icons" ),
                                 ("Unit orders",         "unit_orders" ),
                                 ("Unit line of sight",  "unit_los" ),
                                 ("Superior commanders", "unit_commanders" ),
                                 ("Weapon ranges",       "weapon_ranges" ) ))
 

        # and make it visible
        scenario.playfield.setVisible ( self.layer )



    def handleLeftMousePressed (self, event):
        """Handles a click with the left mouse button. This method checks wether the 'Ok' button was
        clicked, and if it was then sets the new policy for all selected units. If no button was not
        clicked then nothing will be done. Activates the state OwnUnit when closed.""" 

        # get event position
        xev, yev = event.pos

        # was ok pressed? Let the layer handle the keypress
        if self.layer.isOkPressed ( xev, yev ):
            # ok was pressed, have the layer update the layer status
            self.layer.updateLayerVisibility ()
            
            # return the old state by closing the dialog
            return self.close ()

        # no 'Ok' clicked, see if there' something else that needs to be take care of
        self.layer.handleLeftMousePressed ( xev, yev )
        

    def close (self):
        """Closes the state without doing anything. Hides the dialog."""
        
        # find the layer and hide it
        scenario.playfield.setVisible ( self.layer, 0 )

        # we need an update now of the playfield
        scenario.playfield.needRepaint ()
        
        # return the previous state
        return self.oldstate


        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
