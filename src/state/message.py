###############################################################################################
# $Id$
###############################################################################################

import scenario
import pygame
import pygame.event
from pygame.locals    import *

import state

class Message (state.State):
    """
    This class is a state that is used to show a simple message text on the screen. It will add a new
    layer to the playfield and use it to display a dialog with some message text. The user can click a
    button 'Ok' to return to the previous state pr press 'Escape' or 'Enter' to achive the same
    result. The kay F12 can be used to save a screenshot. 

    The proper way to use this class is:

        # show the message
        message.Message ( ['Some message', 'Another line'] ).run ()

    The method 'run()' must be called, otherwise this state works just like any other state and will
    do nothing useful.

    WARNING: This state will fully grab all event handling, letting no other parts of the
    application run!
    
    """

    def __init__ (self, labels):
        """Initializes the instance. Sets default values for all needed members and shows the
        message layer that contains the graphics for the dialog.."""

        # call superclass constructor
        state.State.__init__ (self)

        # set default cursor
        self.setDefaultCursor ()
       
        # set defaults
        self.name = "message_state"
 
        # find the message layer
        self.messagelayer = scenario.playfield.getLayer ( "message" )
        
        # set the labels it should show
        self.messagelayer.setMessage ( labels )

        # and make it visible
        scenario.playfield.setVisible ( self.messagelayer )


    def run (self):
        """Runs the main loop of the message state. This loop will totally hog the event handling
        and let no other part of the application run. Is it bad? Possibly. The handled events are
        only the left mouse button clicks and the keys Escape, Enter and F12."""

        # do an initial repaint of the playfield as we've now shown the dialog. This is a little bit
        # of a hack basically ugly as sin, but it makes things so much easier.
        scenario.playfield.paint ()
        scenario.sdl.update ()

        # loop while we have SDL events to handle
        while 1:
            # get next event
            event = pygame.event.wait ()
            
            # do we have a left mouse button pressed?
            if event.type == MOUSEBUTTONDOWN and event.button == 1:
                # left mouse button pressed, get event position
                xev, yev = event.pos

                # was ok pressed?
                if self.messagelayer.isOkPressed ( xev, yev ):
                    # yep, close the dialog
                    self.close ()
                    return
            
            # do we have a key pressed pressed?
            elif event.type == KEYDOWN:
                # get the table of pressed keys
                pressed = pygame.key.get_pressed ()

                if pressed[K_RETURN] != 0 or pressed[K_ESCAPE] != 0:
                    # enter/escape pressed, close the dialog
                    self.close ()
                    return
                
                elif pressed[K_F12] != 0:
                    # F12, save screenshot
                    self.saveScreenshot ()


    def close (self):
        """Closes the dialog. Hides the dialog layer."""
        # find the layer and hide it
        scenario.playfield.setVisible ( self.messagelayer, 0 )
 

        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
