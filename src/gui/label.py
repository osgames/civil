###############################################################################################
# $Id$
###############################################################################################

import pygame
from pygame.locals import *
from widget import Widget
import widget

class Label(Widget):
    """
    This class defines a simple label with a text rendered in a certain font. 
    """

    def __init__ (self, font, text, position = (0,0), callbacks = None,
                  color = (255,255,255), background = (0,0,0)):
        """Initializes the widget. Renders the font using the passed data."""

        # firts call superclass constructor
        Widget.__init__ (self, position, callbacks)

        # create the surface
        self.surface = font.render ( text, 1, color )

        # store the needed data so that we can set the text later
        self.font = font
        self.color = color
        self.background = background

        # store our text too
        self.text = text


    def setText (self, text):
        """Sets a new text for the label. Renders the new label using the font and colors passed in
        the constrctor."""

        # create the surface
        self.surface = self.font.render( text, 1, self.color )

        # store the new text
        self.text = text
        
        # we're dirty now
        self.dirty = 1
        

    def getText (self):
        """Returns the current text of the label."""
        return self.text


    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
