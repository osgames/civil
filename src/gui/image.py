###############################################################################################
# $Id$
###############################################################################################

from widget import Widget
import widget
import pygame
import pygame.image
from pygame.locals import *

class Image (Widget):
    """
    This class defines a simple image on the screen. No interaction can be had with it. It needs a
    filename from which the surface is loaded.
    """

    # define a shared cache
    cache = {}

    def __init__ (self, filename, position = (0,0), callbacks = None, alpha = -1):
        """Initializes the widget. Loads the icons from the passed filename."""

        # firts call superclass constructor
        Widget.__init__ (self, position, callbacks)

        # do we have the wanted image in our cache?
        if Image.cache.has_key ( filename ):
            # yep, use that instead
            self.surface = Image.cache [filename]
            
        else:
            # not in memory, try to load the surfaces
            self.surface = pygame.image.load ( filename )

            # store in the cache for later use
            Image.cache [filename] = self.surface

            
        # set the color key
        self.surface.set_colorkey ( (255,255,255), RLEACCEL )

        # did we get an alpha value?
        if alpha != -1:
            # yep, convert the image to alpha
            self.surface.set_alpha ( alpha, RLEACCEL )
                
            # conver to a more efficient format
            self.surface = self.surface.convert ()
       
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
