###############################################################################################
# $Id$
###############################################################################################

import pygame
from pygame.locals import *
import scenario
import properties
import sys
import widget
from button          import Button
from image           import Image
from widget_manager  import WidgetManager
from dialog          import *

class Credits(Dialog):
    """
    This class is used as a dialog for showing credits information. The only thing this dialog
    should do is to provide a credits image and a button to close the dialog. It would server no
    specific purpose other than showing the image.
    """

    def __init__ (self):
        "Creates the dialog."
        # init superclass
        Dialog.__init__ (self, scenario.sdl)

        # set our background to a tiled image
        self.setBackground ( properties.credits_background )
       

    def createWidgets (self):
        "Creates all widgets for the dialog."

        # create the cancel button
        self.wm.register ( Button ( properties.path_dialogs + "butt-ok-moff.png",
                                    properties.path_dialogs + "butt-ok-mover.png",
                                    (750, 650), {widget.MOUSEBUTTONUP : self.ok } ) )



    def ok (self, trigger, event):
        """Callback triggered when the user clicks the 'Ok' button. Simply closes the dialog and
        returns to the main dialog."""

        # we're cancelling the dialog
        self.state = ACCEPTED
        
        return widget.DONE



#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
