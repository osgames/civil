###############################################################################################
# $Id$
###############################################################################################

import pygame
from pygame.locals import *
from widget import Widget
import widget
import properties

class TinyLabel(Widget):
    """
    This class defines a tiny label with a text rendered in a predefined font, color and
    background. 
    """

    # a shared tiny font, color and background
    font       = None
    background = None
    

    def __init__ (self, text, position = (0,0), callbacks = None, color=properties.tiny_font_color):
        """Initializes the widget. Renders the font using the passed data."""

        # firts call superclass constructor
        Widget.__init__ (self, position, callbacks)

        # do we have a font already?
        if TinyLabel.font == None:
            # no, so create it 
            TinyLabel.font = pygame.font.Font  ( properties.tiny_font_name, properties.tiny_font_size )

            # and the background 
            TinyLabel.background = properties.tiny_font_background

        # create the surface
        self.surface = TinyLabel.font.render ( text, 1, color )

        # store our text and color too
        self.text = text
        self.color = color
        

    def setText (self, text):
        """Sets a new text for the label. Renders the new label."""

        # create the surface
        self.surface = TinyLabel.font.render( text, 1, self.color )

        # store the new text
        self.text = text
        
        # we're dirty now
        self.dirty = 1
        

    def getText (self):
        """Returns the current text of the label."""
        return self.text


    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
