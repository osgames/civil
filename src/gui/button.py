###############################################################################################
# $Id$
###############################################################################################

from widget import Widget
import widget
import properties
import scenario
import pygame
import pygame.image
import pygame.mixer
from pygame.locals import *

class Button(Widget):
    """
    This class defines a simple pushbutton which can can be clicked. It needs a surfaces that
    represent the button. The surface will be loaded by the constructor from a passed filename.

    This class plays a small sound when the button is clicked.

    Dependinging on the value of propeties.button_enter_leave the button behaves graphically
    different. For a value of 1 the button changes image when the mouse enters/leaves the button,
    and for a value od 0 the image changes then the player clicks the button.
    """

    # define a shared cache
    cache = {}

    def __init__ (self, filename1, filename2, position = (0,0), callbacks = None):
        """Initializes the widget. Loads the icons from the passed filename."""

        # firts call superclass constructor
        Widget.__init__ (self, position, callbacks)

        # do we have the wanted image in our cache?
        if Button.cache.has_key ( filename1 ):
            # yep, use that instead
            self.surface_released = Button.cache [filename1]
            self.surface_outside = self.surface_released
        else:
            # not in memory, try to load the surfaces
            self.surface_released = pygame.image.load ( filename1 ).convert ()
            self.surface_outside = self.surface_released

            # setup color key
            self.__setupColorkey ( self.surface_released )

            # store in the cache for later use
            Button.cache [filename1] = self.surface_released

        # do we have the wanted image in our cache?
        if Button.cache.has_key ( filename2 ):
            # yep, use that instead
            self.surface_pressed = Button.cache [filename2]
            self.surface_inside = self.surface_pressed
           
        else:
            # not in memory, try to load the surfaces
            self.surface_pressed = pygame.image.load ( filename2 ).convert ()
            self.surface_inside = self.surface_pressed

            # setup color key
            self.__setupColorkey ( self.surface_pressed )

            # store in the cache for later use
            Button.cache [filename2] = self.surface_pressed

        # initial surface is the non-pressed one
        self.surface = self.surface_released
 
        # set our internal callbacks so that we can trap keys, depending on what mode of operation
        # we want for the button graphics
        if properties.button_enter_leave:
            # use enter/leave
            self.internal = {widget.MOUSEENTEREVENT : self.mouseEnter,
                             widget.MOUSELEAVEEVENT : self.mouseLeave }
        else:
            # use up/down
            self.internal = {widget.MOUSEBUTTONUP   : self.mouseUp,
                             widget.MOUSEBUTTONDOWN : self.mouseDown }
            

    def mouseUp (self, event):
        """Internal callback triggered when the mouse is released when it is over a button. This
        sets a new icon for the button."""
        # set the new icon
        self.surface = self.surface_released

        # play a sound
        scenario.audio.playSample ( 'button-clicked' )
        
        # we're dirty
        self.dirty = 1


    def mouseDown (self, event):
        """Internal callback triggered when the mouse is pressed when it is over a button. This
        sets a new icon for the button."""
        # set the new icon
        self.surface = self.surface_pressed

        # we're dirty
        self.dirty = 1


    def mouseEnter (self, event):
        """Internal callback triggered when the mouse enters a button. This sets a new icon for the
        button.""" 
        # set the new icon
        self.surface = self.surface_inside
        
        # we're dirty
        self.dirty = 1


    def mouseLeave (self, event):
        """Internal callback triggered when the mouse leaves a button. This sets a new icon for the
        button.""" 
        # set the new icon
        self.surface = self.surface_outside
        
        # we're dirty
        self.dirty = 1


    def __setupColorkey (self, button):
        """Sets up colorkey data if needed for the passed button."""

        # do we need colorkeying?
        if properties.button_use_colorkey:
            # yeah, set it
            button.set_colorkey ( properties.button_colorkey_color )

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
