###############################################################################################
# $Id$
###############################################################################################

import pygame
import os
import traceback
from socket         import *

import scenario
import properties
import paths

from net.connection import Connection
import gui.widget
from gui.image          import Image
from gui.dialog         import *
from gui.button         import Button
from gui.title_label    import TitleLabel
from gui.tiny_label     import TinyLabel
from gui.normal_label   import NormalLabel
from gui.editfield      import EditField
from gui.widget_manager import WidgetManager
from gui.messagebox     import Messagebox


class SetupNetwork(Dialog):
    """
    This class is used as a dialog for setting up the connection to the game server. It lets the
    player choose what kind of opponent he/she is playing against. There are three choices:

    * human opponent where we are the server
    * human opponent where we connect to a server
    * ai opponent

    For the second choice this dialog will attempt to connect to the server. That may fail if the
    remote has not yet launched the server, and in that case an error is shown and the player can
    try again. For the first and third choices nothing special will be done, some settings are
    simply stored so that the server and/or ai client can later be started.
    """

    def __init__ (self):
        "Creates the dialog."
        # init superclass
        Dialog.__init__ (self, scenario.sdl)

        # set our background to a tiled image
        self.setBackground ( properties.window_background )
        

    def createWidgets (self):
        """Creates all widgets for the dialog. Depending on wether we're a server or a client
        different widgets will be shown."""

        # create the version label
        version = "Version %s" % properties.version
        self.wm.register ( TinyLabel ( version , (15, 745) ) )

        # are we a server?
        if not scenario.start_server:
            # we're running as client. need an additional label and editfield for the hostname
            self.wm.register ( NormalLabel ( "Server runs on host: ", (250, 250) ) )

            self.host = EditField ( "localhost", 200, (500, 240) )

            self.wm.register ( self.host )

        # common widgets
        self.wm.register ( TitleLabel ( "Setup network information", (20, 10) ))

        self.wm.register ( NormalLabel ( "Server uses port: ", (250, 310)) )
        self.port = EditField ( str(properties.network_port), 200, (500, 300) )

        self.wm.register ( self.port )
           
        # buttons
        self.wm.register ( Button ( properties.path_dialogs + "butt-ok-moff.png",
                                    properties.path_dialogs + "butt-ok-mover.png",
                                    (284, 650), {widget.MOUSEBUTTONUP : self.ok } ), K_RETURN)
        self.wm.register ( Button ( properties.path_dialogs + "butt-back-moff.png",
                                    properties.path_dialogs + "butt-back-mover.png",
                                    (528, 650), {widget.MOUSEBUTTONUP : self.back } ) )

        # if we're supposed to start immediately then enable the timer
        if scenario.commandline_quickstart != 0:
            self.enableTimer(500)
            self.quickstart_state = 0


    def timer (self):
        """Callback triggered when the timer has fired. This is used if Civil is supposed to be
        started immediately without any user intervention. This will simulate a mouse click in the
        XXX button when YYY."""
        if 0:
            pass

        elif self.quickstart_state == 0:
            pygame.event.post(pygame.event.Event(widget.MOUSEBUTTONDOWN, { "pos": (290, 670) }))
            
        elif self.quickstart_state == 1:
            pygame.event.post(pygame.event.Event(widget.MOUSEBUTTONUP, { "pos": (290, 670) }))

        else:
            self.disableTimer()

        # increment statemachine
        self.quickstart_state += 1
            
    
    def ok (self, trigger, event):
        """Callback triggered when the user clicks the 'Ok' button. Applies the changes after
        verifying the given data, and closes the dialog. Tries to connect to the remote server if
        the player is not supposed to start the server. If something fails the player may choose
        again. If some data is missing the player must choose again"""

        # get the port. this is common for both client and server
        try:
            properties.network_port = int ( self.port.getText () )

            # is it valid?
            if not 1 <= properties.network_port <= 65535:
                # not a valid number, show a messagebox
                Messagebox ( "Invalid port number, must be in the range 1 to 65535!" )

                # repaint and go away
                self.wm.paint (force=1, clear=1)
                return widget.HANDLED
            
        except TypeError:
            # not a valid number, show a messagebox
            Messagebox ( "Invalid port number!" )

            # repaint and go away
            self.wm.paint (force=1, clear=1)
            return widget.HANDLED


        # are we not supposed to start a server?
        if not scenario.start_server:
            # nope, so try to connect to the server
            return self.__connectToServer ()

        # all is ok, we're accepting the dialog
        self.state = ACCEPTED
        return widget.DONE

               
    def back (self, trigger, event):
        """Callback triggered when the user clicks the 'Back' button. Simply closes the dialog and
        returns to the main dialog, ignoring any changes. This is used when the player wants to
        change something."""
        # we're cancelling the dialog
        self.state = REJECTED
        
        return widget.DONE


    def __connectToServer (self):
        """Connects to the server. If all is ok a connection is stored in the scenario. On error a
        dialog is shown to the user and the user may try again. An error is thus not fatal in any
        way. A messagebox is shown with some info to the player that he/she should try again when the
        server is ready."""
        
        # get the name of the host
        host = self.host.getText ()

        print "SetupNetwork: trying to connect to %s:%d" % ( host, properties.network_port )

        # did we get a hostname?
        if host == "":
            # oops, no host given, show a messagebox
            Messagebox ( "No server hostname given!" )

            # repaint and go away
            self.wm.paint (force=1, clear=1)
            return widget.HANDLED

        try:
            # create the socket
            new_socket = socket (AF_INET, SOCK_STREAM)
        
            # connect to the remote system
            new_socket.connect ( ( host, properties.network_port  ) )

            # all ok, store the new and connected socket and the extra info
            scenario.connection = Connection ( new_socket )

            # send our name
            scenario.connection.send ( scenario.local_player_name + '\n' )
            
        except:
            Messagebox ( "Could not connect to server, maybe the server is not ready?" )

            # repaint and go away
            self.wm.paint (force=1, clear=1)

            return widget.HANDLED
       
        # all is ok, we're accepting the dialog
        self.state = ACCEPTED
        
        return widget.DONE


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
