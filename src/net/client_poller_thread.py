##############################################################################
# $Id$
##############################################################################

import sys
import traceback
from threading     import *
from select        import select
import pygame
from pygame.locals import *

import properties
import scenario
import constants
from connection    import Connection

class ClientPollerThread ( Thread ):
    """This class implements a thread that waits for data on a specific socket connection. It sends
    a USEREVENT with a code and a connection parameter."""
    
    def __init__(self, connection):
        """Initializes the thread. connection is the Connection that
        it will listen to"""
        Thread.__init__(self)
        self.connection = connection

        # allow the event we will use for notification
        pygame.event.set_allowed ( USEREVENT )

        pygame.register_quit ( self.__pygameQuitting )

        # we assume that pygame is ok for now. this flag is needed so that we don't later try to
        # post events when the event subsystem is already shot down
        self.pygame_ok = 1
        

    def run (self):
        """Starts and runs the thread"""

        print "ClientPollerThread.run: starting connection poller thread"

        while scenario.playing == constants.PLAYING and self.pygame_ok:
            # We block with a timeout value
            # so we can detect when the game has ended
            try:
                incoming, out, exceptional = select ( [ self.connection ], [], [], 1.000 )
            except:
                # hmm, seems the other party has died?
                scenario.playing       = constants.GAME_ENDED
                scenario.end_game_type = constants.OPPONENT_CRASH

                # terminate ourselves
                break
            
            # Did we get anything?
            if len(incoming) == 0:
                continue

            # precautions
            if not self.pygame_ok:
                # pygame has gone away, so do we
                print "ClientPollerThread.run: pygame is not ok anymore, we're done"
                break
            
            try:
                # Create a new event
                e = pygame.event.Event(pygame.USEREVENT, { "code": properties.USEREVENT_DATA_FROM_SOCKET,
                                                           "connection": self.connection })
                # Send it to the main event loop
                pygame.event.post(e)

                # Just in case, also gives time for the main loop to read the packets
                pygame.time.wait (10)
            except:
                # Something broke badly, exit
                print "\nOops, something went wrong. Dumping brain contents: "
                print "-" * 75
                traceback.print_exc (file=sys.stdout)
                print "-" * 75
                break

        # the main loop is done, close the socket and ignore any errors from that
        try:
            self.connection.close ()
        except:
            pass
        
        print "ClientPollerThread.run: stopping connection poller thread"


    def __pygameQuitting (self):
        """Callback triggered when Pygame is being shut down. It means that the game has somehow
        ended and that main thread has closed down the Pygame stuff. We can no longer send messages
        anywhere but should instead just exit."""
        print "ClientPollerThread.__pygameQuitting"

        # ok, pygame is now down
        self.pygame_ok = 0

        
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
