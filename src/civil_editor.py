#! /usr/bin/env python2.3
###############################################################################################
# $Id$
###############################################################################################

import sys
import traceback
from qt                   import *

from simple_dom_parser    import SimpleDOMParser, Node
from editor.editor_weapon import EditorWeapon
from editor.mainwindow    import MainWindow
import editor.globals

def readWeapons ():
    """Reads in all weapons from an XML file and stores them in the global data.'"""

    # create a parser
    parser = SimpleDOMParser ()
    
    # parse the data
    try:
        # read it
        root = parser.parseFile ( open ('editor/data/weapons.xml') )
        
        # is the root ok?
        if root.getName () != 'weapons':
            # oops, invalid weapon data file
            print "invalid data in editor/data/weapons.xml, expected 'weapons' root tag"
            sys.exit ( 1 )

        # loop over all children this node has
        for node in root.getChildren ():
            # what do we have here?
            if node.getName () == 'defaults':
                # read the attributes indicating the default weapon id:s for the various units
                editor.globals.defaultweapons ['infantry']  = int ( node.getAttribute ( 'infantry' ))
                editor.globals.defaultweapons ['cavalry']   = int ( node.getAttribute ( 'cavalry' ))
                editor.globals.defaultweapons ['artillery'] = int ( node.getAttribute ( 'artillery' ))
                editor.globals.defaultweapons ['headquarter'] = int ( node.getAttribute ('headquarter'))

            elif node.getName () == 'weapon':
                # get the necessary attibutes
                id     = int ( node.getAttribute ( 'id' ) )
                name   = node.getAttribute ( 'name' )
                type   = node.getAttribute ( 'type' )
                range  = int ( node.getAttribute ( 'range' ) )
                damage = int ( node.getAttribute ( 'damage' ) )
                accuracy = int ( node.getAttribute ( 'accuracy' ) )

                # availability data
                startyear  = int ( node.getAttribute ( 'startyear' ) )
                startmonth = int ( node.getAttribute ( 'startmonth' ) )
                endyear    = int ( node.getAttribute ( 'endyear' ) )
                endmonth   = int ( node.getAttribute ( 'endmonth' ) )

                # create a new weapon
                weapon = EditorWeapon ( id, name, type, range, damage, accuracy,
                                        (startyear, startmonth), (endyear, endmonth) )
            
                # add the new objective to the global hash
                editor.globals.weapons [id] = weapon

            else:
                # oops, invalid tag here
                print "Invalid data in : editor/data/weapons.xml, expected weapons of defaults"
                sys.exit ( 1 )

    except KeyError:
        # oops, something went wrong
        print "error parsing editor/data/weapons.xml: "
        traceback.print_exc (file=sys.stdout)
        sys.exit ( 1 )


###############################################################################################
def readRanks ():
    """Reads in all available commander ranks from an XML file and stores them in the global data.'"""

    # create a parser
    parser = SimpleDOMParser ()
    
    # parse the data
    try:
        # read it
        root = parser.parseFile ( open ('editor/data/ranks.xml') )
        
        # is the root ok?
        if root.getName () != 'ranks':
            # oops, invalid weapon data file
            print "invalid data in editor/data/ranks.xml, expected 'ranks' root tag"
            sys.exit ( 1 )

        # loop over all children this node has
        for node in root.getChildren ():
            # what do we have here?
            if node.getName () == 'default':
                # the default rank
                editor.globals.defaultrank = node.getData ()

            elif node.getName () == 'rank':
                # a rank, get the rank name
                rank = node.getData ()
            
                # add the new rank to the global data
                editor.globals.ranks.append ( rank )

    except KeyError:
        # oops, something went wrong
        print "error parsing editor/data/ranks.xml: "
        traceback.print_exc (file=sys.stdout)
        sys.exit ( 1 )


###############################################################################################
def readXML ():
    """Reads in all XML data."""

    # read weapons
    readWeapons ()

    # read ranks
    readRanks ()
    
    
###############################################################################################
def main ():
    """Main funtion of the game. Initializes everything and executes the main window of the entire
    application. """
    
    # Needed by scenario_parser.parseMap
    import properties
    properties.is_civil_editor = 1
    
    # parse in all XML data
    readXML ()
    
    # create the application
    application = QApplication (sys.argv)

    # create the main window
    mainwindow = MainWindow ()
    application.setMainWidget ( mainwindow )

    # show it
    mainwindow.show ()

    # execute the application
    application.exec_loop()

 
###############################################################################################
# run, civil, run
if __name__ == '__main__':
    main ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
