import time
import os, re
from scenario_manager import ScenarioManager
from simple_dom_parser import SimpleDOMParser, Node
from scenario_info     import ScenarioInfo

start = time.time ()

sm = ScenarioManager ()

sm.loadAllScenarioInfos ( '../scenarios/' )


stop = time.time ()

print sm.scenarios

print "time elapsed: %f" % (stop - start)
