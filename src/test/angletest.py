
from math import atan2, pi, floor
import sys

x = 100
y = 100

def getangle (dx, dy):
    coeff = 180 / pi / 10
#    angle = int ( coeff * atan2 ( dy - y, dx - x ) ) + 9
    angle = int ( floor ( atan2 ( dx - x, y - dy ) * coeff ) % 36)       
#    if angle < 0:
#        angle += 36

    return angle

c1 = getangle (   0,   0 )
c2 = getangle ( 100,   0 )
c3 = getangle ( 200,   0 )

c4 = getangle (   0, 100 )
c6 = getangle ( 200, 100 )

c7 = getangle (   0, 200 )
c8 = getangle ( 100, 200 )
c9 = getangle ( 200, 200)


print "%2d %2d %2d" % ( c1, c2, c3)
print "%2d  x %2d"  % ( c4, c6 )
print "%2d %2d %2d" % ( c7, c8, c9 )
