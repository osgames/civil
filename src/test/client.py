#! /usr/local/bin/python

import server, client
from socket import *
import sys, traceback, string

           
class Client (Thread):

    def __init__(self, socket):
        """Initializes a thread that will connect to the given 'host' on the port 'port'."""
        # call superclass
        Thread.__init__ (self)

        # store the host and port
        self.host = host
        self.port = port

 
    def run (self):
        """Initial method that starts the thread. This is the entry point into the new thread."""
        try:
            # create the socket
            self.socket = socket (AF_INET, SOCK_STREAM)

            # connect to the remote system
            self.socket.connect ( ( self.host, self.port ) )
        except:
            print "error creating socket: " + `sys.exc_value`
            return

        for i in range(100):
            try:
                self.socket.send ( 'foo\n' )
            except:
                print "error sending data."
                return

        print "client done"
        self.socket.close()
