#! /usr/bin/env python2.1

import sys
import time
import pygame
import pygame.time
from pygame.locals import *

def main ():

    pygame.init ()

    # load the triangle
    triangle = pygame.image.load ( sys.argv [1] )

    # now loop over the entire map matrix 
    for y in range ( 16 ):
        print "(",
        for x in range ( 16 ):
            # get the pixel in the triangle
            color = triangle.get_at ( (x, y) )

            # transparent?
            if color[0] == 255:
                print "0,",

            else:
                print "1,",

        print "),"
           
if __name__ == '__main__':
    main ()
