#! /usr/bin/env python2.1

import copy

class foo:
    def __init__ (self, a):
        self.a = a

    def __str__(self):
        return self.a

def compare (a1, a2):
    if a1.a < a2.a:
        return -1
    elif a1.a == a2.a:
        return 0
    else:
        return 1

l = [ foo('aaa'),
      foo('d'),
      foo('f'),
      foo('ggg'),
      foo('e'),
      foo('bbb'),
      foo('ccc'),
      foo('iii') ]

l.sort (compare)

for f in l:
    print f
