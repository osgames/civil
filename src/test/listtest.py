#! /usr/local/bin/python

class foo:
    def __init__ (self):
        self.list = []

    def get (self):
        return self.list


f = foo ()

# add a few items to it
f.get ().append ( 1 )
f.get ().append ( 2 )
f.get ().append ( 3 )
f.get ().remove ( 2 )

print f.get ()
