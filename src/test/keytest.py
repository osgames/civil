#! /usr/bin/env python2

import sys
import pygame
import pygame.time
from pygame.locals import *

def main ():

    pygame.init ()

    sdl = pygame.display.set_mode ( [ 640, 480 ], 0, 24 )  

    
    # loop forever
    while 1:
        # get next event
        event = pygame.event.wait ()

        if event.type == MOUSEMOTION:
            continue
            
        # left mouse pressed?
        if event.type == MOUSEBUTTONDOWN:
            pygame.quit ()
            break

        if event.type == KEYDOWN:
            print event, event.key, pygame.key.name ( event.key), event.unicode
            print KMOD_SHIFT & event.mod, KMOD_ALT & event.mod,  KMOD_NONE & event.mod
            
if __name__ == '__main__':
    main ()
