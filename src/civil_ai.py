#! /usr/bin/env python2.3
###############################################################################################
# $Id$
###############################################################################################

import sys
import string
import getopt
import traceback
import cPickle
from socket import *

import constants
import properties
import scenario
from constants        import REBEL, UNION, UNKNOWN
from scenario_manager import ScenarioManager
from scenario_parser  import ScenarioParser

from net.connection   import Connection

from ai.event_loop    import eventLoop
from ai.aiMinimal     import aiMinimal

# host and port of server
host = 'localhost'
port = 20000

# our ai object will be initialized later
ai = ''


###############################################################################################
def usage (code):
    """Prints out usage for the ai client. This is called when the user gives the commandline
    parameter -h or --help, and if the parameters are wrong. It then exits using the passed error
    code."""

    print """
Usage:
    --host=<host>    - connect to <host> (default: localhost)
    --port=<port>    - connect using port <port> (default: 20000)
    """

    # go away
    sys.exit ( code )


###############################################################################################  
def parseCommandline (args):
    """Parses the commandline options and make susre they are valid. Exits the application on error
    and prints an error message."""

    global host, port

    # set defauls values
    scenario.local_player_id = UNKNOWN
    
    try:
        # get options an their arguments
        options, arguments = getopt.getopt ( args, "h:p:", [ "host=", "port=" ] )

    except getopt.GetoptError, message:
        # oops, invalid parameters
        print "AI: invalid parameters: " + str(message)
        usage ( 2 )

    # loop over all options
    for option, arg in options:
        # is this the port argument?
        if option in ("-p", "--port"):
            # store the port
            try:
                port = int ( arg )

                # verify validness of port
                if port < 1 or port > 65535:
                    # invalid port
                    raise
            except:
                # fsck, not an id, print error and exit
                print "\nAI: invalid port number. Valid range: 1-65535"
                usage ( 1 )
               
        # is this the host argument?
        elif option in ("-p", "--host"):
            # store the host
            host = arg

        # is this the help argument?
        elif option in ( "-?", "--help" ):
            # show usage and exit
            usage ( 0 )

       

###############################################################################################
def initNetwork ():
    """Initializes the network connection using the passed parameters. Attempts to connect to a
    server running on 'host' which listens on 'port'. If all is ok and the connection was formed
    ok then the connection is stored and None is returned. On error a string with an error
    message is returned."""

    global host, port
 
    try:
        # create the socket
        new_socket = socket (AF_INET, SOCK_STREAM)
        
        # connect to the remote system
        new_socket.connect ( ( host, port  ) )

        # all ok, store the new and connected socket and the extra info
        scenario.connection = Connection ( new_socket )

        # send our name
        scenario.connection.send ( "Mr Computer\n" )
      
    except error, data:
        # explode the error 
        code, message = data
        print "AI: error creating socket to '%s' on port %d: %s" % ( host, port, message )

        # was it a refused connection?
        if code == 111:
            # yes, warn the player
            print "AI: please ensure that a player acting as server is already running."
            print "AI: the AI client can not act as a server."

        sys.exit ()
 
    except:
        print "AI: error creating socket: "
        traceback.print_exc (file=sys.stdout)
        sys.exit ()
        
       
    # we got this far, so all is ok. The socket is now connected and the server has given us
    # permission to continue with setting up the game



###############################################################################################
def loadScenario (path):
    """Loads the scenario in 'path' and stores all needed data. Returns 1 if all is ok and 0 on error."""
    
    # create a scenario manager 
    scenario_manager = ScenarioManager ()

    print "AI: loading scenario... "

    # load in the raw XML and los map
    if not scenario_manager.loadScenario ( path ):
        # error loading the scenario
        print "AI: error loading scenario:", path
        return 0

    print "AI: scenario loaded ok"
    return 1


###############################################################################################
def readConfig ():
    """Receives config data from the server. This data contains the player the AI should play, the
    name of the scenario file that should be loaded and the name of the other player. Returns the
    path to the scenario that should be loaded"""


    # get data about the scenario from the server
    scenario_line = scenario.connection.readLine ( -1 )

    # get data about the opponent from the server
    player_line = scenario.connection.readLine ( -1 )

    # split up the data
    scenario_parts = scenario_line.split ()
    player_parts   = player_line.split ()

    # get the scenario data
    type      = scenario_parts[0]
    base_path = scenario_parts[1]

    # get the id of the local player. it is the opposite of that of the remote player whose id we
    # got sent
    scenario.local_player_id = (constants.UNION, constants.REBEL)[int (player_parts[0])]

    # and the remote player name
    scenario.remote_player_name = string.join ( player_parts[1:] )

    # get the full path to the scenario file of the given type
    path = { 'standard' : properties.path_scenarios,
             'saved'    : properties.path_saved_games,
             'custom'   : properties.path_custom_scenarios}[type]

    # create a full path
    filename = path + '/' + base_path

    print "AI: scenario is %s" % filename
    print "AI: our id is: %d" % scenario.local_player_id

    return filename


###############################################################################################
def main ():
    """Main funtion of the game. Initializes everything and the runs the main event loop. Creates
    internal datastructures and loads the scenario."""
    # we're an ai client
    scenario.local_player_ai = 1
    
    # parse commandline options
    parseCommandline ( sys.argv [1:] )

    # init the network
    initNetwork ()

    # read config data
    scenario_filename = readConfig ()

    # is the scenario ok?
    if scenario_filename == None or scenario_filename == '':
        # hm, not a valid name?
        print "AI: error receiving scenario data, aborting"
        return
    
    # all is ok, load the scenario
    if not loadScenario ( scenario_filename ):
        # loading failed, not too much we can do about that
        print "AI: error loading scenario, aborting"
        sys.exit ( 1 )

    # now we're playing!
    scenario.playing = constants.PLAYING

    # initialize the actual AI
    scenario.local_ai=aiMinimal()   
          
    # start the main event loop
    eventLoop ()


###############################################################################################
def start ():
    """Starting point for the application, simply runs main() and checks for errors and
    finally quits the application."""
    try:
        # run, civil, run!
        main ()

    except KeyboardInterrupt:
        # game was interrupted by the user
        print "AI: interrupted by user, exiting."
        sys.exit ( 0 )

    except SystemExit, value:
        # exited, just go on
        sys.exit ( value )
            
    except:
        # other error
        print "AI: oops, something went wrong. Dumping brain contents: "
        print "-" * 75
        traceback.print_exc (file=sys.stdout)
        print "-" * 75
        print "\nAI: Please mail this stack trace to civil-devel@lists.sourceforge.net"
        print "AI: along with a short description of what you did when this crash happened, "
        print "AI: so that the error can be fixed. Thank you! -- the Civil team\n"

        sys.exit ( 1 )

    # we got here, so everything was normal
    print "AI: game terminated normally."


###############################################################################################
# starting point if run directly on the commandline
if __name__ == '__main__':
    start ()
            
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
