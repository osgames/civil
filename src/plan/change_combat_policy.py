###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
from   plan                  import Plan
from   constants             import HOLDFIRE, DEFENSIVEFIRE, FIREATWILL


class ChangeCombatPolicy (Plan):
    """
    This class implements the plan 'changecombatpolicy'. This plan is used to set a general combat
    policy for the unit. The combat policy affects how the unit gets automatically assigned
    combat orders. The value is an integer defined in the file unit.py.
    
    Parameters:

    o the numeric id of the unit.
    o the new policy for the unit.
    """

    def __init__ (self, unitid=-1, policy=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "changecombatpolicy", unitid)

        # store passed data
        self.unitid = unitid
 
        # store the policy
        self.policy = policy

        # set a nice text depending on the policy id
        self.labeltext = ('holding fire', 'defensive fire only', 'firing at will')[policy]

        # this plan can not be visualized
        self.showonplayfield = 0
       

    def extract (self, parameters):
        """Extracts the data for the rotation and stores in local variables. This method is used
        when the command has been sent over the network."""
        
        # parse out the data
        self.id      = int ( parameters[0] )
        self.unitid  = int ( parameters[1] )
        self.policy  = int ( parameters[2] )


    def getPolicy (self):
        """Returns the new combat policy for the unit."""
        return self.policy
    
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        tmp = "%s %d %d %d\n" % ( self.getName (), self.id, self.unitid, self.policy )
        
        # return it
        return tmp


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
