###############################################################################################
# $Id$
###############################################################################################

import properties
from plan         import Plan


class ChangeMode (Plan):
    """
    This class implements the plan 'changemode'. This plan is sent by clients when a unit has been
    ordered to change it mode. Modes can by the player be changed like this:

    * limbered -> unlimbering -> unlimbered
    * mounted -> dismounting -> dismounted
    * formation -> changing to formation -> column

    And of course the reverse too. This plan will alter the mode of the unit, i.e. basically toggle it.

    Parameters:

    o the numeric id of the unit.
    """

    def __init__ (self, unitid = -1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "changemode", unitid)

        # store passed data
        self.unitid = unitid

        # get the unit
        #unit = scenario.info.units[unitid]
        
        # a nice text for the label
        self.labeltext = "changing mode"

        # TODO: this doesn't work, as the unit will be invalid here
        #self.labeltext = unit.getMode ().getTitle ()


    def extract (self, parameters):
        """Extracts the data for the plan and stores in local variables. This method is used
        when the command has been sent over the network."""
        
        # parse out the data
        self.id      = int ( parameters[0] )
        self.unitid  = int ( parameters[1] )

  
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d\n" % ( self.getName (), self.id, self.unitid )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
