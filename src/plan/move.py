###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
from plan                  import Plan
from general_move_command  import GeneralMoveCommand


class Move (GeneralMoveCommand):

    """This class implements the packet 'move'. This command is sent by clients when a unit has been
    ordered to move. The server will based on the data calculate when and how the movement takes
    place. The unit moves at a normal pace without altering state anyhow. A unit in column mode
    moves in column mode etc.

    Parameters:

    o the turn the command was issued by the player.
    o the numeric id of the unit
    o the x- and y-coordinates of the target.
    """

    def __init__ (self, unitid = -1, x = -1, y = -1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        GeneralMoveCommand.__init__ (self, "move", unitid)

        # set illegal values for all data
        self.unitid  = unitid
        self.targetx = x
        self.targety = y

        # a nice text for the label
        self.labeltext = "move to (%d,%d)" % ( x, y )


    def extract (self, parameters):
        """Extracts the data for the move."""

        # parse out the data
        self.id       = int ( parameters[0] )
        self.unitid   = int ( parameters[1] )
        self.targetx  = int ( parameters[2] )
        self.targety  = int ( parameters[3] )


    def getTarget (self):
        """Returns the target position the unit should move to as an (x,y) tuple."""
        return ( self.targetx, self.targety )

    
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return '%s %d %d %d %d\n' % (self.getName (), self.id, self.unitid, self.targetx, self.targety )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

  
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
