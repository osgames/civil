###############################################################################################
# $Id$
###############################################################################################

from plan                  import Plan
from general_move_command  import GeneralMoveCommand


class Retreat (GeneralMoveCommand):
    """This class implements the plan 'retreat'. It is used when a unit has been ordered to retreat
    by the player.

    A retreat is quite similar to a move, but the unit will keep its current facing. This means that
    the unit tries to pull back in an orderly fashion and still face the enemy.

    Parameters:

    o the numeric id of the unit
    o the x- and y-coordinates of the target.
    """

    def __init__ (self, unitid=-1, x=-1, y=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "retreat", unitid)

        # set illegal values for all data
        self.unitid  = unitid
        self.targetx = x
        self.targety = y

        # a nice text for the label
        self.labeltext = "retreat"


    def extract (self, parameters):
        """Extracts the data for the retreat."""

        # parse out the data
        self.id       = int ( parameters[0] )
        self.unitid   = int ( parameters[1] )
        self.targetx  = int ( parameters[2] )
        self.targety  = int ( parameters[3] )

 
    def getTarget (self):
        """Returns the target position the unit should move to as an (x,y) tuple."""
        return ( self.targetx, self.targety )

  
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return '%s %d %d %d %d\n' % (self.getName (), self.id, self.unitid, self.targetx, self.targety )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

  
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
