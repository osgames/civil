###############################################################################################
# $Id$
###############################################################################################

# import all plans
from assault              import Assault
from change_combat_policy import ChangeCombatPolicy
from change_mode          import ChangeMode
from move                 import Move
from move_fast            import MoveFast
from rotate               import Rotate
from rout                 import Rout
from retreat              import Retreat
from skirmish             import Skirmish
from wait                 import Wait

# our map of creators
creators = { 'assault'            : Assault,
             'changecombatpolicy' : ChangeCombatPolicy,
             'changemode'         : ChangeMode,
             'move'               : Move,
             'movefast'           : MoveFast,
             'retreat'            : Retreat,
             'rotate'             : Rotate,
             'rout'               : Rout,
             'skirmish'           : Skirmish,
             'wait'               : Wait
             }

 
def create (parameters):
    """Creates a new plan from the passed parameters. The parameter is a list of strings string that
    is supposed to be data for a plan. The first word is the type of plan, and the other are data
    for that specific plan. Based on the type a new plan is created and returned. """ 

    print "plan: create:", parameters

    # get the packet type
    plan_type = parameters[0]

    # do we have such a plan?
    if not creators.has_key ( plan_type ):
        # unknown plan type
        raise "factory.create: unknown plan type: '" + plan_type + "', params: ", parameters

    # create a plan based on the type
    plan = creators [plan_type] ()    

    # and let it extract whatever it needs from the parameters it got
    plan.extract ( parameters[1:] )
    
    return plan
    
       
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
