###############################################################################################
# $Id$
###############################################################################################

from plan                import Plan


class Rotate (Plan):
    """
    This class implements the packet 'rotate'. This plan means that the unit has been ordered to
    rotate to a new facing. The server will based on the data calculate when and how the 
    rotation takes place. The unit rotates without altering state anyhow. A unit in column mode
    rotates in column mode etc.

    Parameters:

    o the numeric id of the unit.
    o the new target the unit should be facing.
    """

    def __init__ (self, unitid=-1, x=-1, y=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "rotate", unitid )

        # store passed data
        self.unitid  = unitid
        self.targetx = x
        self.targety = y
       
        # a nice text for the label
        self.labeltext = "rotate to (%d,%d)" % ( x, y )


    def extract (self, parameters):
        """Extracts the data for the rotation and stores in local variables. This method is used
        when the command has been sent over the network."""
        
        # parse out the data
        self.id       = int ( parameters[0] )
        self.unitid   = int ( parameters[1] )
        self.targetx  = int ( parameters[2] )
        self.targety  = int ( parameters[3] )


    def getTarget (self):
        """Returns the target position the unit should rotate to face as an (x,y) tuple."""
        return ( self.targetx, self.targety )

 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d %d %d\n" % ( self.getName (), self.id, self.unitid, self.targetx, self.targety )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
