###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
from plan                  import Plan


class Assault (Plan):

    """This class implements the plan 'assault'. This plan means that the unit has been ordered to
    do a full scale assault on the target. An assault means that the unit will advance against the
    target while firing at it, and when close enough perform an assault and engage in hand-to-hand
    combat. Bloody stuff.

    Parameters:

    o the numeric id of the attacker unit
    o the numeric id of the target unit
    """


    def __init__ (self,  unitid=-1, targetid=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "assault", unitid)

        # set illegal values for all data
        self.unitid  = unitid
        self.targetid = targetid

        # create the label
        self.createLabel ()


    def createLabel (self):
        """Create the label text."""
        # do we have a target yet?
        if self.targetid == -1:
            # nope, set soemthing invalid
            self.labeltext = "assaulting unknown"
            return

        # first get the full unit if we have it
        targetunit = scenario.info.units[self.targetid]

        # a nice text for the label
        self.labeltext = "assaulting %s" % targetunit.getTypeString ().lower ()
        

    def extract (self, parameters):
        """Extracts the data for the move."""

        # parse out the data
        self.id       = int ( parameters[0] )
        self.unitid   = int ( parameters[1] )
        self.targetid = int ( parameters[2] )

        # create the label
        self.createLabel ()


    def getTargetId (self):
        """Returns the id of the target we're assaulting."""
        return self.targetid

      
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return it
        return '%s %d %d %d\n' % (self.getName (), self.id, self.unitid, self.targetid )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

  
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
