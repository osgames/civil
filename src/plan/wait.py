###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
from plan    import Plan


class Wait (Plan):
    """
    This class implements the plan 'wait'. This plan is used to make a unit wait for a certain
    number of seconds. When a unit waits it does nothing but keep the current mode and possibly
    defend itself against attacks. When a delay has elapsed the unit proceeds with the next plan.

    Parameters:

    o the numeric id of the unit.
    
    """

    def __init__ (self, unitid = -1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Plan.__init__ (self, "wait", unitid)

        # store passed data
        self.unitid = unitid
        
        # a nice text for the label
        self.labeltext = "wait"


    def extract (self, parameters):
        """Extracts the data for the plan and stores in local variables. This method is used
        when the command has been sent over the network."""
        
        # parse out the data
        self.id     = int ( parameters[0] )
        self.unitid = int ( parameters[1] )

  
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d\n" % ( self.getName (), self.id, self.unitid )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
