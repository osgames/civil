###############################################################################################
# $Id$
###############################################################################################

from plan import Plan

class GeneralMoveCommand(Plan):
    """Any command which displaces the unit somehow must subclass from this class. Target
    coordinates of the move command must be in self.targetx, self.targety. This class is mostly used
    to have a common base class for movement related orders."""
    pass

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
