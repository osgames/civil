###############################################################################################
# $Id$
###############################################################################################

import sys
import string
import pygame
import scenario
import constants
from action      import Action
from messages    import CHAT1


class SurrenderAct (Action):
    """
    This class implements the action 'surrender'. This updated means that the player sending
    it wants to surrender the battle unconditionally and declare the opponent a winner. The server
    will internally handle the surrender and send out an 'EndGame' update to both players."""


    def __init__ (self, player=-1):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "surrender_act")

        # store the player too
        self.player = player
        

    def extract (self, parameters):
        """Extracts the data for the command. """ 
        # parse out the data
        self.player = int ( parameters[0] ) 
    

    def execute (self):
        """Executes the plan. Will terminate the application by setting the flag that indicates that
        the main loop should terminate. Stores the surrendering player data."""

        # we're no longer playing a game
        scenario.playing = constants.GAME_ENDED

        # get the type of end game
        endgametype = [constants.REBEL_SURRENDER, constants.UNION_SURRENDER][self.player]

        # store the type of end game
        scenario.end_game_type = endgametype

        # this should not be done for the ai
        if scenario.local_player_ai:
            # ai player, go away
            return
        
        # add the message we have to the messages
        surrendername = ['Rebel', 'Union'][self.player]
        scenario.messages.add ( "%s player has surrendered" % surrendername, CHAT1 )


    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d\n" % ( self.getName (), self.player )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
