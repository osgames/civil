###############################################################################################
# $Id$
###############################################################################################

import sys
import string
import pygame

import scenario
import constants
from action      import Action


class CeaseFireResponseAct (Action):
    """
    This class implements the action 'cease fire response'. It is sent to the player that initiated
    a cease fire question and contains the answer that the other player gave. Based on the answer
    the game is either ended or it continues as normally.
    """


    def __init__ (self, player=-1, accepted=0):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "cease_fire_response_act")
        
        # store the player too
        self.player = player
        self.accepted = accepted
        

    def getPlayer (self):
        """Returns the player the update was created by."""
        return self.player


    def wasAccepted (self):
        """Returns 1 if the cease fire was accepted and 0 if not."""
        return self.accepted
    

    def extract (self, parameters):
        """Extracts the data for the command. """ 
        # parse out the data
        self.player   = int ( parameters[0] ) 
        self.accepted = int ( parameters[1] ) 
    

    def execute (self):
        """Executes the action. Ends the game or continues as normally."""

        # are we the same player that sent the request?
        if self.player == scenario.local_player_id:
            # yes, we don't want to do anything here, only the remote player has anything to do
            return

        # this should actually never be sent to the AI player, as it never sends out an cease fire request
        if scenario.local_player_ai:
            # ai player
            print "AI: CeaseFireResponseAct.execute: *** should never happen ***"
            return

        # was the request accepted?
        if self.accepted:
            # yes, the game should end here and now
            scenario.messages.add ( "Your cease fire offer was accepted, ending the game" )

            # the game is not being played anymore, we can stop immediately
            scenario.playing       = constants.GAME_ENDED
            scenario.end_game_type = constants.CEASE_FIRE

        else:
            # not accepted
            scenario.messages.add ( "Your cease fire offer was rejected" )


    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d\n" % ( self.getName (), self.player, self.accepted )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
