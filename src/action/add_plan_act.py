###############################################################################################
# $Id$
###############################################################################################

import scenario
from action      import Action
import plan.factory


class AddPlanAct (Action):
    """
    This class implements the action 'add plan'. This is sent by the server when an unit
    has finished a plan and it should be removed from the unit's plans. The first plan that the unit
    has (the one that's supposed to be active) is removed."""

    def __init__ (self, unitid=-1, plan=None ):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "add_plan_act" )

        # store all data
        self.unitid = unitid

        # store the plan
        self.plan = plan
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )

        print "AddPlanAct.extract:", parameters [1:]

        # create the plan from the rest of the data
        self.plan = plan.factory.create ( parameters [1:] )
        
        
    def execute (self):
        """Executed the action. Finds the affected unit and just removes the first plan it has."""
        
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # is it a local unit? only do this to local units
        if unit.getOwner () == scenario.local_player_id:
            # add the new plan
            unit.getPlans ().append ( self.plan )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %s\n" % ( self.getName (), self.unitid, self.plan.toString ().strip () )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
