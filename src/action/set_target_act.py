###############################################################################################
# $Id$
###############################################################################################

import scenario
from action                  import Action

from mode.mode_factory       import createMode


class SetTargetAct (Action):
    """
    This class implements the action 'set target'. This is sent by the server when an unit
    gets a new target it is firing at. IT can also be used to clear an old target, which is done
    when giving a target id of -1.
    
    """

    def __init__ (self, unitid=-1, targetid=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Action.__init__ (self, "set_target_act" )

        # set values for all data
        self.unitid = unitid
        self.targetid = targetid


    def extract (self, parameters):
        """Extracts the data for the move."""

        # parse out the data
        self.unitid   = int ( parameters[0] )
        self.targetid = int ( parameters[1] )

 
    def execute (self):
        """Executed the action. Finds the affected unit and updates its target to the new target."""
        
        # get the unit that is setting the target
        unit = scenario.info.units [ self.unitid ]

        # do we have a target too? or are we clearing an old target?
        if self.targetid == -1:
            # clearing an old target
            target = None

        else:
            # setting a new target
            target = scenario.info.units [ self.targetid ]

        # and assign the target
        unit.setTarget ( target )
          
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return "%s %d %d\n" % ( self.getName (), self.unitid, self.targetid )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
