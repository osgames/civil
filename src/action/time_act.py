###############################################################################################
# $Id$
###############################################################################################

import datetime

import scenario
from action      import Action


class TimeAct (Action):
    """
    This class implements the action 'time'. This is sent by the server when the time has
    changed. It is sent out regularly so that players can keep up with what's going on. The time is
    given in elapsed seconds since the last update.
    """

    def __init__ (self, seconds=-1 ):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "time_act" )

        # store all data
        self.seconds = seconds
       

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.seconds = int ( parameters[0] )

        
    def execute (self):
        """Executed the action. Adds the given number of seconds to the current game time."""
        
        # create a delta time
        delta = datetime.timedelta ( seconds=self.seconds )
        
        # set the new time
        scenario.info.setCurrentDate ( scenario.info.getCurrentDate () + delta )

        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'time_changed', None )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d\n" % ( self.getName (), self.seconds )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
