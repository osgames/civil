###############################################################################################
# $Id$
###############################################################################################

import scenario
from action      import Action
import plan.factory


class ClearPlansAct (Action):
    """
    This class implements the action 'clear plans'. This is sent by the server when an unit
    needs to get rid of all plans that it currently has. All plans are simply removed and nothing
    else done."""

    def __init__ (self, unitid=-1):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "clear_plans_act" )

        # store all data
        self.unitid = unitid
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )
        
        
    def execute (self):
        """Executed the action. Finds the affected unit and just clears all plans."""
        
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # clear the plans
        unit.setPlans ( [] )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d\n" % ( self.getName (), self.unitid, )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
