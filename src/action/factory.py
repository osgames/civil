###############################################################################################
# $Id$
###############################################################################################

# import all action  
from add_plan_act             import AddPlanAct
from cease_fire_act           import CeaseFireAct
from cease_fire_response_act  import CeaseFireResponseAct
from change_combat_policy_act import ChangeCombatPolicyAct
from change_modifiers_act     import ChangeModifiersAct
from chat_act                 import ChatAct
from clear_plans_act          import ClearPlansAct
from clear_target_act         import ClearTargetAct
from damage_act               import DamageAct
from done_act                 import DoneAct
from end_game_act             import EndGameAct
from move_act                 import MoveAct
from quit_act                 import QuitAct
from reinforcements_act       import ReinforcementsAct
from rotate_act               import RotateAct
from set_mode_act             import SetModeAct
from set_target_act           import SetTargetAct
from set_visibility_act       import SetVisibilityAct
from surrender_act            import SurrenderAct
from time_act                 import TimeAct


# our map of creators
creators = { 'add_plan_act'             : AddPlanAct,
             'cease_fire_act'           : CeaseFireAct,
             'cease_fire_response_act'  : CeaseFireResponseAct,
             'change_combat_policy_act' : ChangeCombatPolicyAct,
             'change_modifiers_act'     : ChangeModifiersAct,
             'chat_act'                 : ChatAct,
             'clear_plans_act'          : ClearPlansAct,
             'clear_target_act'         : ClearTargetAct,
             'damage_act'               : DamageAct,
             'done_act'                 : DoneAct,
             'endgame_act'              : EndGameAct,
             'move_act'                 : MoveAct,
             'quit_act'                 : QuitAct,
             'reinforcements_act'       : ReinforcementsAct,
             'rotate_act'               : RotateAct,
             'set_target_act'           : SetTargetAct,
             'set_mode_act'             : SetModeAct,
             'set_visibility_act'       : SetVisibilityAct,
             'surrender_act'            : SurrenderAct,
             'time_act'                 : TimeAct
             }
             

def isAction (type):
    """Checks wether the given type is an action. The type is a cleartext string as received from
    the network."""
    # just do a check
    return creators.has_key ( type )


def create (parameters):
    """Creates a new action from the passed parameters. The parameter is a list of strings string
    that is supposed to be data for a action. The first word is the type of action, and the other
    are data for that specific upate. Based on the type a new instance is created and returned. """   

    # get the packet type
    type = parameters[0]

    # do we have such a plan?
    if not creators.has_key ( type ):
        # not an action
        return None
  
    # create a action based on the type
    action = creators [type] ()    

    # and let it extract whatever it needs from the parameters it got
    action.extract ( parameters[1:] )
    
    return action
  

def isAction (type):
    """Checks wether the given type (string) is an action or not. This can be used by the engine to
    determine wether something is a plan or an action. Return 1 if it is an action and 0 if not."""
    # just do a brutal check
    return creators.has_key ( type )

       
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
