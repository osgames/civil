###############################################################################################
# $Id$
###############################################################################################

import scenario
import constants
from action                  import Action


class SetVisibilityAct (Action):
    """
    This class implements the action 'set visibilty'. This is sent by the server when an enemy unit
    chenges visibility for the local player. Own units are always visible.
    """

    def __init__ (self, unitid=-1, visible=-1, receiver=constants.BOTH):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Action.__init__ (self, "set_visibility_act" )

        # set values for all data
        self.unitid  = unitid
        self.visible = visible

        # set the custom receiver too
        self.setReceiver ( receiver )
        

    def extract (self, parameters):
        """Extracts the data for the action."""

        # parse out the data
        self.unitid  = int ( parameters[0] )
        self.visible = int ( parameters[1] )

 
    def execute (self):
        """Executed the action. Finds the affected unit and sets the new visibilty. Lets the other
        part of the game know about the changed unit."""
        
        # get the unit that is getting the target cleared
        unit = scenario.info.units [ self.unitid ]

        # and clear it
        unit.setVisible ( self.visible )
          
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return "%s %d %d\n" % ( self.getName (), self.unitid, self.visible )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
