###############################################################################################
# $Id$
###############################################################################################

import scenario
from action                  import Action


class ClearTargetAct (Action):
    """
    This class implements the action 'clear target'. This is sent by the server when an unit
    gets its target cleared. Finds the unit and clear the target.    
    """

    def __init__ (self, unitid=-1):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Action.__init__ (self, "clear_target_act" )

        # set values for all data
        self.unitid = unitid


    def extract (self, parameters):
        """Extracts the data for the action."""

        # parse out the data
        self.unitid = int ( parameters[0] )

 
    def execute (self):
        """Executed the action. Finds the affected unit and clears the target."""
        
        # get the unit that is getting the target cleared
        unit = scenario.info.units [ self.unitid ]

        # and clear it
        unit.setTarget ( None )
          
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return "%s %d\n" % ( self.getName (), self.unitid )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
