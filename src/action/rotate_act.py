###############################################################################################
# $Id$
###############################################################################################

import scenario
from action      import Action


class RotateAct (Action):
    """
    
    This class implements the action 'rotate'. This is sent by the server when a unit has rotated
    some amount. The new facing will be set and the game informed of the changed unit.
    """

    def __init__ (self, unitid=-1, facing=-1 ):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "rotate_act" )

        # store all data
        self.unitid = unitid
        self.facing = facing
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )
        self.facing = int ( parameters[1] )

        
    def execute (self):
        """Executed the command. Finds the affected unit and updates its facing to the new
        facing."""
               
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # set the new facing
        unit.setFacing ( self.facing )
           
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
       
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d\n" % ( self.getName (), self.unitid, self.facing )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
