###############################################################################################
# $Id$
###############################################################################################

import constants
import scenario
from action      import Action


class DoneAct (Action):
    """
    This class implements action 'done'. This is sent by the server when an unit
    has finished a plan and it should be removed from the unit's plans. The first plan that the unit
    has (the one that's supposed to be active) is removed."""

    def __init__ (self, unitid=-1, receiver=constants.BOTH):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "done_act" )

        # store all data
        self.unitid = unitid
         
        # set the custom receiver too
        self.setReceiver ( receiver )


    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )

        
    def execute (self):
        """Executed the action. Finds the affected unit and just removes the first plan it has."""
        
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # remove the first plan
        unit.setPlans ( unit.getPlans() [1:] )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d\n" % ( self.getName (), self.unitid )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
