###############################################################################################
# $Id$
###############################################################################################

import sys
import string
import constants
import pygame
import scenario
from action      import Action
from messages    import CHAT1


class QuitAct (Action):
    """
    This class implements the action 'quit'. Used when a player has quit.

    TODO: should also contain some kind of info for why that game was quit?
    """

    def __init__ (self, player=-1):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "quit_act" )

        # store the player too
        self.player = player
        

    def extract (self, parameters):
        """Extracts the data for the command. """ 
        # parse out the data
        self.player = int ( parameters[0] ) 


    def execute (self):
        """Executes the action. Sets a flag that indicates that the game has ended."""
        # we're no longer playing a game. this also terminates the AI client as the human player has
        # quit 
        scenario.playing = constants.GAME_ENDED

        # get the type of end game
        endgametype = [constants.REBEL_QUIT, constants.UNION_QUIT][self.player]

        # store the type of end game
        scenario.end_game_type = endgametype

        # this should not be done for the ai
        if scenario.local_player_ai:
            # ai player, go away
            print "AI: QuitAct.execute: human player has quit, so we will too"
            return
        
        # add the message we have to the messages
        surrendername = ['Rebel', 'Union'][self.player]
        scenario.messages.add ( "%s player has quit" % surrendername, CHAT1 )


    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d\n" % ( self.getName (), self.player )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
