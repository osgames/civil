###############################################################################################
# $Id$
###############################################################################################

import string

import scenario
from action      import Action


class ReinforcementsAct (Action):
    """
    This class implements the action 'reinforcements'. This is sent by the server when a number of
    units have arrived at the battle scene as reinforcements. Both Union and Rebel units are in this
    same action. The player owned units are just set to be visible, and the enemy units are visible
    only if a friendly unit sees them.
    """

    def __init__ (self, unit_ids=[]):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "reinforcements_act" )

        # store all data
        self.unit_ids = unit_ids
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # the parameters is a list of the ids, but as strings. so convert each string to an integer
        # and create a list from that
        self.unit_ids = map ( lambda id: int(id), parameters )

        
    def execute (self):
        """Executed the command. Finds the affected unit and updates its facing to the new
        facing."""

        arrived = 0
        
        # loop over all unit ids that should be shown
        for unit_id in self.unit_ids:
            # get the real unit
            unit = scenario.info.units [ unit_id ]

            # is it ours?
            if unit.getOwner() == scenario.local_player_id:
                # yes, so just set it as visible
                unit.setVisible ( visible=1 )

                # remove from the internal reinforcements map
                if scenario.info.reinforcements.has_key ( unit_id ):
                    del scenario.info.reinforcements [ unit_id ]

                # so the unit has arrived, add it to the global structure of available units
                scenario.info.units [ unit_id ] = unit

                # one more arrived
                arrived += 1

            else:
                # TODO: an enemy unit has arrived check if any of our units see it?
                pass

            # did we get any own units?
            if arrived > 0:
                scenario.messages.add ( '%d reinforcements units have arrived' % arrived )
                
        # make sure the world knows of this change
        #if not scenario.local_player_ai:
        #    scenario.dispatcher.emit ( 'units_changed', (unit,) )
       
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a space separated string from all the id:s
        id_string = string.join ( map (lambda id: str(id), self.unit_ids))

        # create a string and return
        return "%s %s\n" % ( self.getName (), id_string )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
