###############################################################################################
# $Id$
###############################################################################################

import scenario
from action      import Action


class MoveAct (Action):
    """
    This class implements the action 'move'. This is sent by the server when an unit
    should move to a new position. Facing, mode or other data is not affected in any way, just the
    unit position.
    """

    def __init__ (self, unitid=-1, x=-1, y=-1 ):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "move_act" )

        # store all data
        self.unitid = unitid
        self.x      = x
        self.y      = y
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )
        self.x      = int ( parameters[1] )
        self.y      = int ( parameters[2] )

        
    def execute (self):
        """Executed the action. Finds the affected unit and updates its position to the new
        position."""
               
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # set the new position
        unit.setPosition ( (self.x, self.y) )

        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d %d\n" % ( self.getName (), self.unitid, self.x, self.y )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
