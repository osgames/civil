###############################################################################################
# $Id$
###############################################################################################

import scenario
from action                  import Action

from mode.mode_factory       import createMode


class SetModeAct (Action):
    """
    This class implements the action 'setmode'. This is sent by the server when an unit
    gets a new mode.
    """

    def __init__ (self, unitid=-1, mode=""):
        """Initializes the instance."""
        # call superclass constructor, the turn may not be valid here yet
        Action.__init__ (self, "set_mode_act" )

        # set values for all data
        self.unitid = unitid
        self.mode   = mode


    def extract (self, parameters):
        """Extracts the data for the move."""

        # parse out the data
        self.unitid = int ( parameters[0] )
        self.mode   =       parameters[1]

 
    def execute (self):
        """Executed the action. Finds the affected unit and updates its mode to the new mode."""
        
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # create the proper mode instance
        mode = createMode ( self.mode )

        # assign it
        unit.setMode ( mode )
          
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )


    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string
        return "%s %d %s\n" % ( self.getName (), self.unitid, self.mode )
        

    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()

 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
