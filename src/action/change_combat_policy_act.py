###############################################################################################
# $Id$
###############################################################################################

import scenario
from action      import Action


class ChangeCombatPolicyAct (Action):
    """
    This class implements the action 'change_combat_policy'. Sets a new combat policy for the unit.
    """

    def __init__ (self, unitid=-1, policy=-1 ):
        """Initializes the instance."""
        # call superclass constructor
        Action.__init__ (self, "change_combat_policy_act" )

        # store all data
        self.unitid = unitid
        self.policy = policy
        

    def extract (self, parameters):
        """Extracts the data for the command. """
        # parse out the data
        self.unitid = int ( parameters[0] )
        self.policy = int ( parameters[1] )

        
    def execute (self):
        """Executed the command. Finds the affected unit and updates its policy to the new
        policy."""
               
        # get the unit with the given id 
        unit = scenario.info.units [ self.unitid ]

        # set the new policy
        unit.setCombatPolicy ( self.policy )
          
        # make sure the world knows of this change
        if not scenario.local_player_ai:
            scenario.dispatcher.emit ( 'units_changed', (unit,) )
        
 
    def toString (self):
        """Returns a string representation of the command, suitable for sending over a socket."""
        # create a string and return
        return "%s %d %d\n" % ( self.getName (), self.unitid, self.policy )


    def __str__ (self):
        """Convenience wrapper for toString() suitable for using when debugging and printing the
        command to the screen. Will just call toString()."""
        return self.toString ()
    
     
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
