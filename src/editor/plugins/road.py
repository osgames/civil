from editor.plugins.creator import *

class RoadCreator ( Creator ):
    """RoadCreator creates the proper icons for a continuous road. It does
    NOT generate road, merely looks for and places suitable road icons."""
    def __init__(self, iconview):
        Creator.__init__(self, iconview, Creator.addkind__path, "RoadCreator", "Create continuous road pieces")

    def isEmpty(self, set, id):
        return set[id] == 0


def new (iconview):
    return RoadCreator(iconview)
