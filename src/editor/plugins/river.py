from editor.plugins.creator import *

class RiverCreator ( Creator ):
    """RiverCreator creates the proper icons for a continuous river. It does
    NOT generate river, merely looks for suitable river icons."""
    def __init__(self, iconview):
        Creator.__init__(self, iconview, Creator.addkind__river, "RiverCreator", "Create continuous river pieces")

    def isEmpty(self, set, id):
        return set[id] == 0

def new (iconview):
    return RiverCreator(iconview)
