###############################################################################################
# $Id$
###############################################################################################

from editor.plugins.creator import *

class LosTracer (Plugin):
    """This class lets the user trace LOS (Line of sight). The start and endpoints for the trace are
    set using the mouse. By clicking the left mouse button in the map the start position is set, and
    the right button sets the end point. The line of sight is then traced from the start towards the
    end point, as long as visible.
    """


    def __init__(self):
        Plugin.__init__ (self, "Los tracer", "Trace line of sight between two positions")

        # start and end positions
        self.start = None
        self.end   = None
        

    def mapClickedLeft (self, x, y, hexx, hexy):
        """Handles an event when the left mouse button has been pressed in the map. Stores the
        position as the new starting point."""
        print "LosTracer.mapClickedLeft", x, y, hexx, hexy

        # store the position
        self.start = (x, y)

        
    def mapClickedRight (self, x, y, hexx, hexy):
        """Handles an event when the right mouse button has been pressed in the map. Stores the
        position as the new ending point."""
        print "LosTracer.mapClickedRight", x, y, hexx, hexy

        # store the position
        self.end = (x, y)


def new (iconview):
    """Constructor function."""
    return LosTracer ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
