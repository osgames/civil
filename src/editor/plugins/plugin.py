###############################################################################################
# $Id$
###############################################################################################

class Plugin:
    """Superclass for all plugins. It mainly defines an interface that inheriting plugins should
    use. A plugin is a class that performs some specific task in response to something the user does
    with the mouse in the map. The plugins can manipulate the map somehow or do something else.


    The methods in this class let the plugin react to various mouse events."""

    def __init__(self, name, longExplanation):
        self.name = name
        self.longExplanation = longExplanation


    def leftMousePressed (self):
        """Handles an event when the left mouse button has been pressed in the map. Should be
        overridden by the plugin if it wants notification of that event."""
        pass

        
    def midMousePressed (self):
        """Handles an event when the mid mouse button has been pressed in the map. Should be
        overridden by the plugin if it wants notification of that event."""
        pass

        
    def rightMousePressed (self):
        """Handles an event when the right mouse button has been pressed in the map. Should be
        overridden by  the plugin if it wants notification of that event."""
        pass
        

    def leftMouseReleased (self):
        """Handles an event when the left mouse button has been released in the map. Should be
        overridden by the plugin if it wants notification of that event."""
        pass

        
    def midMouseReleased (self):
        """Handles an event when the mid mouse button has been released in the map. Should be
        overridden by the plugin if it wants notification of that event."""
        pass

        
    def rightMouseReleased (self):
        """Handles an event when the right mouse button has been released in the map. Should be
        overridden by  the plugin if it wants notification of that event."""
        pass
        
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

