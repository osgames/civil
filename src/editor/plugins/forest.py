from editor.plugins.creator import *
import terrain

class ForestCreator ( Creator ):
    """ForestCreator creates the proper icons for a continuous block
    of forest."""
    def __init__(self, iconview):
        Creator.__init__(self, iconview, Creator.addkind__forest, "ForestCreator", "Create continuous blocks of forest")

    def isEmpty(self, set, id):
        for i in set[id]:
            if i == terrain.terrainDict['w']:
                return 0
        return 1

    def get_empty_need(self):
        return [None, None, None, None, None, None]

    def calcNeed(self, x, y, neighbors, bit_from_our_hex):
        t = scenario.map.getHex(x, y).template.getTerrains()

        # Add connection to our primary hex
        rev = bit_from_our_hex << 3
        if rev > UL: rev = bit_from_our_hex >> 3

        w = terrain.terrainDict['w']
        if rev == 1:
            t[0] = w
        if rev == 2:
            t[1] = w
        if rev == 4:
            t[2] = w
        if rev == 8:
            t[3] = w
        if rev == 16:
            t[4] = w
        if rev == 32:
            t[5] = w
        return t

    def match(self, from_set, our_need):
        assert(len(from_set) == len(our_need))
        for i in range(len(from_set)):
            if from_set[i] != our_need[i]:
                return 0
        return 1

def new (iconview):
    return ForestCreator(iconview)
