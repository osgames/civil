from editor.plugins.creator import *

class HillCreator ( Creator ):
    """HillCreator creates the proper icons for a continuous road. It does
    NOT generate road, merely looks for and places suitable road icons."""
    def __init__(self, iconview):
        Creator.__init__(self, iconview, Creator.addkind__hill, "HillCreator", "Create continuous hill crestlines")

    def isEmpty(self, set, id):
        for i in set[id]:
            if i != 0:
                return 0
        return 1
        

def new (iconview):
    return HillCreator(iconview)
