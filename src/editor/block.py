###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from simple_dom_parser import SimpleDOMParser, Node

class Block:
    """This class..."""
    
    def __init__(self, filename ):
        """Initializes the instance."""

        # no icons yet
        self.icons = []

        # create a new parser
        domparser = SimpleDOMParser ()

        # parse the data
        try:
            root = domparser.parseFile ( open (filename) )
            
        except:
            # serios error
            raise
            raise "Block: error parsing %s" % filename

        # get name
        self.name = root.getChild ('name').getData ()

        self.maxx = 0
        self.maxy = 0
        
        # loop over all icons
        for node in root.getChild ('icons').getChildren ():
            # get the x, y and icon id
            x    = int ( node.getAttribute ( 'x' ) )
            y    = int ( node.getAttribute ( 'y' ) )
            id = int ( node.getAttribute ( 'id' ) )

            # store all data in a new item
            self.icons.append ( (x, y, id ) )

            # is this block enlarged now?
            if x > self.maxx:
                # new max x
                self.maxx = x

            if y > self.maxy:
                # new may y
                self.maxy = y


    def getName (self):
        """Returns the name of the block. This name can be used to identify the block in lists etc and
        is supposed to be human readable."""
        return self.name


    def getIcons (self):
        """Returns a list of all icons the block contains. The items in the list are tuples (x,y,id)
        where 'x' and 'y' are the hex coordinates and 'id' the id of the icon for that position."""
        return self.icons


    def getSize (self):
        """Returns the size of the block in hexes."""
        return ( self.maxx + 1, self.maxy + 1 )

    
#    Local Variables:
#    mode: auto-fill
#    fill-column: 100
#    End:

