###############################################################################################
# $Id$
###############################################################################################


import sys
from qt import *
from constants import REBEL, UNION, UNKNOWN


class EditLocation(QDialog):
    """This class provides a dialog which allows the user to graphically edit the properties of
    locations."""

    def __init__(self,parent, location):
        QDialog.__init__(self,parent, "edit location", 1)

        # store the location
        self.location = location

        self.resize(399,127)
        self.setCaption(self.tr('Edit location'))
        Edit_locationLayout = QGridLayout(self)
        Edit_locationLayout.setSpacing(6)
        Edit_locationLayout.setMargin(11)
        spacer = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        Edit_locationLayout.addItem(spacer,3,1)

        self.name = QLineEdit(self,'name')
        QToolTip.add(self.name,self.tr('Name of the location'))

        Edit_locationLayout.addMultiCellWidget(self.name,0,0,2,3)

        self.TextLabel1 = QLabel(self,'TextLabel1')
        self.TextLabel1.setSizePolicy(QSizePolicy(1,1,self.TextLabel1.sizePolicy().hasHeightForWidth()))
        self.TextLabel1.setText(self.tr('Name:'))
        self.TextLabel1.setAlignment(QLabel.AlignVCenter | QLabel.AlignLeft)

        Edit_locationLayout.addWidget(self.TextLabel1,0,0)

        Layout19 = QHBoxLayout()
        Layout19.setSpacing(6)
        Layout19.setMargin(0)
        spacer_7 = QSpacerItem(20,20,QSizePolicy.Expanding,QSizePolicy.Minimum)
        Layout19.addItem(spacer_7)

        self.okbutton = QPushButton(self,'ok')
        self.okbutton.setText(self.tr('&Ok'))
        Layout19.addWidget(self.okbutton)
        spacer_8 = QSpacerItem(20,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        Layout19.addItem(spacer_8)

        self.cancel = QPushButton(self,'cancel')
        self.cancel.setText(self.tr('&Cancel'))
        Layout19.addWidget(self.cancel)

        Edit_locationLayout.addMultiCellLayout(Layout19,5,5,2,3)

        # populate all info into the widgets
        self.populate ()
        
        # connect signals to slots
        self.connect ( self.cancel,   SIGNAL ('clicked()'), self, SLOT('reject()'))
        self.connect ( self.okbutton, SIGNAL ('clicked()'), self.ok)


    def populate (self):
        """Populates the dialog with data from the location. This is a separate unit so
        that it can be easily picked out and improved."""

        # basic data
        self.name.setText ( self.location.getName () )

    def ok(self):
        """Callback triggered when the player clicks Ok. Stores all the data in the location and
        closes the dialog."""

        # set all data
        self.location.setName ( str (self.name.text ()) )

        # close the dialog
        self.accept ()
        

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
