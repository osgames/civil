###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from qt import *
import properties
from constants import REBEL, UNION, UNKNOWN

###############################################################################################
class LocationItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the location the item represents. The location can be retrieved using the 'getLocation()'
  method."""

  
  def __init__ (self, parent, location ):
    # create the strings for the item
    name   = location.getName ()
    QListViewItem.__init__ ( self, parent, name )
 
    # store the location
    self.location = location
 
 
  def getLocation (self):
    """Returns the location this item represents."""
    return self.location


  def update (self):
    """Updates the labels for the item. Can be used if the location has changed."""

    # set the new labels
    self.setText ( 0, self.location.getName () )

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

