###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from qt import *
import properties

###############################################################################################
class BrigadeItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the brigade the item represents. Th brigade can be retrieved using the 'getBrigade()' method."""

  
  def __init__ (self, parent, brigade):
    QListViewItem.__init__ ( self, parent, brigade.name, "" )
 
    # store the brigade
    self.brigade = brigade

    # set the item to be open by default
    self.setOpen ( 1 )

    # set the displayed data
    self.update ()


  def getOrganization (self):
    """Returns the organization that this item contains. """
    return self.getBrigade ()

  
  def getBrigade (self):
    """Returns the brigade this item represents."""
    return self.brigade

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText ( 0, self.brigade.getName () )


###############################################################################################
class RegimentItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the regiment the item represents. Th regiment can be retrieved using the 'getRegiment()' method."""

  
  def __init__ (self, parent, regiment):
    QListViewItem.__init__ ( self, parent, regiment.name, "" )
 
    # store the regiment
    self.regiment = regiment

    # set the item to be open by default
    self.setOpen ( 1 )
  
    # set the displayed data
    self.update ()


  def getOrganization (self):
    """Returns the organization that this item contains. """
    return self.getRegiment ()

 
  def getRegiment (self):
    """Returns the regiment this item represents."""
    return self.regiment

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText ( 0, self.regiment.getName () )


###############################################################################################
class BattallionItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the battallion the item represents. Th battallion can be retrieved using the 'getBattallion()'
  method.""" 

  
  def __init__ (self, parent, battallion):
    QListViewItem.__init__ ( self, parent, battallion.name, "" )
 
    # store the battallion
    self.battallion = battallion

    # set the item to be open by default
    self.setOpen ( 1 )

    # set the displayed data
    self.update ()

 
  def getOrganization (self):
    """Returns the organization that this item contains. """
    return self.getBattallion ()


  def getBattallion (self):
    """Returns the battallion this item represents."""
    return self.battallion

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText ( 0, self.battallion.getName () )


###############################################################################################
class UnitItem (QListViewItem):
  """This class subclasses a normal QListViewItem to provide a common base class for all unit
  items."""

  def __init__ (self, parent):
    # init superclass
    QListViewItem.__init__ ( self, parent, unit )

   
###############################################################################################
class HeadquarterItem (UnitItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the company the item represents. The company can be retrieved using the 'getCompany()'
  method.""" 

  # icon
  icon = None

  def __init__ (self, parent, hq):
    QListViewItem.__init__ ( self, parent )
 
    # store the company
    self.company = hq

    # do we have an icon already?
    if not HeadquarterItem.icon:
      # nope, load it
      HeadquarterItem.icon = QPixmap ( '../gfx/editor/button-hq.png' )

    # set the displayed data
    self.update ()
    
 
  def getCompany (self):
    """Returns the company this item represents."""
    return self.company

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText   ( 0, self.company.getName () )
    self.setPixmap ( 1, HeadquarterItem.icon )
    self.setText   ( 2, str ( self.company.getMen () ) )


###############################################################################################
class InfantryItem (UnitItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the company the item represents. The company can be retrieved using the 'getCompany()'
  method.""" 

  # icon
  icon = None

  def __init__ (self, parent, company):
    QListViewItem.__init__ ( self, parent )
 
    # store the company
    self.company = company

    # do we have an icon already?
    if not InfantryItem.icon:
      # nope, load it
      InfantryItem.icon = QPixmap ( '../gfx/editor/button-head.png' )

    # set the displayed data
    self.update ()
    
 
  def getCompany (self):
    """Returns the company this item represents."""
    return self.company

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText   ( 0, self.company.getName () )
    self.setPixmap ( 1, InfantryItem.icon )
    self.setText   ( 2, str ( self.company.getMen () ) )

  
###############################################################################################
class CavalryItem (UnitItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the company the item represents. Th company can be retrieved using the 'getCompany()'
  method.""" 

  # icon
  icon = None
  
  def __init__ (self, parent, company):
    QListViewItem.__init__ ( self, parent )
 
    # store the company
    self.company = company
 
    # do we have an icon already?
    if not CavalryItem.icon:
      # nope, load it
      CavalryItem.icon = QPixmap ( '../gfx/editor/button-horse.png' )

    # set the displayed data
    self.update ()
 
  def getCompany (self):
    """Returns the company this item represents."""
    return self.company

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText   ( 0, self.company.getName () )
    self.setPixmap ( 1, CavalryItem.icon )
    self.setText   ( 2, str ( self.company.getMen () ) )

   
###############################################################################################
class ArtilleryItem (UnitItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the company the item represents. Th company can be retrieved using the 'getCompany()'
  method.""" 

  # icon
  icon = None
 
  def __init__ (self, parent, company):
    QListViewItem.__init__ ( self, parent )
 
    # store the company
    self.company = company
 
    # do we have an icon already?
    if not ArtilleryItem.icon:
      # nope, load it
      ArtilleryItem.icon = QPixmap ( '../gfx/editor/button-gun.png' )

    # set the displayed data
    self.update ()

 
  def getCompany (self):
    """Returns the company this item represents."""
    return self.company

 
  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""
    # set the displayed data
    self.setText   ( 0, self.company.getName () )
    self.setPixmap ( 1, ArtilleryItem.icon )
    self.setText   ( 2, str ( self.company.getMen () ) )

  

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

