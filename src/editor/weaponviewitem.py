###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from qt import *
import properties

###############################################################################################
class WeaponItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the weapon the item represents. The weapon can be retrieved using the 'getWeapon()'
  method."""

  
  def __init__ (self, parent, weapon ):
    # create the strings for the item
    name     = weapon.getName ()
    type     = weapon.getType ()
    range    = str ( weapon.getRange () )
    damage   = str ( weapon.getDamage () )
    accuracy = str ( weapon.getAccuracy () )
    
    QListViewItem.__init__ ( self, parent, name, type, range, damage, accuracy )

    # store the weapon
    self.weapon = weapon
 
 
  def getWeapon (self):
    """Returns the weapon this item represents."""
    return self.weapon


  def update (self):
    """Updates the labels for the item. Can be used if the weapon has changed."""

    # set the new labels
    self.setText ( 0, self.weapon.getName () )
    self.setText ( 1, self.weapon.getType () )
    self.setText ( 2, str ( self.weapon.getRange () ) )
    self.setText ( 3, str ( self.weapon.getDamage () ) )
    self.setText ( 4, str ( self.weapon.getAccuracy () ) )

 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

