###############################################################################################
# $Id$
###############################################################################################


import sys
import zipfile
import traceback
from qt import *

import scenario
import globals
import undostack
from map.hex              import Hex
from scenario_parser      import ScenarioParser
from scenario_writer      import ScenarioWriter
from editor.new_scenario  import NewScenario, createNewScenario
from mapview              import MapView
from iconview             import IconView
from unitview             import UnitView
from objectiveview        import ObjectiveView
from locationview         import LocationView
from blockview            import BlockView
from weaponview           import WeaponView
from scenarioview         import ScenarioView
from pluginview           import PluginView
from constants            import REBEL, UNION
from editor_map           import MapHeightException

##########################################################################################
class MainWindow (QMainWindow):
    """This is the main window of the application. Contains the palette of tools and the actual
    map. 
    """

    # menu id:s
    ITEM_NEW, ITEM_OPEN, ITEM_SAVE, ITEM_SAVE_AS, ITEM_QUIT, ITEM_VALIDATE = range (6)


    def __init__(self):
        QMainWindow.__init__(self, None, 'Civil Scenario Editor', Qt.WDestructiveClose)

        # create the menubar
        self.file = QPopupMenu(self)
        self.view = QPopupMenu(self)
        self.edit = QPopupMenu(self)
        self.tools = QPopupMenu(self)
        self.help = QPopupMenu(self)
        self.menuBar().insertItem ('&File', self.file)
        self.menuBar().insertItem ('&Edit', self.edit)
        self.menuBar().insertItem ('&View', self.view)
        self.menuBar().insertItem ('&Tools', self.tools)
        self.menuBar().insertSeparator ()
        self.menuBar().insertItem ('&Help', self.help)
        
        # create the menu items for 'File'
        self.file.insertItem ('&New',         self.new,      Qt.CTRL + Qt.Key_N, MainWindow.ITEM_NEW)
        self.file.insertItem ('&Open',        self.open,     Qt.CTRL + Qt.Key_O, MainWindow.ITEM_OPEN)
        self.file.insertItem ('&Save',        self.save,     Qt.CTRL + Qt.Key_S, MainWindow.ITEM_SAVE)
        self.file.insertItem ('Save &As',     self.saveAs,   Qt.CTRL + Qt.Key_A, MainWindow.ITEM_SAVE_AS)
        self.file.insertItem ('&Quit',        self.quit,     Qt.CTRL + Qt.Key_Q, MainWindow.ITEM_QUIT)

        
        # create the menu items for 'Edit'
        self.edit.insertItem ('&Clear',       self.clear )
        self.edit.insertItem ('&Undo',        self.undo, Qt.CTRL + Qt.Key_Z)
        
        # create the menu items for 'View'
        self.view.insertItem ('&Grid',        self.viewGrid, Qt.CTRL + Qt.Key_G)

        # create the menu items for 'Tools'
        self.tools.insertItem ('&Validate', self.validateScenario, Qt.CTRL + Qt.Key_V, MainWindow.ITEM_VALIDATE)

        # disable some entries for now
##         self.file.setItemEnabled  ( MainWindow.ITEM_SAVE,     0 )
##         self.file.setItemEnabled  ( MainWindow.ITEM_SAVE_AS,  0 )
##         self.tools.setItemEnabled ( MainWindow.ITEM_VALIDATE, 0 )
        
        # create the splitter and make it the main widget
        self.splitter = QSplitter(Qt.Horizontal, self, "main" )
        self.splitter.setFocus()
        self.setCentralWidget(self.splitter)
        
        # create the palette
        self.palette = QTabWidget (self.splitter)

        # and all its tabs
        self.iconview      = IconView ( self )
        self.unionview     = UnitView ( self, UNION )
        self.rebelview     = UnitView ( self, REBEL )
        self.objectiveview = ObjectiveView ( self )
        self.locationview  = LocationView ( self )
        self.blockview     = BlockView ( self )
        self.weaponview    = WeaponView ( self )
        self.scenarioview  = ScenarioView ( self )
        self.pluginview    = PluginView ( self )
        
        # add the tabs too
        self.palette.addTab ( self.iconview,      "Icons" )
        self.palette.addTab ( self.rebelview,     "Rebel units" )
        self.palette.addTab ( self.unionview,     "Union units" )
        self.palette.addTab ( self.scenarioview,  "Scenario info" ) 
        self.palette.addTab ( self.objectiveview, "Objectives" )
        self.palette.addTab ( self.locationview,  "Locations" )
        self.palette.addTab ( self.blockview,     "Blocks" )
        self.palette.addTab ( self.weaponview,    "Weapons" )
        self.palette.addTab ( self.pluginview,    "Plugins" )
        
        # disable all tabs
        self.palette.setDisabled ( 1 )
        
        # create the map view
        self.mapscrollview = QScrollView ( self.splitter )
        self.mapview = MapView ( self.mapscrollview, self )

        # store it globally too
        globals.mapview = self.mapview

        # create the canvas and store it for global access
        self.mapscrollview.addChild ( self.mapview , 0, 0 )

        self.connect ( self.rebelview,    PYSIGNAL('currentUnitChanged'), self.mapview, SLOT("repaint()"))
        self.connect ( self.unionview,    PYSIGNAL('currentUnitChanged'), self.mapview, SLOT("repaint()"))
        self.connect ( globals.mapview,   PYSIGNAL('mapClickedLeft'),     self.mapClickedLeft )
        self.connect ( globals.mapview,   PYSIGNAL('mapClickedMid'),      self.mapClickedMid )
        self.connect ( globals.mapview,   PYSIGNAL('mapClickedRight'),    self.mapClickedRight )
        self.connect ( globals.mapview,   PYSIGNAL('mapMove'),            self.mapMove )

        # we want to know when the date changes so that the weapons can be kept up to date
        self.connect ( self.scenarioview, PYSIGNAL('dateChanged'),        self.weaponview.refresh )
        
        # set a default save name
        self.filename = 'default.civil'
        
        # set a suitable caption
        self.setCaption ( 'Civil Scenario Editor' )
        self.statusBar().message ('Ready', 2000)

        # by default create a new scenario
        createNewScenario (self, 40, 40)

        self.show ()


    def new (self):
        """Callback for the menuitem 'File->New'. Asks the user wether a new scenario should be started
        and discards ny old if wanted."""

        # ask the user what to do
        result = QMessageBox.warning ( self, "New scenario", "Really create a new scenario?", "Yes", "No")

        # did we get a 1, which is the second button?
        if result == 1:
            # yep, don't quit
            return
    
        # create and show the dialog
        if NewScenario ( self ).exec_loop () == QDialog.Rejected:
            # nothing done, go away
            return

        # enable menu entries for saving
        self.file.setItemEnabled ( MainWindow.ITEM_SAVE,    1 )
        self.file.setItemEnabled ( MainWindow.ITEM_SAVE_AS, 1 )


    def open (self):
        """Callback for the menuitem 'File->Open'. Asks the user for the name of a scenario and
        loads it. Does not load the pickled LOS data, as it is enyway regenerated when saving."""
        # get filename
        filename = str ( QFileDialog.getOpenFileName ( None, "Civil scenarios (*.civil)", self ) )

        # did we get anything?
        if filename == None or filename == '':
            # no file, go away
            return

        # clean up the filename
        filename = str ( filename )

        try:
            # open the file as a zip file
            archive = zipfile.ZipFile ( filename )

            # get the main scenario file from the archive
            data = archive.read ( 'scenario.xml' )
            info = archive.read ( 'info.xml' )

            # note that we do not parse the los data at all, as it is anyway regenerated when saving
            
            # and close it
            archive.close ()

        except:
            # oops parsing failed
            QMessageBox.warning ( self, "Error loading file", "The scenario file %s is invalid." % filename, "&Ok" )

            # print stack trace
            print "-" * 75
            traceback.print_exc (file=sys.stdout)
            print "-" * 75

        # TODO
        #QMessageBox.warning ( self, "TODO", "TODO: should clear all data first!", "&Yeah" )
        
        try:
            # parse the file
            ScenarioParser ().parseString ( data )
            
            # store new filename
            self.filename = filename

            # add all destroyed units to all of the units. we can while designing have units with 0
            # men, and we should not mark those as 'destroyed', as the parser does. just add them
            scenario.info.units.update ( scenario.info.destroyed_units )

            # make sure the mapview  knows the new map 
            self.mapview.refresh ()

            # and the unit views need refreshing too
            self.unionview.refresh ()
            self.rebelview.refresh ()

            # objectives too
            self.objectiveview.refresh ()
            self.locationview.refresh ()

            # and the scenarioview should be reset too
            self.scenarioview.refresh ()

            # enable all tabs
            self.palette.setEnabled ( 1 )
 
            # enable menu entries for saving
            self.file.setItemEnabled ( MainWindow.ITEM_SAVE,    1 )
            self.file.setItemEnabled ( MainWindow.ITEM_SAVE_AS, 1 )
           
        except:
            # oops parsing failed
            QMessageBox.warning ( self, "Error loading file", "Failed to load the file %s" % filename, "&Ok" )

            # print stack trace
            print "-" * 75
            traceback.print_exc (file=sys.stdout)
            print "-" * 75

        globals.mapview.repaintVisible()


    def undo (self):
        """Peforms an undo."""
        undostack.undo()

 
    def save (self):
        """Callback for the menuitem 'File->Save'. Saves the current scenario with a default name or the
        one used when the scenario was saved using 'File->Save As'."""

        # assume the map is valid
        scenario.info.setValid ( 1 )
        
        # check the heights of the map to make sure all is ok. if the map is supposed to be finished
        # the user will want to know about the errors before saving
        try:
            scenario.map.calculateAbsoluteHeights ()
            
        except MapHeightException,e:
            # oops, we got errors,
            count = len (e.value)

            # make the error text
            text  = 'There seems to be %d height errors in the map. The map can not ' % count
            text += 'be used in the game before it is fully valid. Continue with save?' 

            # ask the user what to do
            result = QMessageBox.warning ( self, "Map height errors", text, "Yes", "No")

            # did we get a 1, which is the second button?
            if result == 1:
                # yep, don't save
                return

            # the map is not valid
            scenario.info.setValid ( 0 )

        # map is valid, so create the LOS map once and for all
        scenario.map.createLosMap ()
        
        # show message
        self.statusBar ().message ( 'Saving scenario to file ' + self.filename )

        # update the scenario info from the view first so that it's up-to-date in the cental
        # ScenarioInfo object too
        self.scenarioview.store ()

        # create a new scenario writer and write out the data with the scenario info 
        ScenarioWriter ().write ( self.filename )

        self.statusBar ().message ( 'Scenario saved', 3000 )
    
 
    def saveAs (self):
        """Callback for the menuitem 'File->Save As'. Saves the current scenario with a name that is
        asked from the user. Stores the used name as the new default name."""
    
        # get filename
        filename = str ( QFileDialog.getSaveFileName ( None, "Civil scenarios (*.civil)", self ) )

        # did we get anything?
        if filename == None or filename == '':
            # no file, go away
            return

        # clean up the filename
        self.filename = str ( filename )

        # call method for saving
        self.save ()
    
##         try:
##             # parse the file
##             ScenarioWriter ().write ( filename )

##             # store new filename
##             self.filename = filename
      
##         except:
##             # oops parsing failed
##             QMessageBox.warning ( self, "Error saving file", "Failed to save the file %s" % filename, "&Ok" )

##             # print stack trace
##             print "-" * 75
##             traceback.print_exc (file=sys.stdout)
##             print "-" * 75


 
    def quit (self):
        """Callback for the menuitem 'File->Quit'. Asks the player wether the application should be
        quit, and quits if wanted."""
        # ask the user what to do
        result = QMessageBox.warning ( self, "Really quit", "Really quit the application?", "Yes", "No")

        # did we get a 1, which is the second button?
        if result == 1:
            # yep, don't quit
            return
        
        # quit
        qApp.quit ()


    def viewGrid (self):
        """Callback for the menuitem 'View->Grid'. Toggles the visibility of the helper grid in the
        mapview."""
        self.mapview.toggleGrid ()


    def clear (self):
        """Callback for the menuitem 'Edit->Clear'. Asks the player wether the map should be cleared to
        the currently selected icon."""
        # ask the user what to do
        result = QMessageBox.warning ( self, "Really clear map", "Really clear the map and fill with " +
                                             "the current icon?", "Yes", "No")

        # did we get a 1, which is the second button? 
        if result == 1:
            # yep, don't do it
            return

        # do we have a selected icon?
        if not globals.icons.hasSelected ():
            return
    
        # get the size of the map
        sizex, sizey = scenario.map.getSize ()

        # get selected icon
        icon = globals.icons.getSelected ()
    
        # loop and set the new initial icon (or hex really) for the map
        for x in range (sizex):
            for y in range (sizey):
                scenario.map.getHexes () [y][x] = Hex ( x, y, icon )

        # repaint it all
        self.mapview.canvas.repaint ()
        # self.mapview.repaint ( 0, 0, self.mapview.width () - 1, self.mapview.height () - 1 )


    def validateScenario (self):
        """Validates the current scenario. Checks all the data in the scenario that must be valid
        for the scenario to be valid. Shows a summary to the user about the state of the
        scenario. Marks the map as valid or invalid depending on the result."""

        # all the views that are allowed to validate data
        views = ( ( self.iconview,      "Map" ),
                  ( self.rebelview,     "Rebel units" ),
                  ( self.unionview,     "Union units" ),
                  ( self.scenarioview,  "Scenario data" ), 
                  ( self.objectiveview, "Objectives" ),
                  ( self.locationview,  "Locations" ),
                  ( self.weaponview,    "Weapons" ) )

        # initial message
        report = ""

        # no errors yet
        errors = 0
        
        # loop over all views and validate them
        for view, name in views:
            # validate the view and get the result
            text = view.validate ()

            # do we have errors?
            if text != None:
                # add the text to the report
                report += '<p><b>%s</b><br/>\n%s</p>\n' % ( name, text )

                # we have errors now
                errors = 1

        # all modules traversed and validated, did we get any errors?
        if errors:
            # yep, so add a short blurb about that
            header  = "<qt><p>The scenario contains the following errors that must be fixed before the "
            header += "scenario can be used in <em>Civil</em>:</p>"

            # add the header
            report = header + report
            
            # errors, not valid
            scenario.info.setValid ( 0 )
        else:
            # no errors
            report = "<qt><p>The scenario contains no errors and can be used in <em>Civil</em>.</p></qt>"

            # no errors, ok
            scenario.info.setValid ( 1 )

        #add a footer to the report
        report += "</qt>"

        # show the report to the user
        QMessageBox.information ( self, "Validation summary", report, "&Ok" )


    def mapMove (self, x, y, hexx, hexy):
        """Callback triggered when the user drags the mouse in the map."""
        self.mapClickedLeft(x,y,hexx,hexy)


    def mapClickedLeft (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked."""

        # get current child tab
        child = self.palette.currentPage ()

        #try:
        # let the child handle the click
        child.mapClickedLeft ( x, y, hexx, hexy )
          
        #except:
        #    print "MainWindow.mapClickedLeft: child should handle mapClickedLeft():", child


    def mapClickedMid (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked."""

        # get current child tab
        child = self.palette.currentPage ()

        try:
            # let the child handle the click
            child.mapClickedMid ( x, y, hexx, hexy )

        except:
            print "MainWindow.mapClickedMid: child should handle mapClickedMid():", child


    def mapClickedRight (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked."""

        # get current child tab
        child = self.palette.currentPage ()

        try:
            # let the child handle the click
            child.mapClickedRight ( x, y, hexx, hexy )
          
        except:
            print "MainWindow.mapClickedRight: child should handle mapClickedRight():", child

        # repaint the map
        self.mapview.repaint ( x, y, 1, 1, 1 )
        # self.mapview.repaint ( 0 )


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

