###############################################################################################
# $Id$
###############################################################################################


import sys
from qt import *


class EditUnit (QDialog):

    def __init__(self,parent, unit):
        QDialog.__init__(self,parent, "edit unit", 1)

        # store the unit
        self.unit = unit
 
        # gory editor code
        self.resize(392,340)
        self.setCaption(self.tr('Edit unit information'))
        editunitLayout = QGridLayout(self)
        editunitLayout.setSpacing(6)
        editunitLayout.setMargin(11)

        self.TabWidget2 = QTabWidget(self,'TabWidget2')
        QToolTip.add(self.TabWidget2,self.tr('The name of the commander for the unit'))

        self.tab = QWidget(self.TabWidget2,'tab')
        tabLayout = QGridLayout(self.tab)
        tabLayout.setSpacing(6)
        tabLayout.setMargin(11)
        spacer = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout.addItem(spacer,0,1)

        self.name = QLineEdit(self.tab,'name')
        QToolTip.add(self.name,self.tr('The name of the unit'))

        tabLayout.addWidget(self.name,0,2)

        self.TextLabel1 = QLabel(self.tab,'TextLabel1')
        self.TextLabel1.setText(self.tr('Name:'))

        tabLayout.addWidget(self.TextLabel1,0,0)
        spacer_2 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout.addItem(spacer_2,5,1)

        self.morale = QComboBox(0,self.tab,'morale')
        self.morale.setSizePolicy(QSizePolicy(7,0,self.morale.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.morale,self.tr('Default morale for the unit'))

        tabLayout.addWidget(self.morale,4,2)

        self.TextLabel4 = QLabel(self.tab,'TextLabel4')
        self.TextLabel4.setText(self.tr('Morale:'))

        tabLayout.addWidget(self.TextLabel4,4,0)

        self.ComboBox5 = QComboBox(0,self.tab,'ComboBox5')
        self.ComboBox5.setSizePolicy(QSizePolicy(7,0,self.ComboBox5.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.ComboBox5,self.tr('Default fatigue for the unit'))

        tabLayout.addWidget(self.ComboBox5,6,2)

        self.TextLabel6 = QLabel(self.tab,'TextLabel6')
        self.TextLabel6.setText(self.tr('Fatigue:'))

        tabLayout.addWidget(self.TextLabel6,6,0)

        self.ComboBox4 = QComboBox(0,self.tab,'ComboBox4')
        self.ComboBox4.setSizePolicy(QSizePolicy(7,0,self.ComboBox4.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.ComboBox4,self.tr('Default experience for the unit'))

        tabLayout.addWidget(self.ComboBox4,5,2)
        spacer_3 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout.addItem(spacer_3,4,1)

        self.TextLabel5 = QLabel(self.tab,'TextLabel5')
        self.TextLabel5.setText(self.tr('Experience:'))

        tabLayout.addWidget(self.TextLabel5,5,0)
        spacer_4 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout.addItem(spacer_4,6,1)
        spacer_5 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Expanding)
        tabLayout.addItem(spacer_5,7,2)

        self.men = QSpinBox(self.tab,'SpinBox1')
        self.men.setSuffix(self.tr(''))
        self.men.setSpecialValueText(self.tr(''))
        self.men.setButtonSymbols(QSpinBox.UpDownArrows)
        self.men.setMaxValue(200)
        self.men.setMinValue(1)
        self.men.setValue( self.unit.getMen () )
        QToolTip.add(self.men,self.tr('The number of men in the unit'))

        tabLayout.addWidget(self.men,2,2)
        spacer_6 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.MinimumExpanding)
        tabLayout.addItem(spacer_6,3,2)

        self.TextLabel2_3 = QLabel(self.tab,'TextLabel2_3')
        self.TextLabel2_3.setText(self.tr('Men:'))

        tabLayout.addWidget(self.TextLabel2_3,2,0)
        spacer_7 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout.addItem(spacer_7,2,1)
        self.TabWidget2.insertTab(self.tab,self.tr('Basic info'))

        self.tab_2 = QWidget(self.TabWidget2,'tab_2')
        tabLayout_2 = QGridLayout(self.tab_2)
        tabLayout_2.setSpacing(6)
        tabLayout_2.setMargin(11)

        self.morale_2 = QComboBox(0,self.tab_2,'morale_2')
        self.morale_2.setSizePolicy(QSizePolicy(7,0,self.morale_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.morale_2,self.tr('The commander experience'))

        tabLayout_2.addWidget(self.morale_2,2,2)

        self.ComboBox4_2 = QComboBox(0,self.tab_2,'ComboBox4_2')
        self.ComboBox4_2.setSizePolicy(QSizePolicy(7,0,self.ComboBox4_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.ComboBox4_2,self.tr('The commander aggressiveness'))

        tabLayout_2.addWidget(self.ComboBox4_2,3,2)
        spacer_8 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_8,1,1)
        spacer_9 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_9,2,1)
        spacer_10 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_10,3,1)
        spacer_11 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_11,0,1)

        self.name_2 = QLineEdit(self.tab_2,'name_2')

        tabLayout_2.addWidget(self.name_2,0,2)

        self.type_2 = QComboBox(0,self.tab_2,'type_2')
        self.type_2.insertItem(self.tr('General'))
        self.type_2.insertItem(self.tr('Colonel'))
        self.type_2.insertItem(self.tr('Major'))
        self.type_2.insertItem(self.tr('Lieutnant'))
        self.type_2.insertItem(self.tr('Sergeant'))
        self.type_2.setSizePolicy(QSizePolicy(7,0,self.type_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.type_2,self.tr('The rank of the commander'))

        tabLayout_2.addWidget(self.type_2,1,2)

        self.ComboBox5_2 = QComboBox(0,self.tab_2,'ComboBox5_2')
        self.ComboBox5_2.setSizePolicy(QSizePolicy(7,0,self.ComboBox5_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.ComboBox5_2,self.tr('The commander rally skill'))

        tabLayout_2.addWidget(self.ComboBox5_2,4,2)

        self.ComboBox5_2_2 = QComboBox(0,self.tab_2,'ComboBox5_2_2')
        self.ComboBox5_2_2.setSizePolicy(QSizePolicy(7,0,self.ComboBox5_2_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.ComboBox5_2_2,self.tr('The commander motivation skill'))

        tabLayout_2.addWidget(self.ComboBox5_2_2,5,2)
        spacer_12 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Expanding)
        tabLayout_2.addItem(spacer_12,6,2)

        self.TextLabel6_2_2 = QLabel(self.tab_2,'TextLabel6_2_2')
        self.TextLabel6_2_2.setText(self.tr('Motivation:'))

        tabLayout_2.addWidget(self.TextLabel6_2_2,5,0)

        self.TextLabel6_2 = QLabel(self.tab_2,'TextLabel6_2')
        self.TextLabel6_2.setText(self.tr('Rally skill:'))

        tabLayout_2.addWidget(self.TextLabel6_2,4,0)

        self.TextLabel5_2 = QLabel(self.tab_2,'TextLabel5_2')
        self.TextLabel5_2.setText(self.tr('Aggressiveness:'))

        tabLayout_2.addWidget(self.TextLabel5_2,3,0)

        self.TextLabel4_2 = QLabel(self.tab_2,'TextLabel4_2')
        self.TextLabel4_2.setText(self.tr('Experience:'))

        tabLayout_2.addWidget(self.TextLabel4_2,2,0)

        self.TextLabel2_2 = QLabel(self.tab_2,'TextLabel2_2')
        self.TextLabel2_2.setText(self.tr('Rank:'))

        tabLayout_2.addWidget(self.TextLabel2_2,1,0)

        self.TextLabel1_2 = QLabel(self.tab_2,'TextLabel1_2')
        self.TextLabel1_2.setText(self.tr('Name:'))

        tabLayout_2.addWidget(self.TextLabel1_2,0,0)
        spacer_13 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_13,4,1)
        spacer_14 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_2.addItem(spacer_14,5,1)
        self.TabWidget2.insertTab(self.tab_2,self.tr('Commander'))

        self.tab_3 = QWidget(self.TabWidget2,'tab_3')
        tabLayout_3 = QGridLayout(self.tab_3)
        tabLayout_3.setSpacing(6)
        tabLayout_3.setMargin(11)

        self.TextLabel1_2_2 = QLabel(self.tab_3,'TextLabel1_2_2')
        self.TextLabel1_2_2.setText(self.tr('Type:'))

        tabLayout_3.addWidget(self.TextLabel1_2_2,0,0)

        self.TextLabel1_2_2_2 = QLabel(self.tab_3,'TextLabel1_2_2_2')
        self.TextLabel1_2_2_2.setText(self.tr('Number:'))

        tabLayout_3.addWidget(self.TextLabel1_2_2_2,1,0)

        self.morale_2_2 = QComboBox(0,self.tab_3,'morale_2_2')
        self.morale_2_2.setSizePolicy(QSizePolicy(7,0,self.morale_2_2.sizePolicy().hasHeightForWidth()))
        QToolTip.add(self.morale_2_2,self.tr('The main type of weapon for the unit'))

        tabLayout_3.addWidget(self.morale_2_2,0,2)
        spacer_15 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Expanding)
        tabLayout_3.addItem(spacer_15,2,2)
        spacer_16 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_3.addItem(spacer_16,0,1)
        spacer_17 = QSpacerItem(40,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        tabLayout_3.addItem(spacer_17,1,1)

        self.weaponcount = QSpinBox(self.tab_3,'weaponcount')
        self.weaponcount.setSuffix(self.tr(''))
        self.weaponcount.setSpecialValueText(self.tr(''))
        self.weaponcount.setButtonSymbols(QSpinBox.UpDownArrows)
        self.weaponcount.setMaxValue(300)
        self.weaponcount.setMinValue(1)
        self.weaponcount.setValue(100)
        QToolTip.add(self.weaponcount,self.tr('Number of weapons of the above type'))

        tabLayout_3.addWidget(self.weaponcount,1,2)
        self.TabWidget2.insertTab(self.tab_3,self.tr('Weapon'))

        editunitLayout.addWidget(self.TabWidget2,0,0)

        Layout2 = QHBoxLayout()
        Layout2.setSpacing(6)
        Layout2.setMargin(0)
        spacer_18 = QSpacerItem(20,20,QSizePolicy.Expanding,QSizePolicy.Minimum)
        Layout2.addItem(spacer_18)

        self.okbutton = QPushButton(self,'ok')
        self.okbutton.setText(self.tr('&Ok'))
        Layout2.addWidget(self.okbutton)
        spacer_19 = QSpacerItem(20,20,QSizePolicy.Fixed,QSizePolicy.Minimum)
        Layout2.addItem(spacer_19)

        self.cancelbutton = QPushButton(self,'cancel')
        self.cancelbutton.setText(self.tr('&Cancel'))
        Layout2.addWidget(self.cancelbutton)

        editunitLayout.addLayout(Layout2,1,0)

        # populate all info into the widgets
        self.populate ()
        
        # connetc signals to slots
        self.connect ( self.okbutton,     SIGNAL('clicked()'), self.ok )
        self.connect ( self.cancelbutton, SIGNAL('clicked()'), self.cancel )


    def populate (self):
        """Populates the dialog with data from the unit. This is a separate unit so that it can be
        easily picked out and improved."""
        # basic data
        self.name.setText ( self.unit.getName () )
        
        
    def ok (self):
        """Callback triggered when the user clicks the 'Ok' button. This will apply the changes to
        the current unit and then close the dialog."""
        # set all passed data
        self.unit.setName ( str ( self.name.text () ) )
        self.unit.setMen  ( int ( self.men.value () ) )

        # all is ok
        self.accept ()
        

    def cancel (self):
        """Callback triggered when the user clicks the 'Cancel' button. This will simply close the
        dialog and abandon all changes that were made."""
        self.reject ()

    
if __name__ == '__main__':
    a = QApplication(sys.argv)
    QObject.connect(a,SIGNAL('lastWindowClosed()'),a,SLOT('quit()'))
    w = editunit()
    a.setMainWidget(w)
    w.exec_loop()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
