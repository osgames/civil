###############################################################################################
# $Id$
###############################################################################################


import sys
from qt import *
import traceback

import scenario
from map.map       import Map
from scenario_info import ScenarioInfo
from map.hex       import Hex
from organization  import REBEL, UNION

##########################################################################################
class UserInfo (QWidget):
  """This class provides a widget containing the information about the user creating the
  scenario. Information such as email addresses and urls are provided too."""

  def __init__ (self, parent):
    QWidget.__init__ ( self, parent )

    # create the overall info label
    self.info = QLabel ( "Enter personal information here. This information is stored in\n" +
                         "the scenario file. The information is not mandatory, and you\n" +
                         "may leave it blank to remain anonymous.", self )

    # create the labels
    self.namelabel    = QLabel ( "Name: ", self )
    self.emaillabel   = QLabel ( "Email: ", self )
    self.urllabel     = QLabel ( "URL: ", self )
    self.commentlabel = QLabel ( "Comment: ", self )

    # create other components
    self.name    = QLineEdit ( "", self )
    self.email   = QLineEdit ( "", self )
    self.url     = QLineEdit ( "", self )
    self.comment = QMultiLineEdit ( self )

    # create the layout
    self.layout = QGridLayout (self, 5, 2, 10, 10 )

    # add them to the layout
    self.layout.addMultiCellWidget ( self.info, 0, 0, 0, 1)
    self.layout.addWidget ( self.namelabel, 1, 0 )
    self.layout.addWidget ( self.name, 1, 1 )
    self.layout.addWidget ( self.emaillabel, 2, 0 )
    self.layout.addWidget ( self.email, 2, 1 )
    self.layout.addWidget ( self.urllabel, 3, 0 )
    self.layout.addWidget ( self.url, 3, 1 )
    self.layout.addWidget ( self.commentlabel, 4, 0 )
    self.layout.addWidget ( self.comment, 4, 1 )


##########################################################################################
class MissionInfo (QWidget):
  """This class provides a widget containing the information about the player's missions. This
  information is meant to be shown to only one of the players, giving some extra briefing in
  addition to the more general 'description' info.."""

  def __init__ (self, parent):
    QWidget.__init__ ( self, parent )

    # create the overall info label
    self.unionlabel = QLabel ( "Mission information for the Union player", self )
    self.rebellabel = QLabel ( "Mission information for the Confederate player", self )

    # create other components
    self.unionmission = QMultiLineEdit ( self )
    self.rebelmission = QMultiLineEdit ( self )

    # create the layout
    self.layout = QVBoxLayout  (self, 10, 10 )

    # add them to the layout
    self.layout.addWidget ( self.unionlabel )
    self.layout.addWidget ( self.unionmission )
    self.layout.addWidget ( self.rebellabel )
    self.layout.addWidget ( self.rebelmission )

    # populate the union mission. Loop over all paragrahs
    for para in scenario.info.getMission ( UNION ):
      self.unionmission.insertLine ( para )

    # populate the rebel mission. Loop over all paragrahs
    for para in scenario.info.getMission ( REBEL ):
      self.rebelmission.insertLine ( para )


##########################################################################################
class BaseInfo (QWidget):
  """This class provides a widget containing the basic info about the scenario."""

  def __init__ (self, parent):
    QWidget.__init__ ( self, parent )

    # create the overall info label
    self.info = QLabel ( "Enter basic information about the scenario here. The number of\n" +
                         "turns is important as it gives the maximum length in game\n" +
                         "minutes of the scenario.", self )

    # create the labels
    self.namelabel        = QLabel ( "Name: ", self )
    self.descriptionlabel = QLabel ( "Description: ", self )
    self.locationlabel    = QLabel ( "Location: ", self )
    self.turnslabel       = QLabel ( "Turns: ", self )
    self.datelabel        = QLabel ( "Date: ", self )
    self.sizelabel        = QLabel ( "Map size (x/y): ", self )

    # create other components
    self.name        = QLineEdit ( scenario.info.getName (), self )
    self.turns       = QLineEdit ( str (scenario.info.getMaxTurns ()), self )
    self.description = QMultiLineEdit ( self )
    self.location    = QLineEdit ( scenario.info.getLocation (), self )
    self.year        = QComboBox ( self )
    self.month       = QComboBox ( self )
    self.day         = QComboBox ( self )
    self.hour        = QComboBox ( self )
    self.minute      = QComboBox ( self )
    self.x           = QSpinBox  ( 10, 300, 1, self )
    self.y           = QSpinBox  ( 10, 300, 1, self )
                                  
    # add items to the combos
    for item in ( '1861', '1862', '1863', '1864' ):
      self.year.insertItem ( item )

    for item in ( range ( 1, 13) ):
      self.month.insertItem ( str ( item)  )

    for item in ( range ( 1, 32) ):
      self.day.insertItem ( str ( item)  )

    for item in ( range ( 0, 24 ) ):
      self.hour.insertItem ( str ( item)  )

    for item in ( range ( 0,60 ) ):
      self.minute.insertItem ( str ( item)  )

    # populate the description. Loop over all paragrahs
    for para in scenario.info.getDescription ():
      self.description.insertLine ( para )
          
    # create the layout
    self.layout = QGridLayout (self, 6, 6, 10, 10 )

    # add them to the layout
    self.layout.addMultiCellWidget ( self.info, 0, 0, 0, 5 )
    self.layout.addWidget ( self.namelabel, 1, 0 )
    self.layout.addMultiCellWidget ( self.name, 1, 1, 1, 5 )
    self.layout.addWidget ( self.descriptionlabel, 2, 0 )
    self.layout.addMultiCellWidget ( self.description, 2, 2, 1, 5 )
    self.layout.addWidget ( self.locationlabel, 3, 0 )
    self.layout.addMultiCellWidget ( self.location, 3, 3, 1, 5 )
    self.layout.addWidget ( self.turnslabel, 4, 0 )
    self.layout.addWidget ( self.turns, 4, 1 )
    self.layout.addWidget ( self.datelabel, 4, 0 )
    self.layout.addWidget ( self.year, 4, 1 )
    self.layout.addWidget ( self.month, 4, 2 )
    self.layout.addWidget ( self.day, 4, 3 )
    self.layout.addWidget ( self.hour, 4, 4 )
    self.layout.addWidget ( self.minute, 4, 5 )
    self.layout.addWidget ( self.sizelabel, 5, 0 )
    self.layout.addWidget ( self.x, 5, 1 )
    self.layout.addWidget ( self.y, 5, 2 )
    

##########################################################################################
class EditScenario (QTabDialog):
  """This class presents a simple dialog where properties for a new scenario can be set."""

  def __init__ (self, parent):
    QTabDialog.__init__ ( self, parent, "EditPlaylistDialog", 1)

    # store the info if we have it
    if scenario.info == None:
      # no info, create a new one
      scenario.info = ScenarioInfo ()
      scenario.info.setName ( "Scenario name" )
      scenario.info.setDescription ( ["Description of the scenario "] )
      scenario.info.setLocation ( "Location of scenario" )
      scenario.info.setMaxTurns ( 1000 )
      scenario.info.setDate ( ( 1862, 1, 1, 12, 0 ) )


    # create the tab widgets
    self.baseinfo    = BaseInfo ( self )
    self.missioninfo = MissionInfo ( self )
    self.userinfo    = UserInfo ( self )
    
    # add tabs
    self.addTab ( self.baseinfo,    "Basic information" )
    self.addTab ( self.missioninfo, "Missions" )
    self.addTab ( self.userinfo,     "Author" )

    # we have ok and cancel buttons
    self.setOkButton ()
    self.setCancelButton ()
    
    # connect signals fromt he buttons
    QObject.connect ( self, SIGNAL('applyButtonPressed()'), self.ok )
    QObject.connect ( self, SIGNAL('cancelButtonPressed()'), self.reject )
    
    # set a nice caption
    self.setCaption ( "Edit scenario" )


  def ok (self):
    """Callback triggered when the button ok is clicked. Validates the info before
    closing the dialog."""
    # get all data
    name         = str ( self.baseinfo.name.text () )
    description  = str ( self.baseinfo.description.text () )
    unionmission = str ( self.missioninfo.unionmission.text () )
    rebelmission = str ( self.missioninfo.rebelmission.text () )
    description  = str ( self.baseinfo.description.text () )
    location     = str ( self.baseinfo.location.text () )
    turns        = int ( str ( self.baseinfo.turns.text () ) )
    year         = int ( str ( self.baseinfo.year.currentText () ) )
    month        = int ( str ( self.baseinfo.month.currentText () ) )
    day          = int ( str ( self.baseinfo.day.currentText () ) )
    hour         = int ( str ( self.baseinfo.hour.currentText () ) )
    minute       = int ( str ( self.baseinfo.minute.currentText () ) )
    x            = int ( str ( self.baseinfo.x.text () ) )
    y            = int ( str ( self.baseinfo.y.text () ) )


    # validate data
    if name == '' or turns < 10 or location == '' or description == '':
      # oops, illegal or missing data
      QMessageBox.warning ( self, "Illegal data", "Illegal or missing data, please check.", "&Ok" )
      return

    # set the scenario info
    scenario.info.setName ( name )
    scenario.info.setDescription ( [description] )
    scenario.info.setMission ( UNION, [unionmission] )
    scenario.info.setMission ( REBEL, [rebelmission] )
    scenario.info.setLocation ( location )
    scenario.info.setMaxTurns ( turns )
    scenario.info.setDate ( ( year, month, day, hour, minute ) )

    # create the map
    scenario.map = Map ( x, y )

    # loop and set initial icons for the map
    for index_x in range (x):
      for index_y in range (y):
        scenario.map.getHexes () [index_x][index_y] = Hex ( index_x, index_y, 3 )
        
    # accept ourselves
    self.accept ()

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

