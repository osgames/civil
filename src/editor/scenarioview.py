###############################################################################################
# $Id$
###############################################################################################


from qt import *


import scenario
from constants  import REBEL, UNION

class ScenarioView(QWidget):
    """This view is used as a general view for all misc scenario info. It lets the user change data
    such as the scenario name, description, date, missions etc."""

    def __init__(self,parent):
        QWidget.__init__(self, parent)

        self.setCaption(self.tr('Form1'))
        ScenarioViewLayout = QVBoxLayout(self)
        ScenarioViewLayout.setSpacing(10)
        ScenarioViewLayout.setMargin(11)

        self.GroupBox1 = QGroupBox(self,'GroupBox1')
        self.GroupBox1.setTitle(self.tr('Basic information'))
        self.GroupBox1.setColumnLayout(0,Qt.Vertical)
        self.GroupBox1.layout().setSpacing(0)
        self.GroupBox1.layout().setMargin(0)
        GroupBox1Layout = QGridLayout(self.GroupBox1.layout())
        GroupBox1Layout.setAlignment(Qt.AlignTop)
        GroupBox1Layout.setSpacing(10)
        GroupBox1Layout.setMargin(11)

        self.TextLabel1 = QLabel(self.GroupBox1,'TextLabel1')
        self.TextLabel1.setText(self.tr('Name:'))

        GroupBox1Layout.addWidget(self.TextLabel1,0,0)

        self.TextLabel2 = QLabel(self.GroupBox1,'TextLabel2')
        self.TextLabel2.setText(self.tr('Description:'))

        GroupBox1Layout.addWidget(self.TextLabel2,1,0)

        self.TextLabel3 = QLabel(self.GroupBox1,'TextLabel3')
        self.TextLabel3.setText(self.tr('Geographic location:'))

        GroupBox1Layout.addWidget(self.TextLabel3,2,0)

        self.description = QMultiLineEdit(self.GroupBox1,'description')
        self.description.setWordWrap ( QMultiLineEdit.WidgetWidth )
        # Lame QT 2.3/3.0 compatibility
        
        try:
            wordwrap = QMultiLineEdit.AtWhiteSpace
        except:
            pass
        
        try:
            wordwrap = QMultiLineEdit.AtWordBoundary
        except:
            pass
        
        self.description.setWrapPolicy ( wordwrap )
        
        QToolTip.add(self.description,self.tr('Short description of what the scenario is about'))

        GroupBox1Layout.addMultiCellWidget(self.description,1,1,1,4)

        self.name = QLineEdit(self.GroupBox1,'name')
        QToolTip.add(self.name,self.tr('Name of the scenario'))

        GroupBox1Layout.addMultiCellWidget(self.name,0,0,1,4)

        self.TextLabel4 = QLabel(self.GroupBox1,'TextLabel4')
        self.TextLabel4.setText(self.tr('Date:'))

        GroupBox1Layout.addWidget(self.TextLabel4,3,0)

        self.month = QSpinBox(self.GroupBox1,'month')
        self.month.setMaxValue(12)
        self.month.setMinValue(1)
        QToolTip.add(self.month,self.tr('Month of the battle'))

        GroupBox1Layout.addWidget(self.month,3,2)

        self.minute = QSpinBox(self.GroupBox1,'minute')
        self.minute.setMaxValue(59)
        QToolTip.add(self.minute,self.tr('Minute of the battle'))

        GroupBox1Layout.addWidget(self.minute,4,2)

        self.hour = QSpinBox(self.GroupBox1,'hour')
        self.hour.setMaxValue(23)
        QToolTip.add(self.hour,self.tr('Hour of the battle'))

        GroupBox1Layout.addWidget(self.hour,4,1)

        self.TextLabel6 = QLabel(self.GroupBox1,'TextLabel6')
        self.TextLabel6.setText(self.tr('Time:'))

        GroupBox1Layout.addWidget(self.TextLabel6,4,0)
        spacer = QSpacerItem(20,20,QSizePolicy.Expanding,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer,3,4)

        self.year = QSpinBox(self.GroupBox1,'year')
        self.year.setMaxValue(1865)
        self.year.setMinValue(1861)
        self.year.setValue(1861)
        QToolTip.add(self.year,self.tr('Year of the battle'))

        GroupBox1Layout.addWidget(self.year,3,1)

        self.location = QLineEdit(self.GroupBox1,'location')
        QToolTip.add(self.location,self.tr('Name of the place for the battle'))

        GroupBox1Layout.addMultiCellWidget(self.location,2,2,1,4)

        self.day = QSpinBox(self.GroupBox1,'day')
        self.day.setMaxValue(31)
        self.day.setMinValue(1)
        QToolTip.add(self.day,self.tr('Day of the battle'))

        GroupBox1Layout.addWidget(self.day,3,3)

        self.TextLabel1_2 = QLabel(self.GroupBox1,'TextLabel1_2')
        self.TextLabel1_2.setText(self.tr('Turns:'))

        GroupBox1Layout.addWidget(self.TextLabel1_2,5,0)

        self.turns = QSpinBox(self.GroupBox1,'turns')
        self.turns.setMaxValue(999)
        self.turns.setMinValue(1)
        QToolTip.add(self.turns,self.tr('Number of turns in the scenario'))

        GroupBox1Layout.addWidget(self.turns,5,1)
        spacer_2 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer_2,5,2)
        spacer_3 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer_3,4,3)
        spacer_4 = QSpacerItem(20,20,QSizePolicy.Minimum,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer_4,5,3)
        spacer_5 = QSpacerItem(20,20,QSizePolicy.Expanding,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer_5,4,4)
        spacer_6 = QSpacerItem(20,20,QSizePolicy.Expanding,QSizePolicy.Minimum)
        GroupBox1Layout.addItem(spacer_6,5,4)
        ScenarioViewLayout.addWidget(self.GroupBox1)

        self.GroupBox2 = QGroupBox(self,'GroupBox2')
        self.GroupBox2.setTitle(self.tr('Missions'))
        self.GroupBox2.setColumnLayout(0,Qt.Vertical)
        self.GroupBox2.layout().setSpacing(0)
        self.GroupBox2.layout().setMargin(0)
        GroupBox2Layout = QVBoxLayout(self.GroupBox2.layout())
        GroupBox2Layout.setAlignment(Qt.AlignTop)
        GroupBox2Layout.setSpacing(10)
        GroupBox2Layout.setMargin(11)

        self.TextLabel7 = QLabel(self.GroupBox2,'TextLabel7')
        self.TextLabel7.setText(self.tr('Union mission information:'))
        GroupBox2Layout.addWidget(self.TextLabel7)

        self.unionmission = QMultiLineEdit(self.GroupBox2,'unionmission')
        self.unionmission.setWordWrap ( QMultiLineEdit.WidgetWidth )
        self.unionmission.setWrapPolicy ( wordwrap )
        QToolTip.add(self.unionmission,self.tr('Description about what the union player should accomplish'))
        GroupBox2Layout.addWidget(self.unionmission)

        self.TextLabel7_2 = QLabel(self.GroupBox2,'TextLabel7_2')
        self.TextLabel7_2.setText(self.tr('Rebel mission information::'))
        GroupBox2Layout.addWidget(self.TextLabel7_2)

        self.rebelmission = QMultiLineEdit(self.GroupBox2,'rebelmission')
        self.rebelmission.setWordWrap ( QMultiLineEdit.WidgetWidth )
        self.rebelmission.setWrapPolicy ( wordwrap )
        QToolTip.add(self.rebelmission,self.tr('Description about what the rebel player should accomplish'))
        GroupBox2Layout.addWidget(self.rebelmission)
        ScenarioViewLayout.addWidget(self.GroupBox2)

        self.GroupBox3 = QGroupBox(self,'GroupBox3')
        self.GroupBox3.setTitle(self.tr('Author info'))
        self.GroupBox3.setColumnLayout(0,Qt.Vertical)
        self.GroupBox3.layout().setSpacing(0)
        self.GroupBox3.layout().setMargin(0)
        GroupBox3Layout = QGridLayout(self.GroupBox3.layout())
        GroupBox3Layout.setAlignment(Qt.AlignTop)
        GroupBox3Layout.setSpacing(10)
        GroupBox3Layout.setMargin(11)

        self.TextLabel8 = QLabel(self.GroupBox3,'TextLabel8')
        self.TextLabel8.setText(self.tr('Name:'))

        GroupBox3Layout.addWidget(self.TextLabel8,0,0)

        self.TextLabel9 = QLabel(self.GroupBox3,'TextLabel9')
        self.TextLabel9.setText(self.tr('Email:'))

        GroupBox3Layout.addWidget(self.TextLabel9,1,0)

        self.authorname = QLineEdit(self.GroupBox3,'authorname')
        QToolTip.add(self.authorname,self.tr('Name of the scenario author'))

        GroupBox3Layout.addWidget(self.authorname,0,1)

        self.email = QLineEdit(self.GroupBox3,'email')
        QToolTip.add(self.email,self.tr('Email of the scenario author'))

        GroupBox3Layout.addWidget(self.email,1,1)

        self.TextLabel11 = QLabel(self.GroupBox3,'TextLabel11')
        self.TextLabel11.setText(self.tr('Comment:'))

        GroupBox3Layout.addWidget(self.TextLabel11,3,0)

        self.comment = QMultiLineEdit(self.GroupBox3,'comment')
        QToolTip.add(self.comment,self.tr('Comment about the scenario'))

        GroupBox3Layout.addWidget(self.comment,3,1)

        self.TextLabel10 = QLabel(self.GroupBox3,'TextLabel10')
        self.TextLabel10.setText(self.tr('URL:'))

        GroupBox3Layout.addWidget(self.TextLabel10,2,0)

        self.url = QLineEdit(self.GroupBox3,'url')
        QToolTip.add(self.url,self.tr('Url to a homepage for the scenario'))

        GroupBox3Layout.addWidget(self.url,2,1)
        ScenarioViewLayout.addWidget(self.GroupBox3)

        # make sure we know of changes
        self.connect ( self.year,  SIGNAL('valueChanged(int)'), self.dateChanged )
        self.connect ( self.month, SIGNAL('valueChanged(int)'), self.dateChanged )


    def dateChanged (self):
        """This callback is triggered when the user changes the year or the month of the
        scenario. It will set the new date and then emit a signal telling the rest of the
        application that we have a new date. """
        # get the date data
        year   = self.year.value ()
        month  = self.month.value () 
        day    = self.day.value () 
        hour   = self.hour.value ()
        minute = self.minute.value ()
        second = 0
        
        # set the new changed date
        scenario.info.setDate ( datetime.datetime ( year, month, day, hour, minute, second ) )

        # let the world know that we have a new date
        self.emit (PYSIGNAL('dateChanged'), () )


    def refresh (self):
        """Refreshes all the scenario info data. This method should be used when a new scenario has
        been loaded or created. It will get all data from the central ScenarioInfo instance and
        populate this widget with it.""" 

        # set all simple data
        self.name.setText     ( scenario.info.getName () )
        self.location.setText ( scenario.info.getLocation () )
        self.turns.setValue   ( scenario.info.getMaxTurns () )

        # set the date
        year, month, day, hour, minute = scenario.info.getStartDate ()
        self.year.setValue   ( year )
        self.month.setValue  ( month )
        self.day.setValue    ( day )
        self.hour.setValue   ( hour )
        self.minute.setValue ( minute )

        self.description.clear()
        self.unionmission.clear()
        self.rebelmission.clear()
        
        # loop over all paragraphs and set the description
        for para in scenario.info.getDescription ():
            # add the line, followed by an empty line
            self.description.insertLine ( para )
            self.description.insertLine ( "" )
        
        # loop over all paragraphs and set the union mission
        for para in scenario.info.getMission ( UNION ):
            # add the line, followed by an empty line
            self.unionmission.insertLine ( para )
            self.unionmission.insertLine ( "" )
            
        # loop over all paragraphs and set the rebel mission
        for para in scenario.info.getMission ( REBEL ):
            # add the line, followed by an empty line
            self.rebelmission.insertLine ( para )
            self.rebelmission.insertLine ( "" )


    def store (self):
        """Stores all data from the controls of this view in the global object for scenario info
        data. This method should be called before a scenario is saved. """

        # get all data
        name         = str ( self.name.text () )
        description  = str ( self.description.text () )
        unionmission = str ( self.unionmission.text () )
        rebelmission = str ( self.rebelmission.text () )
        location     = str ( self.location.text () )
        turns        = self.turns.value ()
        year         = self.year.value ()
        month        = self.month.value () 
        day          = self.day.value () 
        hour         = self.hour.value ()
        minute       = self.minute.value ()
        second       = 0
        
        # TODO: should we validate the data too, or just assume it's ok?

        # set the scenario info
        scenario.info.setName ( name )
        scenario.info.setDescription ( [description] )
        scenario.info.setMission ( UNION, [unionmission] )
        scenario.info.setMission ( REBEL, [rebelmission] )
        scenario.info.setLocation ( location )
        scenario.info.setMaxTurns ( turns )
        scenario.info.setDate ( datetime.datetime ( year, month, day, hour, minute, second ) )
 

    def validate (self):
        """Validates the part of the scenario that this view is repsonsible for creating. Returns a
        free text report that indicates the validation result or None if all is ok. """

        # the default empty text
        text = ''
        
        # check the name
        if str ( self.name.text () ).strip () == '':
            # no name given
            text += 'the scenario has no name.<br/>'

        # check the description
        if str ( self.description.text () ).strip () == '':
            # no description given
            text += 'the scenario has no description.<br/>'

        # check the location
        if str ( self.location.text () ).strip () == '':
            # no location given
            text += 'the scenario has no location.<br/>'


        # check the union mission
        if str ( self.unionmission.text () ).strip () == '':
            # no union mission given
            text += 'the scenario has no mission for the Union player.<br/>'

        # check the rebel mission
        if str ( self.rebelmission.text () ).strip () == '':
            # no rebel mission given
            text += 'the scenario has no mission for the Rebel player.<br/>'

        # did we get any errors?
        if text == '':
            # no errors
            return None

        # we have something to report on
        return text
    
            
# Local Variables:
# mode: auto-fill
# fill-column: 100
# End:

