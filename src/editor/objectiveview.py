###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from qt import *
import properties
import scenario
import globals
from objective         import Objective
from objectiveviewitem import ObjectiveItem
from edit_objective    import EditObjective

class ObjectiveView (QListView):
    """This class..."""
    
    # a static next id
    nextid = 0

    # menu items
    NEW    = 10
    EDIT   = 11
    DELETE = 12
    
    def __init__(self, parent ):
        """Initializes the instance."""

        QListView.__init__( self, parent)
        
        # add the columns
        self.addColumn ('Name')
        self.addColumn ('Points')
        self.addColumn ('Owner')
 
        # single selection and not decorated root
        self.setMultiSelection ( 0 )
        self.setRootIsDecorated ( 0 )
 
        # create the popup menu
        self.popup = QPopupMenu ( self )

        self.popup.insertItem ( 'New',    self.new,    Qt.CTRL + Qt.Key_N, ObjectiveView.NEW )
        self.popup.insertItem ( 'Edit',   self.edit,   Qt.CTRL + Qt.Key_E, ObjectiveView.EDIT )
        self.popup.insertItem ( 'Delete', self.delete, Qt.CTRL + Qt.Key_D, ObjectiveView.DELETE )


    def refresh (self):
        """Refreshes all objectives by clearing the list and regenerating it."""
        # clear first
        self.clear ()

        # loop over all objectives
        for objective in scenario.info.objectives:
            # create a new item
            ObjectiveItem ( self, objective )
            

    def new (self):
        """Callback triggered when the user chooses 'New' from the popup menu. Will create a new
        objective and add it to the global datastructures."""
        # create a new objective
        objective = Objective ( ObjectiveView.nextid, None )

        # increment the id
        ObjectiveView.nextid += 1
 
        # create a new item for the listview
        item = ObjectiveItem ( self, objective )

        # add the objective to the global data
        scenario.info.objectives.append ( objective )
        
 
    def delete (self):
        """Callback triggered when the user chooses 'Delete' from the popup menu. Will delete the
        currently selected objective."""
        # what do we have under the mouse cursor?
        item = self.selectedItem ()

        # did we get any item?
        if item == None:
            # nothing here
            return

        # get the deleted objective
        objective = item.getObjective ()

        # remove the objective from the global data
        scenario.info.objectives.remove ( objective )

        # update display too
        self.takeItem ( item )
         

    def edit (self):
        """Callback triggered when the user chooses 'Edit' from the popup menu. This method will
        bring up a dialog  where the properties of the selected objective can be edited."""
        # get the current item and the company
        current = self.selectedItem ()
        objective = current.getObjective ()
        
        # create and show the dialog
        if not EditObjective ( self, objective ).exec_loop ():
            # dialog was cancelled
            return

        # update the visualized data
        current.update ()


    def contentsMousePressEvent (self, event):
        """Callback handling the fact that the user has pressed some mouse button. shows the menu on the
        right button."""

        # is this the right button?
        if event.button () != Qt.RightButton:
            # nope, perform normal stuff
            QListView.contentsMousePressEvent (self, event );
            return
        
        # what do we have under the mouse cursor?
        item = self.selectedItem ()

        # did we get any item?
        if item == None:
            # no item, so the listview is empty, disable all items that should not be active
            self.popup.setItemEnabled ( ObjectiveView.EDIT, 0 ) 
            self.popup.setItemEnabled ( ObjectiveView.DELETE, 0 ) 

        else:
            # an item is selected, enable the items
            self.popup.setItemEnabled ( ObjectiveView.EDIT, 1 ) 
            self.popup.setItemEnabled ( ObjectiveView.DELETE, 1 ) 
        
        # show the popup
        self.popup.move ( event.globalPos () )
        self.popup.show ()


    def contentsMouseReleaseEvent (self, event):
        """Callback handling the fact that the user has released a mouse button. Hides the menu on the
        right button."""

        # is this the right button?
        if event.button () == Qt.RightButton:
            # just hide the popup
            self.popup.hide ()

        else:
            # perform normal stuff
            QListView.contentsMouseReleaseEvent (self, event );


    def mapClickedLeft (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked. Sets an objective at the map position. It
        gets the currently selected objective (if any) and changes its position to that of the clicked hex."""

        print "ObjectiveView.mapClickedLeft"
        
        # what do we have under the mouse cursor?
        current = self.selectedItem ()

        # did we get any item?
        if current == None:
            # no objective selected, go away
            return

        # get the objective from the item
        objective = current.getObjective ()

        oldpos = objective.getPosition()

        # set the new position for it
        objective.setPosition ( ( x - 16, y - 16 ) )

        # gfx updates
        globals.mapview.paintEvent(QPaintEvent(QRect(oldpos[0], oldpos[1], 60, 60)))
        globals.mapview.paintEvent(QPaintEvent(QRect(x-30, y-30, 60, 60)))


    def validate (self):
        """Validates the part of the scenario that this view is repsonsible for creating. Returns a
        free text report that indicates the validation result or None if all is ok. """

        # nothing to do
        return None
    
 
#    Local Variables:
#    mode: auto-fill
#    fill-column: 100
#    End:

