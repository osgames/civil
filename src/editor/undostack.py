###############################################################################################
# $Id$
###############################################################################################

import globals
import scenario


# Our list of commands that can be used to undo
undoStack = []

# Maximum undo levels
UNDO_LEVELS = 50

def addUndoList(hexes):
    """hexes if a list of (hexx, hexy) of hexes that will be overwritten."""
    list = []
    for x,y in hexes:
        old = scenario.map.getHexes () [y][x]
        list.append((old, x,y))
    global undoStack
    if len(undoStack) == UNDO_LEVELS:
      undoStack.pop(0)
    assert(len(undoStack) < UNDO_LEVELS)
    undoStack.append(list)

def addUndo(hexx, hexy):
    global undoStack
    if len(undoStack) == UNDO_LEVELS:
      undoStack.pop(0)
    assert(len(undoStack) < UNDO_LEVELS)

    old = scenario.map.getHexes () [hexy][hexx]
    undoStack.append([(old, hexx, hexy)])
    

def undo():
    global undoStack
    if len(undoStack) == 0:
      return

    # Take latest hex change
    hexes = undoStack.pop()

    for (hex, hexx, hexy) in hexes:
        # set the icon for the clicked hex to our selected icon
        scenario.map.getHexes () [hexy][hexx] = hex

        # paste in that icon
        globals.mapview.pasteIcon(hexx, hexy)

    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
