###############################################################################################
# $Id$
###############################################################################################

from qt import *
import os
import sys
import glob
import plugins.plugin
import stat

class PluginView(QListBox):
    """This class is a simple view that is shown in the main tab bar. It contains a list of all the
    plugins that are available. It lets the user select a plugin and then start doing stuff in the
    map. It mainly relays events to the currently selected plugin."""

    def __init__(self, mainwindow):
        """Goes trough the plugins directory, searching for all plugins"""
        QListBox.__init__(self, mainwindow)
        self.mainwindow = mainwindow

        # find all plugins 
        self.plugins = []
        self.__findPlugins()

        # we want single selection
        self.setSelectionMode ( QListBox.Single )

    def __findPlugins(self):
        """Scans a hardcoded path and finds and initializes all plugins. The plugins are added to
        the listbox with all plugins."""
        
        # create a nice os independent path
        plugin_path = os.path.normpath ( 'editor/plugins/*.py' )

        # get all plugins
        for file in glob.glob(plugin_path): 
            mode = os.stat(file)[0]
            if not stat.S_ISREG(mode):
                continue

            file = file[:-3] # skip .py

            try:
                plugin = __import__(file, globals(), locals(), file)
                if not plugin.__dict__.has_key("new"):
                    continue
                instance = plugin.new(self.mainwindow.iconview)
                if not instance:
                    continue
                item = QString(instance.name)
                self.insertItem(item)
                self.plugins.append(instance)
                print "Added plugin %s..." % instance.name
            except:
                print "PLUGIN FAILED", file
                pass

    def getPlugin(self):
        # get the selected plugin index (if any)
        index = self.currentItem ()

        # do we have a selected plugin?
        if index == -1:
            # nope, go away
            return None

        return self.plugins[index]
            
    def mapClickedLeft (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked with the left mouse button. Relays the
        event to the currently selected plugin, if any."""

        plugin = self.getPlugin()
        if not plugin:
            return

        plugin.mapClickedLeft(x,y,hexx, hexy)
        
        print "PluginView.mapClickedLeft"
  

    def mapClickedMid (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked with the middle mouse button. Relays the
        event to the currently selected plugin, if any.""" 

        plugin = self.getPlugin()
        if not plugin:
            return

        plugin.mapClickedMid(x,y,hexx, hexy)

        print "PluginView.mapClickedMid"


    def mapClickedRight (self, x, y, hexx, hexy):
        """Callback triggered when the map has been clicked with the right mouse button. Relays the
        event to the currently selected plugin, if any.""" 

        plugin = self.getPlugin()
        if not plugin:
            return

        plugin.mapClickedRight(x,y,hexx, hexy)

        print "PluginView.mapClickedRight"

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

