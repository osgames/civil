###############################################################################################
# $Id$
###############################################################################################

# current selected company
currentunit = None

# icons
icons = None

# the map view
mapview = None

# global hash of weapons, indexed by the id
weapons = {}

# default weapons for new units
defaultweapons = { 'infantry' : -1,
                   'cavalry': -1,
                   'headquarter': -1,
                   'artillery': -1 }

# alla available ranks for commanders
ranks = []

# default rank for new units
defaultrank = ""

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
