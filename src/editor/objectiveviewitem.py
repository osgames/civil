###############################################################################################
# $Id$
###############################################################################################

import sys
import os
from qt import *
import properties
from constants import REBEL, UNION, UNKNOWN

###############################################################################################
class ObjectiveItem (QListViewItem):
  """This class subclasses a normal QListViewItem and adds needed functionality, such as keeping of
  the objective the item represents. The objective can be retrieved using the 'getObjective()'
  method."""

  
  def __init__ (self, parent, objective ):
    # create the strings for the item
    name   = objective.getName ()
    points = str ( objective.getPoints () )
    owner  = { UNKNOWN: "unknown", REBEL: "rebel", UNION: "union" }[objective.getOwner ()]
    
    QListViewItem.__init__ ( self, parent, name, points, owner )
 
    # store the objective
    self.objective = objective
 
 
  def getObjective (self):
    """Returns the objective this item represents."""
    return self.objective


  def update (self):
    """Updates the labels for the item. Can be used if the objective has changed."""

    # set the new labels
    self.setText ( 0, self.objective.getName () )
    self.setText ( 1, str (self.objective.getPoints () ))
    self.setText ( 2, { UNKNOWN: "unknown", REBEL: "rebel", UNION: "union" }[self.objective.getOwner ()] )

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:

