###############################################################################################
# $Id$
###############################################################################################

import scenario

from server.engine.ai.module import Module
from action.set_target_act   import SetTargetAct


class Rally (Module):
    """This class defines a module that 

    This module is executed every step.
    """


    def __init__ (self):
        """Initializes the module. Sets a suitable name."""

        # call superclass, run once per minute
        Module.__init__ (self, 'rally', 60 )


    def execute (self, actiondata):
        """Executes the module. Loops over all units and checks wether they are disorganized or
        routed. Checks wether the unit can rally and if it can then it's mode is set to the mode it
        should have after after being disorganized/routed.

        A unit can rally or become disorganized if its morale is high enough. The limits for
        rallying are:

        * routed -> disorganized: 30
        * disorganized -> normal: 45
        """

        # TODO: using hardcoded morale values here
        
        # loop over all units we have
        for unit in scenario.info.units.itervalues ():
            # is the unit currently able to rally?
            if unit.getMode ().canRally ():
                # yes, it could optionally rally, it its morale high enough?
                if unit.getMorale ().getValue () > 45:
                    # yep, it can rally
                    self.setMode ( unit, unit.getMode ().onRally (), actiondata )

            # no, it can't rally, can it become disorganized, ie is it routed?
            elif unit.getMode ().canDisorganize ():
                # yes, it could optionally become disorganized, it its morale high enough?
                if unit.getMorale ().getValue () > 30:
                    # yep, it can rally
                    self.setMode ( unit, unit.getMode ().onDisorganize (), actiondata )
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
