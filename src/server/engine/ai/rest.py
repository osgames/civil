###############################################################################################
# $Id$
###############################################################################################

import scenario

from server.engine.ai.module     import Module
from action.change_modifiers_act import ChangeModifiersAct


class Rest (Module):
    """This class defines a module that rests a unit. Every unit which is in a mode that reduces
    fatigue will rest. If the unit rests the a ChangeModifiersAct is sent out with the new data.

    This module is executed every step.
    """


    def __init__ (self):
        """Initializes the module. Sets a suitable name."""
        # call superclass
        Module.__init__ (self, 'rest' )


    def execute (self, actiondata):
        """Executes the module. Loops over all units and checks wether they are in a  mode that
        reduces fatigue (the fatigue change is negative). If the fatigue changes downwards then
        action is returned. For increased fatigue nothing will be done.
        """
        
        # loop over all units we have
        for unit in scenario.info.units.itervalues ():
            # get the possible base fatigue change and multiply by 5, as we run this only every 5th step
            # TODO: wrong numbers!
            fatigue = unit.getMode ().getBaseFatigue () * 5
            
            # does it decrease?
            if fatigue < 0:
                # yes, the unit will rest for this step, first get the old fatigue value so that we
                # can see wether we got a difference at all
                oldfatigue = unit.getFatigue ().getValue ()

                # add the new fatigue
                unit.getFatigue ().addValue ( int (fatigue) )

                # did they change?
                if oldfatigue != unit.getFatigue ().getValue ():
                    # yes, it changed, create action for that
                    self.changeModifiers ( unit, actiondata )
 
        # we're done with the clearing of targets


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
