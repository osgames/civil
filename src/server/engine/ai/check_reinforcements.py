###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
import constants
from server.engine.ai.module    import Module
from action.reinforcements_act  import ReinforcementsAct


class CheckReinforcements (Module):
    """This class defines a module that when executed checks wether any reinforcement units have
    become available. The units that are marked as reinforcements are checked one by one and if they
    are scheduled to arrive at this time they are made visible to the owning player.
   
    This module is executed once every minute.
    """


    def __init__ (self):
        """Initializes the module. Sets a suitable name."""

        # call superclass with the delays and name
        Module.__init__ (self, 'check_reinforcements', 60 )


    def execute (self, actiondata):
        """Executes the module. Checks wether any reinforcements have arrived this turn. This is
        done by iterating the global data structure that contains the arrival turns. If any new unit
        have arrived they will be made available and shown. Sends an ReinforcementsAct action to the
        players.

        It should maybe not be sent to both players, only to the one that gets the reinforcements?
        What about the new units being seen by the other player then, would that work?"""

        # no reinforcements yet
        reinforcements = []
        
        # get the current date
        current_date = scenario.info.getCurrentDate ()

        # loop over all reinforcements that are available
        for company, arrive_date in scenario.info.reinforcements.values ():
            # has this unit arrived at the battlefield?
            if current_turn < arrive_date:
                # not yet, next unit please
                continue

            # so the unit has arrived, add it to the global structure of available units
            scenario.info.units [ company.getid ()] = company

            # remove the unit from the map of reinforcements
            del scenario.info.reinforcements [ company.getid () ]
            
            # we have something now
            reinforcements.append ( company.getid() )

        # ok, all units iterated, did we get anything this turn?
        if len (reinforcements) > 0:
            # yes, create the needed action
            actiondata.append ( ReinforcementsAct (reinforcements) )
    

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
