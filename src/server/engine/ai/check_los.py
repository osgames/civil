###############################################################################################
# $Id$
###############################################################################################

import scenario
import constants

from server.engine.ai.module   import Module
from action.set_visibility_act import SetVisibilityAct


class CheckLOS (Module):
    """This class defines a module that checks line of sight for all units. It iterates over all
    units and checks wether they see any other enemy unit. Changed LOS data is sent out to clients
    so that they can keep consistent LOS information."""


    def __init__ (self):
        """Initializes the module. Sets a suitable name."""
        # call superclass
        Module.__init__ (self, 'checklos', 10 )


    def execute (self, actiondata):
        """Executes the module. Loops over all units and checks LOS to enemy units. """

        # a hash for all the units
        visible = {}
        
        # loop over all units we have
        for unit1 in scenario.info.units.itervalues ():
            # and again, we need a cross product
            for unit2 in scenario.info.units.itervalues ():
                # now check wether unit1 sees unit2
                # TODO: add asymmetric LOS
                if unit1.seesEnemy ( unit2 ):
                    # they see each other
                    visible [unit1.getId()] = 1
                    visible [unit2.getId()] = 1
                else:
                    # does not see it, so no LOS between the units
                    visible [unit1.getId()] = 0
                    visible [unit2.getId()] = 0

                                        
        # now all the units have flags set that match their visibility for the opponent
        for unit in scenario.info.units.itervalues ():
            # has the unit changed visibility for the enemy player?
            if unit.isVisible () == visible [unit.getId()]:
                # same value, so no change
                continue

            # unit has changed visibility, get the enemy to send to
            send_to_id     = (constants.UNION, constants.REBEL)[unit.getOwner()]
            new_visibility = (1,0)[ unit.isVisible() ]

            # create action for it
            actiondata.append ( SetVisibilityAct ( unit.getId(), new_visibility, send_to_id ) )

            # finally assign the new visibility to the units so that the server at least has
            # up-to-date data :) 
            unit.setVisible ( new_visibility )
 

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
