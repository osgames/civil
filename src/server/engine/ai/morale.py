###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
import constants
from server.engine.ai.module import Module
from action.end_game_act     import EndGameAct


class Morale (Module):
    """This class defines a module that when executed checks wether one of the armies is seriously
    demoralized and will autosurrender. This calculation is performed by just adding all total
    morale in the army and dividing with the count of units. A demoralized army is considered
    unvilling to fight anymore and will surrender.

    If either player has no units left then the game is also ended, in the same way as
    'UnitsDestroyed' would do.

    The setting module_morale_minimum determines the minimum morale.
   
    This module is executed once every turn.

    TODO: handle victories and draws somehow
    """


    def __init__ (self):
        """Initializes the module. Sets a suitable name."""

        # call superclass with the delays and name
        Module.__init__ (self, 'morale', 60 )


    def execute (self, actiondata):
        """Executes the module. Counts the number of units that are left for both armies, and if
        either (or both) players have less units left than a certain % then the game is ended."""

        # no morale and no units at all yet
        morale = { constants.REBEL: 0, constants.UNION: 0 }
        counts = { constants.REBEL: 0, constants.UNION: 0 }
        
        # loop over all ok units
        for unit in scenario.info.units.itervalues():
            # add to the counts
            morale [unit.getOwner ()] += unit.getMorale ().getValue ()
            counts [unit.getOwner ()] += 1

        # we need to check the counts to make sure that neither player has no companies left. If
        # that would happend we'd do a nice ZeroDivisionError when calculating the average morale

        # is the rebel player destroyed?
        if counts [ constants.REBEL ] == 0:
            # rebels are destroyed
            print "Morale.execute: rebel army destroyed"
            actiondata.append ( EndGameAct ( constants.REBEL_DESTROYED ) )
            return

        # is the union player destroyed?
        if counts [ constants.UNION ] == 0:
            # unions are destroyed
            print "Morale.execute: union army destroyed"
            actiondata.append ( EndGameAct ( constants.UNION_DESTROYED ) )
            return
        
        # get the minimum morale
        minmorale = properties.module_morale_minimum

        # loop over the armies
        for player in (constants.REBEL, constants.UNION):
            # is the total average morale less than the current allowed?
            if float ( morale[player] ) / counts[player] < minmorale:
                # yep, the army is demoralized
                print "Morale.execute: army for player %d is demoralized" % player

                # get the flag we should send
                flag = [ constants.REBEL_DEMORALIZED, constants.UNION_DEMORALIZED ][player]

                # and enqueue the plan ending the game
                actiondata.append ( EndGameAct ( flag ) )

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
