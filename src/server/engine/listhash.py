###############################################################################################
# $Id$
###############################################################################################

import sys, UserDict

class ListHash (UserDict.UserDict):
    """This class defines a hash which contains lists as the elements. It builds up a 2D
    structure. The meaning is to have each key in the hash be the id of a turn, and the value for
    each key a list of commands.

    All normal dictionary actions can be performed on this class, but the method add() should be
    used to perform the addition of elements."""
   

    def add (self, id, value):
        """Adds 'value' to the end of the list which is found under the key 'key'. If no such key
        exists in the hash then it is created."""
        # do we have such a key?
        if not self.data.has_key ( id ):
            # nope, add it
            self.data [id] = []

        # get the list
        list = self.data [id]

        # append the element
        list.append ( value )
        

if __name__ == '__main__':
    l = ListHash()

    for i in range(10):
        for j in range (3):
            l.add ( 10 - i, j )

    keys = l.keys ()
    keys.sort ()
    
    for key in keys:
        print key, l[key]
         
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
