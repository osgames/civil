###############################################################################################
# $Id$
###############################################################################################


class Sequence:
    """
    This class is a simple class mainly used to contain a numeric sequence of numbers and provide
    means to get the next number from that sequence. It is designed to work as a id generator for
    commands within the server engine.

    The main methods are next() which returns the next available id and increses it, and get() which
    returns the current id without increasing.
    """

    def __init__ (self, value = 0):
        """Initializes the instance. Sets default sequence value."""
        # store the value
        self.value = 0


    def next (self):
        """Returns the next available value from the sequence. Increases the sequence by 1."""
        # increase value
        self.value += 1

        return self.value


    def get (self):
        """Returns the current value without altering it in any way."""
        return self.value


    def toString (self):
        """Returns a string representation of the sequence value."""
        return str ( self.value )       


    def __str__(self):
        """Returns a string representation of the sequence value."""
        return str ( self.value )
