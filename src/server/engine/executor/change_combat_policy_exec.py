###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
import rotate_util       

from executor                        import Executor
from action.change_combat_policy_act import ChangeCombatPolicyAct


class ChangeCombatPolicyExec (Executor):
    """
    This class implements an executor for the plan 'ChangeCombatPolicy'. It contains the actual code for
    executing changing the combat policy for the unit. Changing it really needs no extra data or
    delays, so it is immediately assigned.
    """

    def __init__ (self, unit, plan):
        """Initializes the instance."""
        # call superclass constructor
        Executor.__init__ ( self, unit, plan )
        
      
    def execute (self):
        """Executes the plan. Simply just sets the new policy and sends out the action."""

        # assign the new policy to the unit
        self.unit.setCombatPolicy ( self.plan.getPolicy () )

        # this plan is finished now
        self.finished = 1

        # create action for the changed policy
        return ChangeCombatPolicyAct ( self.unit.getId (), self.plan.getPolicy () )
    
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
