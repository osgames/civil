###############################################################################################
# $Id$
###############################################################################################

import sys
from math import floor, ceil, sqrt, pi, atan2, hypot

import properties
import scenario


def calculateDistance (sx, sy, dx, dy):
    """Returns the distance from (sx,sy) to (dx,dy)."""
    # get the distance from the source to the destination
    return hypot ( float (sx) - float (dx), float (sy) - float (dy) )


def calculateMovementSpeed (unit, basespeed):
    """Calculates the movements speed of the unit in psotitions (pixels) per turn. Returns the
    result.  Several different types of modifications are performed on the movement speed and base
    delay before commands are actually created. These are:

    * base delay
    * unit state
    * unit fatigue
    * terrain

    This method also calculates the x- and y-deltas the unit moves each turn, as well as the
    facing the unit should have in order to move towards the new position.
    """

    # use the given base speed for the unit
    speed = basespeed
    
    # use the unit's fatigue to modify the movement speed. A too fatigued unit will be forced to
    # move slower due to exhaustion
    speed = unit.getFatigue ().evaluateMovementSpeed ( speed )
    
    # use the terrain to modify the movement speed
    speed = speed * scenario.map.getTerrain ( unit.getPosition ()).movementSpeedModifier( unit )
    
    # get the number of steps per turn the unit can do. We use the movement speed per minute for
    # the unit as base for the calculation
    speed = float ( speed ) / (60.0 / properties.seconds_per_step)

    #print "calculateMovementSpeed: base speed %d, modified speed %.2f:" % (basespeed, speed)

    # we're done
    return speed


def calculatePosAlongFacing (facing, distance):
    """Calculates the deltas for a movement 'distance' long in the direction of 'facing'. The data
    returned is a (x,y) tuple, whose values originate from (0,0), so the deltas can be added to any
    coordinate. This function does no precuation checks for coords outside the map etc."""
    # precuation for the angle
    if facing < 0 or facing > 35:
        # illegal value
        raise "illegal facing %d, valid: [0,35]" % facing

    # do some black magic with the facing. the reason for this is that we have facing 0 as straight
    # north, and increasing clockwise. the math stuff has 0 at straight west, and increasing
    # counterclockwise. so we need to modify our value to fit those expectations
    facing = 36 - (facing - 9) % 36
    
    # convert the facing to a radian value
    radian = (facing * 10.0) / 180.0 * pi
    
    # get the distance values in the x, y directions based on the radian
    deltax = cos ( radian ) * distance
    deltay = sin ( radian ) * distance

    # return what we got
    return deltax, deltay

    
def calculateMovementDeltas (sx, sy, dx, dy, speed):
    """This funtion calculates movement deltas for the x- and y-direction based on a given speed for
    a unit. The position (sx,sy) is where the unit is right now and (dx,dy) is the destination. A
    tuple (x,y) is returned with the delta values."""

    # get the distance from the source to the destination
    distance = calculateDistance ( sx, sy, dx, dy )

    # default to invalid delta values
    deltax = None
    deltay = None
    
    #try:
    # how many turns are needed?
    turns_needed = distance / float(speed)

    # also calculate the deltax and deltay, i.e. the size of each step
    if turns_needed < 1.0:
        deltax = float ( dx - sx )
        deltay = float ( dy - sy )
        
    else:
        deltax = float ( dx - sx ) / turns_needed 
        deltay = float ( dy - sy ) / turns_needed 

    # return whatever deltas we have
    return ( deltax, deltay )

        
            # TODO: why use ceil() here? would mean our movement won't ever hit the target....
            #deltax = ceil ( float ( dx - sx ) / turns_needed )
            #deltay = ceil ( float ( dy - sy ) / turns_needed )

            
##     except ZeroDivisionError:
##         # either we're already there, or we can't move.
##         if distance > 0:

##             # TODO: clean this up!
##             print "move_util.calculateMovementDeltas: TODO:  clean this up! hardcoded values!"
            
##             # Try allowing base movement speed:
##             speed = float ( basespeed ) / (60.0 / properties.seconds_per_step)
##             turns_needed = distance / speed

##             deltax = float ( dx - sx ) / turns_needed
##             deltay = float ( dy - sy ) / turns_needed
            
##             # if allowed to move at base speed, are we still stuck in two moves?
##             # FIXME: this should also factor in exhaustion, etc. once those are handled above.
##             if scenario.map.getTerrain((sx+deltax*2, sy+deltay*2)).movementSpeedModifier() == 0:
##                 # Can't get out that way.  Give up, stop moving.
##                 turns_needed = 0 
##                 speed = 0
                
##         else:
##             # we're already there
##             turns_needed = 0 
##             speed = 0

#    print "calculateMovementDeltas: %3f %3f %3f %3f" % (turns_needed,speed,deltax,deltay)


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
