###############################################################################################
# $Id$
###############################################################################################

# import all plan executors
from assault_exec              import AssaultExec
from change_combat_policy_exec import ChangeCombatPolicyExec
from change_mode_exec          import ChangeModeExec
from move_exec                 import MoveExec
from move_fast_exec            import MoveFastExec
from retreat_exec              import RetreatExec
from rotate_exec               import RotateExec
from rout_exec                 import RoutExec
from skirmish_exec             import SkirmishExec
from wait_exec                 import WaitExec


# our map of creators
creators = { 'assault'            : AssaultExec,
             'changecombatpolicy' : ChangeCombatPolicyExec,
             'changemode'         : ChangeModeExec,
             'move'               : MoveExec,
             'movefast'           : MoveFastExec,
             'retreat'            : RetreatExec,
             'rotate'             : RotateExec,
             'rout'               : RoutExec,
             'skirmish'           : SkirmishExec,
             'wait'               : WaitExec
             }
           

def create (unit, plan):
    """Creates a new plan executor from the passed plan. Based on the plan type a new executor is
    created and returned. """  

    # get the plan type
    type = plan.getName ()

    # do we have such a plan?
    if not creators.has_key ( type ):
        # unknown plan type
        raise "factory.create: unknown plan type: '%s'" % type

    # create a executor based on the plan type
    executor = creators [type] ( unit, plan )    
    
    return executor
    
       
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
