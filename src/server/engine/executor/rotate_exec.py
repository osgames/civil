###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties

from executor               import Executor
from action.rotate_act      import RotateAct
from rotate_util            import calculateTurnSpeed, calculateAngle, rotate


class RotateExec (Executor):
    """
    This class implements an executor for the plan 'Rotate'. It contains the actual code for
    executing a rotation for a unit. The engine will based on the data calculate when and how the
    rotation takes place. The unit rotates without altering state anyhow. A unit in column mode
    rotates in column mode etc.
    """

    def __init__ (self, unit, plan):
        """Initializes the instance."""
        # call superclass constructor
        Executor.__init__ ( self, unit, plan )
       
        # calculate turning speed
        self.turnspeed = calculateTurnSpeed ( self.unit )

        xunit, yunit     = self.unit.getPosition ()
        xtarget, ytarget = self.plan.getTarget ()
        
        # calculate the angle the unit should rotate to
        self.targetfacing = calculateAngle ( xunit, yunit, xtarget, ytarget )

        # get the current facing for the unit
        self.currentfacing = self.unit.getFacing ()
        
      
    def execute (self):
        """Executes the plan. Performs all calculation checks and wether the unit can rotate at
        all. The reasons for that might be:

        * no command control
        * bad state (routed)

        If all is ok separate 'rotate' action events will be created, one for every step the unit
        will spend rotating. For a small rotation this may be only one action, but for larger the
        rotation will take several steps.
        """

        # is the rotation done?
        if self.currentfacing == self.targetfacing:
            # we're done, no rotation needed anymore
            self.finished = 1
            return None

        # TODO: can the unit perform the rotation anymore?
        

        # get the new facing for the unit
        self.currentfacing = rotate ( self.currentfacing, self.targetfacing, self.turnspeed )

        # assign the new facing to the unit too
        self.unit.setFacing ( int (self.currentfacing) )

        # create action for the changed facing
        return RotateAct ( self.unit.getId (), int (self.currentfacing) )
    
 
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
