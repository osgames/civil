###############################################################################################
# $Id$
###############################################################################################

import scenario
import properties
from executor                    import Executor


class WaitExec (Executor):
    """
    This class executes the plan 'wait'. This plan is sent by clients when a unit has been
    ordered to wait for a certain amount of time. This executor will simply be delayed that amount
    of time and then it will be finished. No other action. The unit wil lnot gain fatigue.
    """

    def __init__ (self, unit, plan):
        """Initializes the instance."""
        # call superclass constructor
        Executor.__init__ ( self, unit, plan )

       
    def execute (self):
        """Executes the plan. Does nothing, when we get this far the plan is already executed, as
        the execution is the waiting! """

        # execution is finished, nothing to do at all
        self.finished = 1


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
