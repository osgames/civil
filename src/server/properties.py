###############################################################################################
# $Id$
###############################################################################################

###############################################################################################
# the number of seconds to advance for each wall clock second. this is the actual speedup of
# the game compared to real time.
speedup_factor = 5


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
