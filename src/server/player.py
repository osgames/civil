###############################################################################################
# $Id$
###############################################################################################


class Player:
    """
    This class defines a player as the server sees one. A player consists of an id that define the
    side the player has along with a connection. The connection is used to receive and send data to/from the
    player. The connection can not be altered once set, but the id can be changed.
    """


    def __init__ (self, connection, id, name):
        """Creates an instance of the class. Uses the passed data or default values."""
        # set the passed name and rank
        self.name = name
        self.id = id
        self.connection = connection


    def getId (self):
        """Returns the id of the player."""
        return self.id


    def setId (self, id):
        """Stores a new id for the player."""
        self.id = id 


    def getOtherPlayerId (self):
        """Returns the id of the other player."""

    def getName (self):
        """Returns the name of the player. This is a cleartext name suitable for displaying to the
        players. """
        return self.name


    def setName (self, name):
        """Stores a new name for the player."""
        self.name = name 


    def getConnection (self):
        """Returns the connection connected to the player."""
        return self.connection


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
