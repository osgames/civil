###############################################################################################
# $Id$
###############################################################################################

import constants

###############################################################################################
# the players that are connected to the server. The map can be indexed with the player id:s.
# we prefill it with two null values 
players = { constants.REBEL: None,
            constants.UNION: None }


###############################################################################################
# a thread safe queue of incoming stuff from the players
incoming = None


###############################################################################################
# the actual server engine. this is where the server spends all its time.
engine = None


###############################################################################################
# the current turn. this is a number incremented for each iteration of the main loop that the
# server performs
turn = 0

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
