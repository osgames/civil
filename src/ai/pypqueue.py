
# temporary priority queue hack until I work up a permanent replacement

from bisect import insort
class PQueue:
    def __init__(self):
        self.queue = []

    def insert(self,pri,item):
        insort(self.queue,(pri,item))

    def pop(self):
        return self.queue.pop(0)

