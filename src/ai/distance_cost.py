
try:
    import dc

    # specify distance cost function with map type
    dc_algorithm = {
        'diamond_v':dc.distance_cost_dv,
        'diamond_h':dc.distance_cost_dh,
        'rectangle':dc.distance_cost_r,
        'square_sg':dc.distance_cost_sg,
        'square_sd':dc.distance_cost_sd,
    }

except:

    from math import floor,ceil

    # Thanks Amit!
    # http://www-cs-students.stanford.edu/~amitp/Articles/HexLOS.html
    # Note: I'm using x,y coord to represent the opposite what amit does
    # This is mainly to allow for more seemless working with Numeric

    # distance cost helper functions
    def same_sign(n,n1): return (n > -1) == (n1 > -1)
    def a2h((x,y)): return (int(x - floor(float(y)/2)),int(x+ceil(float(y)/2)))
    def h2a((x,y)): return (int(floor(float(x+y)/2)),y-x)

    # For use with rectangular hex maps - usually a 2D array
    # The a2h and h2a algorithms are transforms needed here and to convert back
    # the path list below (XXX below)
    def r_distance_cost(p,p1):
        x,y = a2h(p)
        x1,y1 = a2h(p1)
        dx = x1-x
        dy = y1-y
        if same_sign(dx,dy):
            return max(abs(dx),abs(dy))
        else:
            return (abs(dx) + abs(dy))

    # Diamond shaped maps
    # used for vertically stacked hex maps
    def dv_distance_cost((x,y),(x1,y1)):
        dx = x1-x
        dy = y1-y
        if same_sign(dx,dy):
            return max(abs(dx),abs(dy))
        else:
            return (abs(dx) + abs(dy))

    # used for horizonally stacked hex maps
    def dh_distance_cost((x,y),(x1,y1)):
        dx = x1-x
        dy = y1-y
        if not same_sign(dx,dy):
            return max(abs(dx),abs(dy))
        else:
            return (abs(dx) + abs(dy))

    # for square grid maps that don't allow diagonal movement
    def sg_distance_cost((x,y),(x1,y1)):
        dx = x1-x
        dy = y1-y
        return (abs(dx) + abs(dy))
            
    # for square grid maps that does allow diagonal movement
    def sd_distance_cost((x,y),(x1,y1)):
        dx = x1-x
        dy = y1-y
        return max(abs(dx),abs(dy))

    # specify distance cost function with map type
    dc_algorithm = {
        'diamond_v':dv_distance_cost,
        'diamond_h':dh_distance_cost,
        'rectangle':r_distance_cost,
        'square_sg':sg_distance_cost,
        'square_sd':sd_distance_cost,
    }

