###############################################################################################
# $Id$
###############################################################################################

import sys, string
import os
import xml.parsers.expat
import gzip
import datetime
import traceback

import properties
import scenario
from simple_dom_parser import SimpleDOMParser, Node
from scenario_info     import ScenarioInfo
from unit              import InfantryCompany, CavalryCompany, ArtilleryBattery, Headquarter
from constants         import UNION, REBEL, UNKNOWN
from objective         import Objective
from organization      import Brigade, Battallion, Regiment
from modifier          import Morale, Fatigue, Experience, Aggressiveness, RallySkill, Motivation
from map.map           import Map
from map.hex           import Hex
from location          import Location
from leader            import Leader
from weapon            import Weapon

class ScenarioParser:
    """
    This class works as a compact parser for the scenario files used within Civil. This class has as
    its only purpose to read a given url/filename and parse all scenario data from it. All data is
    stored in members in the global file 'scenario'. 

    The parsing works by first reading the entire file into a simple DOM tree and then doing the
    actual parsing. This is a simpler approach as we when the actual parsing is done have the full
    data available.

    Use this class like this:

    if not ScenarioParser ().parseFile ( file ):
        # failed to parse the file
        ...
    else:
        # file parsed ok
        ...

    Separate methods exist for most of the big groups of data in the scenario file.
    """


    def __init__ (self):
        """Creates the instance of the class. Does no parsing yet."""

        # no root yet
        self.root = None

        # helper members for the unit hierarchy
        self.currentBrigade    = None
        self.currentRegiment   = None
        self.currentBattallion = None

        # helper hash for all weapons
        self.weapons = {}


    def parseString (self, data):
        """Parses the passed string 'data' and creates the data into the scenario. An internal DOM
        tree is built up from the XML data and it is then parsed. Returns 1 on success and 0 on
        error."""

        # create a new parser
        domparser = SimpleDOMParser ()

        # parse the data
        try:
            self.root = domparser.parseString ( data )
       
        except:
            # serios error
            print "Error parsing passed string, aborting parse"
            raise
            del self.root
            return 0
                
        # did we get anything?
        if self.root == None:
            # nope, failed to retrieve it
            print "Failed to parse XML from string"
            del self.root
            return 0

        # now parse the data
        status = self.parseRoot ()

        # delete the root and return the data
        del self.root
        return status


    def parseFile (self, filename):
        """Parses the passed 'filename' and creates the data into the scenario. An internal DOM tree is
        built up from the XML data and it is then parsed. Returns 1 on success and 0 on error."""

        # create a new parser
        domparser = SimpleDOMParser ()
            
        try:
            # parse the data
            #self.root = domparser.parseFile ( open ( url ) )
            self.root = domparser.parseFile ( open ( filename ) )
            #self.root = domparser.parseFile ( gzip.GzipFile  ( url, 'rb' ) )

        except:
            # serios error
            print "Error reading: " + filename + ", aborting parse"
            raise
            del self.root
            return 0
                
        # did we get anything?
        if self.root == None:
            # nope, failed to retrieve it
            print "Failed to retrive or parse XML from: " + filename
            del self.root
            return 0

        # now parse the data
        status = self.parseRoot ()

        # delete the root and return the data
        del self.root
        return status


    def parseRoot (self):
        """Parses a passed DOM-tree and builds up a number of instances of ScenarioInfo. They are
        appended to the internal list of senario-info:s."""
        # the toplevel root should be 'scenario'
        if self.root.getName () != 'scenario':
            # oops, illegal file
            print "DOM tree is illegal, toplevel tag is: " + self.root.getName () + ", "
            print "the node should be 'scenario'"
            return

        # Default
        version = "0.80"
        try:
            version = self.root.getAttribute("version")
        except:
            print "Don't know which version format, assuming 0.80"
            pass

        #
        # Here we should decide which scenario_parser to really
        # use, based on the variable "version"
        #
        # Currently supported versions:
        # 0.80, tagged 2002-08-30
        #
        # 0.82, tagged 2003-xx-yy, adds the <valid/> and <invalid/> tags

        # set the valid children for this level
        valid = { 'scenarioinfo' : self.parseScenarioInfo,
                  'weapons'      : self.parseWeapons,
                  'units'        : self.parseUnits,
                  'locations'    : self.parseLocations,
                  'objectives'   : self.parseObjectives,
                  'map'          : self.parseMap }
                  

        # loop over all toplevel children
        for node in self.root.getChildren ():
            try:
                # execute the proper handler for the node
                valid [node.getName ()]( node )

            except KeyError:
                # no such node
                print "Illegal tag in root tag: '%s'." % node.getName()
                raise
                return 0

        # fix the targets of the units
        if not self.fixUnitTargets ():
            return 0

        return 1


    def parseScenarioInfo (self, node):
        """This method """
        try:
            # get the simple data
            name        = node.getChild ('name').getData ()
            location    = node.getChild ('location').getData ()
            theatre     = node.getChild ('theatre').getData ()
            
            # do we have an id?
            if node.hasAttribute ( 'id' ):
                # yep, get it
                id = int ( node.getAttribute ( 'id' ) )
            else:
                id = None

            # do we have a local player id? if we have that then it must be parsed too
            idnode = node.getChild ( 'localplayer' )
            if idnode != None:
                # yes, we have one so get the attribute and store it
                scenario.local_player_id = int ( idnode.getAttribute ( 'id' ) )

            # no description yet
            description = []
            
            # and the description
            for paratag in node.getChild ('description').getChildren ():
                # get the text
                para = paratag.getData ()
                
                # append the text to the description
                description.append ( para )

            # get the rebel and union missions
            rebel = []
            union = []

            # get the missions
            mission = node.getChild ('missions')
            
            # rebel misisons
            for paratag in mission.getChild ( 'rebel').getChildren ():
                # get the text and append to the description
                rebel.append ( paratag.getData ().strip () )

            # union misisons
            for paratag in mission.getChild ( 'union').getChildren ():
                # get the text and append to the description
                union.append ( paratag.getData ().strip () )

            # create a new scenario info if we don't already have one 
            if scenario.info == None:
                scenario.info = ScenarioInfo ( id )

            # get the starting date and split it up
            start_datenode = node.getChild ('startdate')
            start_year     = int ( start_datenode.getAttribute ( 'year' ) )
            start_month    = int ( start_datenode.getAttribute ( 'month' ) )
            start_day      = int ( start_datenode.getAttribute ( 'day' ) )
            start_hour     = int ( start_datenode.getAttribute ( 'hour' ) )
            start_minute   = int ( start_datenode.getAttribute ( 'minute' ) )
            start_second   = 0

            # get the current date and split it up
            current_datenode = node.getChild ('currentdate')
            current_year     = int ( current_datenode.getAttribute ( 'year' ) )
            current_month    = int ( current_datenode.getAttribute ( 'month' ) )
            current_day      = int ( current_datenode.getAttribute ( 'day' ) )
            current_hour     = int ( current_datenode.getAttribute ( 'hour' ) )
            current_minute   = int ( current_datenode.getAttribute ( 'minute' ) )
            current_second   = 0

            # get the ending date and split it up
            end_datenode = node.getChild ('enddate')
            end_year     = int ( end_datenode.getAttribute ( 'year' ) )
            end_month    = int ( end_datenode.getAttribute ( 'month' ) )
            end_day      = int ( end_datenode.getAttribute ( 'day' ) )
            end_hour     = int ( end_datenode.getAttribute ( 'hour' ) )
            end_minute   = int ( end_datenode.getAttribute ( 'minute' ) )
            end_second   = 0
            
            # set all parsed data
            scenario.info.setName ( name )
            scenario.info.setLocation ( location )
            scenario.info.setTheatre ( theatre )
            scenario.info.setDescription ( description )
            scenario.info.setStartDate ( datetime.datetime ( start_year, start_month, start_day,
                                                             start_hour, start_minute, start_second ) )
            scenario.info.setCurrentDate ( datetime.datetime ( current_year, current_month, current_day,
                                                             current_hour, current_minute, current_second ) )
            scenario.info.setEndDate ( datetime.datetime ( end_year, end_month, end_day,
                                                           end_hour, end_minute, end_second ) )
            scenario.info.setMission ( REBEL, rebel )
            scenario.info.setMission ( UNION, union )

            # now we need to see if the scenario is valid at all
            # TODO: version checks?
            if node.getChild ('valid') == None:
                # no such node, so the scenario is invalid
                scenario.info.setValid ( 0 )
            else:
                # it's valid all righ
                scenario.info.setValid ( 1 )
                
        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <scenarioinfo> node of scenario."
            return 0

        # all is ok
        return 1
    

    def parseUnits (self, node):
        """This method parses all units of the game. This method parses the tag <units> and the
        children <union> and <rebel> of that tag."""

        # get the mandatory children
        rebel = node.getChild ('rebel')
        union = node.getChild ('union')

        # loop over all union brigades
        for node in union.getChildren ():
            # parse the brigade
            if not self.parseBrigade ( node, UNION ):
                # failed to parse it
                return 0

        # loop over all rebel brigades
        for node in rebel.getChildren ():
            # parse the brigade
            if not self.parseBrigade ( node, REBEL ):
                # failed to parse it
                return 0

        # all is ok
        return 1


    def parseBrigade (self, node, owner):
        """This method parses a <brigade> XML construct, and its children. """
        # precautions
        if node.getName () != 'brigade':
            # oops, invalid tag here
            print "Invalid data: '" + node.getName () + "', expected brigade"
            return 0

        # get the necessary attibutes
        id   = int ( node.getAttribute ( 'id' ) )
        name = node.getAttribute ( 'name' )

        # create a new brigade
        brigade = Brigade ( id, name, owner )

        # parse and set the headquarter
        hq = self.parseHeadquarter ( node.getChild ( 'headquarter' ), owner )
        brigade.setHeadquarter ( hq )

        # make the hq a hq for this brigade
        hq.setHeadquarterFor ( brigade )

        # set the parent organization
        # TODO: higher organizations?
        brigade.setParentOrganization ( None )
        hq.setParentOrganization ( None )

        # add the brigade to the scenario data
        scenario.info.brigades [brigade.getOwner ()] [brigade.getId ()] = brigade

        # this brigade is the new current brigade
        self.currentBrigade = brigade
        
        # loop over all the regiments
        for regiment in node.getChildren ():
            # is this node a commander?
            if regiment.getName () == 'headquarter':
                # yep, don't parse it
                continue
            
            # parse the regiment
            if not self.parseRegiment ( regiment, owner ):
                # failed to parse it
                return 0

        #print "Parsed brigade id ", id
            
        # all is ok
        return 1
        

        
    def parseRegiment (self, node, owner):
        """This method parses a <regiment> XML construct, and its children. """
        # precautions
        if node.getName () != 'regiment':
            # oops, invalid tag here
            print "Invalid data: '" + node.getName () + ", expected regiment"
            return 0

        # get the necessary attibutes
        id   = int ( node.getAttribute ( 'id' ) )
        name = node.getAttribute ( 'name' )

        # create a new regiment
        regiment = Regiment ( id, name, owner )

        # parse and set the headquarter
        hq = self.parseHeadquarter ( node.getChild ( 'headquarter' ), owner )
        regiment.setHeadquarter ( hq )

        # make the hq a hq for this brigade
        hq.setHeadquarterFor ( regiment )

        # set the parent organization 
        regiment.setParentOrganization ( self.currentBrigade )
        hq.setParentOrganization ( self.currentBrigade )
       
        # add this regiment to the regiments for the current brigade
        self.currentBrigade.getRegiments ().append ( regiment )

        # this  is the new current 
        self.currentRegiment = regiment
        self.currentBattallion = None
        
        # loop over all the subnodes. These may be battallions or companies
        for organization in node.getChildren ():
            # is this node a leader?
            if organization.getName () == 'headquarter':
                # yep, don't parse it
                continue
            
            # what do we have here?
            elif organization.getName () == 'battallion':
                # parse the battallion
                if not self.parseBattallion ( organization, owner ):
                    # error parsing
                    return 0

            elif organization.getName () == 'company':
                # parse the battallion
                if not self.parseCompany ( organization, owner ):
                    # error parsing
                    return 0

            else:
                # invalid tag
                print "Invalid data: '" + organization.getName () + ", expected battallion,company"
                return 0

        #print "Parsed regiment id ", id

        # all is ok
        return 1
        
        

    def parseBattallion (self, node, owner):
        """This method parses a <battallion> XML construct, and its children. """
        # precautions are unnecessary here, as it was checked above

        try:
            # get the necessary attibutes
            id   = int ( node.getAttribute ( 'id' ) )
            name = node.getAttribute ( 'name' )

            # create a new brigade
            battallion = Battallion ( id, name, owner )

            # parse and set the headquarter
            hq = self.parseHeadquarter ( node.getChild ( 'headquarter' ), owner )
            battallion.setHeadquarter ( hq )
            
            # make the hq a hq for this brigade
            hq.setHeadquarterFor ( battallion )

            # set the parent organization 
            battallion.setParentOrganization ( self.currentRegiment )
            hq.setParentOrganization ( self.currentRegiment )

            # add this regiment to the regiments for the current brigade
            self.currentRegiment.getBattallions ().append ( battallion )

            # this  is the new current 
            self.currentBattallion = battallion
            
            # loop over all the companies
            for company in node.getChildren ():
                # is this node a leader?
                if company.getName () == 'headquarter':
                    # yep, don't parse it
                    continue
                
                # parse the company
                if not self.parseCompany ( company, owner ):
                    # failed to parse it
                    return 0

        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <battallion> node of scenario."
            return 0
       
        #print "Parsed battallion id ", id

        # all is ok
        return 1
         
 
    def parseCompany (self, node, owner):
        """This method """
        # precautions
        if node.getName () != 'company':
            # oops, invalid tag here
            print "Invalid data: '" + node.getName () + ", expected company"
            return 0

        try:
            # get the necessary attibutes
            id     = int ( node.getAttribute ( 'id' ) )
            name   = node.getAttribute ( 'name' )
            type   = node.getAttribute ( 'type' )
            company = None
            
            # what type did we get?
            if type == 'infantry':
                # create a new infantry unit
                company = InfantryCompany ( id, name, owner )

            elif type == 'cavalry':
                # create a new cavalry unit
                company = CavalryCompany ( id, name, owner )
                
            elif type == 'artillery':
                # create a new artillery unit
                company = ArtilleryBattery ( id, name, owner )

            else:
                print "Unknown company type: " + type
                return 0

            # parse the commander from the <commander> tag
            company.setCommander ( self.parseCommander ( node.getChild ( 'commander' ) ) )
            
            # add the unit to the current battallion or regiment
            if self.currentBattallion != None:
                # currently filling in a battallion
                self.currentBattallion.getCompanies ().append ( company )

                # set the parent organization 
                company.setParentOrganization ( self.currentBattallion )
            
            elif self.currentRegiment != None:
                # currently filling in a regiment
                self.currentRegiment.getCompanies ().append ( company )

                # set the parent organization 
                company.setParentOrganization ( self.currentRegiment )

            # get all other data
            company.setMen        ( int ( node.getChild ('men').getAttribute ( 'ok' ) ))
            company.setKilled     ( int ( node.getChild ('men').getAttribute ( 'killed' ) ))
            company.setFacing     ( int ( node.getChild ('facing').getAttribute ( 'value' ) ))
            company.setMorale     ( Morale     (int ( node.getChild ('morale').getAttribute ( 'value' ) )))
            company.setExperience ( Experience (int ( node.getChild ('experience').getAttribute ( 'value' ))))
            company.setFatigue    ( Fatigue    (int ( node.getChild ('facing').getAttribute ( 'value' ) )))

            # get the position
            pos = node.getChild ('pos')
            company.setPosition ( ( int ( pos.getAttribute ( 'x' ) ), int ( pos.getAttribute ( 'y' ) ) ))

            # do we have a arrival turn if we have that then it must be parsed too
            arrivesnode = node.getChild ( 'arrives' )
            if arrivesnode != None:
                # yes, so get the attribute describing when the unit arrives and split it up
                year     = int ( arrivesnode.getAttribute ( 'year' ) )
                month    = int ( arrivesnode.getAttribute ( 'month' ) )
                day      = int ( arrivesnode.getAttribute ( 'day' ) )
                hour     = int ( arrivesnode.getAttribute ( 'hour' ) )
                minute   = int ( arrivesnode.getAttribute ( 'minute' ) )
                second   = 0

                # create a date object from the data
                date = datetime.datetime ( year, month, day, hour, minute, second )

                # store the unit as a reinforcement along with the date
                scenario.info.reinforcements [ company.getId () ] = (company, date)

                # make unit hidden at first
                company.setVisible ( 0 )
            else:     
                # the unit is immediately available, but does it have any men at all? it might
                # already be destroyed
                if company.getMen () == 0:
                    # no men, so this is a destriyed unit
                    scenario.info.destroyed_units [ company.getId ()] = company
                    company.setDestroyed ()
                else:
                    # unit is ok
                    scenario.info.units [ company.getId ()] = company

            # do we have a target id? if we have that then it must be parsed too
            targetnode = node.getChild ( 'target' )
            if targetnode != None:
                # yes, we have one so get the attribute and store it. Note that this is basically
                # wrong! We store a numeric id in the member, where a full unit was expected. This
                # will later in the parser be fixed by looping all units and setting the correct
                # id:s. it can't be done now, as all units may not yet be parsed
                company.target = int ( targetnode.getAttribute ( 'id' ) )

            # get and set the the weapon
            try:
                # get the weapon
                weapon         = int ( node.getChild ('weapon').getAttribute ( 'id' ))
                okcount        = int ( node.getChild ('weapon').getAttribute ( 'ok' ))
                destroyedcount = int ( node.getChild ('weapon').getAttribute ( 'destroyed' ))

                # get the matching weapon. The weapon is an id that refers to previoulsy parsed
                # weapons stored locally
                company.setWeapon       ( self.weapons [ weapon ] )
                company.setWeaponCounts ( okcount, destroyedcount )

            except KeyError:
                # we have a custom catch here, as this is one of the few places where we refer to
                # old information
                traceback.print_exc (file=sys.stdout)
                print "Invalid weapon id in <company> node of scenario."
                return 0
                
        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <company> node of scenario."
            return 0
       
        #print "Parsed company id ", id

        # all is ok
        return 1


    def parseHeadquarter (self, node, owner):
        """ """
        # precautions
        if node.getName () != 'headquarter':
            # oops, invalid tag here
            print "Invalid data: '" + node.getName () + ", expected headquarter"
            return None

        try:
            # get the necessary attibutes
            id   = int ( node.getAttribute ( 'id' ) )
            name = node.getAttribute ( 'name' )

            # create a hq
            hq = Headquarter ( id, name, owner )
            
            # parse the commander from the <commander> tag
            hq.setCommander ( self.parseCommander ( node.getChild ( 'commander' ) ) )

            # get all other data
            hq.setMen        ( int ( node.getChild ('men').getAttribute ( 'ok' ) ))
            hq.setKilled     ( int ( node.getChild ('men').getAttribute ( 'killed' ) ))
            hq.setFacing     ( int ( node.getChild ('facing').getAttribute ( 'value' ) ))
            hq.setMorale     ( Morale ( int ( node.getChild ('morale').getAttribute ( 'value' ) )))
            hq.setExperience ( Experience (int (node.getChild ('experience').getAttribute ( 'value' ))))
            hq.setFatigue    ( Fatigue (int ( node.getChild ('fatigue').getAttribute ( 'value' ) )))
            
            # does it have any men at all?
            if hq.getMen () == 0:
                # no men, so this is a destriyed unit
                scenario.info.destroyed_units [ hq.getId ()] = hq
                hq.setDestroyed ()
            else:
                # unit is ok
                scenario.info.units [ hq.getId ()] = hq

            # get the position
            pos = node.getChild ('pos')
            hq.setPosition ( ( int ( pos.getAttribute ( 'x' ) ), int ( pos.getAttribute ( 'y' ) ) ))

            # get and set the the weapon
            weapon         = int ( node.getChild ('weapon').getAttribute ( 'id' ))
            okcount        = int ( node.getChild ('weapon').getAttribute ( 'ok' ))
            destroyedcount = int ( node.getChild ('weapon').getAttribute ( 'destroyed' ))
            
            # get the matching weapon. The weapon is an id that refers to previoulsy parsed
            # weapons stored locally
            hq.setWeapon ( self.weapons [ weapon ] )
            hq.setWeaponCounts ( okcount, destroyedcount )
                
        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <headquarter> node of scenario."
            return None

        #print "Parsed headquarter",hq

        # all is ok
        return hq

    

    def parseCommander (self, node):
        """This method parses out data of a <commander> tag. A new instance of the class Leader is
        created and returned if all is ok. If not then None is returned. This method differs a bit
        from the return-value semantics of the other methods."""
        # precautions are unnecessary here, as it was checked above

        try:
            # get the necessary attibutes
            name  = node.getAttribute ( 'name' )
            #rank  = "Captain"
            rank  = node.getChild ('rank').getData ()
            agg   = Aggressiveness ( int ( node.getChild ('aggressiveness').getAttribute ('value' )))
            exp   = Experience (int ( node.getChild ('experience').getAttribute ( 'value' ) ))
            rally = RallySkill (int ( node.getChild ('rally').getAttribute ( 'value' ) ))
            mot   = Motivation (int ( node.getChild ('motivation').getAttribute ( 'value' ) ))

            #print "Parsed commander name", name

            # create a new leader and return it
            return Leader ( name, rank, agg, exp, rally, mot )

        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <commander> node of scenario."
            return None


    def parseObjectives (self, node):
        """This method """
        # loop over all children this node has
        for tmpnode in node.getChildren ():

            # precautions
            if tmpnode.getName () != 'objective':
                # oops, invalid tag here
                print "Invalid data: '" + tmpnode.getName () + "', expected objective"
                return 0

            try:
                # get the necessary attibutes
                id     = int ( tmpnode.getAttribute ( 'id' ) )
                points = int ( tmpnode.getAttribute ( 'points' ) )
                x      = int ( tmpnode.getAttribute ( 'x' ) )
                y      = int ( tmpnode.getAttribute ( 'y' ) )
                owner  = tmpnode.getAttribute ( 'owner' )
                name   = tmpnode.getAttribute ( 'name' )
                desc   = tmpnode.getData ()
            
                # create a new obejctive
                objective = Objective ( id, (x, y) )

                # assign the other data
                objective.setName ( name )
                objective.setDescription ( desc )
                objective.setPoints ( points )

                # set the owner
                owners = { "union": UNION, "rebel": REBEL, "unknown": UNKNOWN }
                objective.setOwner(owners.get(owner, UNKNOWN))

                # add the new objective to the scenario data
                scenario.info.objectives.append ( objective )
                
            except KeyError:
                # oops, invalid data
                traceback.print_exc (file=sys.stdout)
                print "Invalid data in <objectives> node of scenario."
                raise
                return 0

        # all is ok
        return 1


    def parseWeapons (self, node):
        """This method parses the tag <weapons> and all the subtags <weapon> that should be in
        it. The weapons arw initialized and placed in an internal list, where they can be retrieved
        when needed for units."""
        # loop over all children this node has
        for tmpnode in node.getChildren ():
            # precautions
            if tmpnode.getName () != 'weapon':
                # oops, invalid tag here
                print "Invalid data: '" + tmpnode.getName () + "', expected weapon"
                return 0

            try:
                # get the necessary attibutes
                id       = int ( tmpnode.getAttribute ( 'id' ) )
                name     = tmpnode.getAttribute ( 'name' )
                type     = tmpnode.getAttribute ( 'type' )
                range    = int ( tmpnode.getAttribute ( 'range' ) )
                damage   = int ( tmpnode.getAttribute ( 'damage' ) )
                accuracy = int ( tmpnode.getAttribute ( 'accuracy' ) )
            
                # create a new weapon
                weapon = Weapon ( id, name, type, range, damage, accuracy )

                # add the new objective to the internal hash
                self.weapons [id] = weapon
                
            except KeyError:
                # oops, invalid data
                traceback.print_exc (file=sys.stdout)
                print "Invalid data in <weapon> node of scenario."
                return 0

        # all is ok
        return 1

    
    def parseLocations (self, node):
        """This method parses all data in the tag <location>, i.e. all specially named locations on
        the map. Creates instances of Location and stores in the global scenario data."""
        # loop over all children this node has
        for tmpnode in node.getChildren ():
            # precautions
            if tmpnode.getName () != 'location':
                # oops, invalid tag here
                print "Invalid data: '" + tmpnode.getName () + "', expected location"
                return 0

            try:
                # get the necessary attibutes
                x    = int ( tmpnode.getAttribute ( 'x' ) )
                y    = int ( tmpnode.getAttribute ( 'y' ) )
                name = tmpnode.getData ()
            
                # create a new location
                location = Location ( x, y, name )

                # add the new location to the scenario data
                scenario.info.locations.append ( location )
                
            except KeyError:
                # oops, invalid data
                traceback.print_exc (file=sys.stdout)
                print "Invalid data in <locations> node of scenario."
                return 0
       
        # all is ok
        return 1


    def parseMap (self, node):
        """This method """
        # precautions
        if node.getName () != 'map':
            # oops, invalid tag here
            print "Invalid data: '" + node.getName () + "', expected map"
            return 0

        try:
            # get the necessary attibutes
            xsize = int ( node.getAttribute ( 'xsize' ) )
            ysize = int ( node.getAttribute ( 'ysize' ) )
            
            # create a new map
            if properties.is_civil_editor:
                from editor.editor_map import EditorMap
                map = EditorMap (xsize, ysize)
            else:
                from map.map_displayable import MapDisplayable
                map = MapDisplayable (xsize, ysize)

            # store the map too
            scenario.map = map
            
            # loop over all children this node has
            for tmpnode in node.getChild ('hexes').getChildren ():
                # precautions
                if tmpnode.getName () != 'hex':
                    # oops, invalid tag here
                    print "Invalid data: '" + tmpnode.getName () + "', expected hex"
                    return 0

                # get the necessary attibutes
                terrain = int ( tmpnode.getAttribute ( 'terrain' ) )
                x       = int ( tmpnode.getAttribute ( 'x' ) )
                y       = int ( tmpnode.getAttribute ( 'y' ) )

                # Compatibility cruft if no height given
                try:
                    height  = int ( tmpnode.getAttribute ( 'height' ) )
                except:
                    height = 0

                # create a new hex
                hex = Hex ( terrain, height )

                # store the hex
                map.getHexes ()[y][x] = hex

            # do we have any extra features?
            features = node.getChild ('features')
            if features == None:
                # no such data, so we're done here
                return 1

            # loop over all the features we got
            for featurenode in features.getChildren ():
                # get all data from it
                type   = featurenode.getAttribute ( 'type' )
                id     = int ( featurenode.getAttribute ( 'id' ) )
                x      = int ( featurenode.getAttribute ( 'x' ) )
                y      = int ( featurenode.getAttribute ( 'y' ) )

                # and add the new feature to the map
                map.addFeature ( type, id, x, y )

        except KeyError:
            # oops, invalid data
            traceback.print_exc (file=sys.stdout)
            print "Invalid data in <hex> node of scenario."
            return 0


        # all is ok
        return 1
    

    def fixUnitTargets (self):
        """Loops over all units and sets the proper targets for them. While parsing the units the
        targets we set (for those that had them) to be the id:s of the targets, not the full
        units. Now all units are parsed, and we can assign the full units instead of the
        id:s. Returns 1 if all is ok and 0 on error."""

        try:
            # loop over all units we have
            for unit in scenario.info.units.itervalues ():
                # get the target
                targetid = unit.getTarget ()

                # is it an id?
                if targetid == None:
                    # no, next please
                    continue

                # get the target matching the id and assign it
                unit.setTarget ( scenario.info.units[ targetid ] )

        except KeyError:
            # oops, no such unit
            traceback.print_exc (file=sys.stdout)
            print "could not find unit %d, invalid target for unit %d" % (targetid, unit.getId () )
            return 0
        
                
###############################################################################################
# testing
if __name__ == '__main__':
    ScenarioParser ().parseFile ( '../scenarios/scenario10.xml' )
    print "ok"


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
