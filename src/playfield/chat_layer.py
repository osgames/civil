###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *
from layer         import Layer
import scenario
import properties


class ChatLayer(Layer):
    """
    This class defines a layer that plugs into the PlayField. It provides code for showing a dialog
    with info about a given weapon. The info includes all info that can be retrieved from the Weapon
    class. The user can using this dialog get a good idea as to what a weapon can do.

    To set the weapon this layer should show call 'setWeapon()'. This layer should be added among
    the last to the playfield, so that it is drawn on top over  everything else.

    TODO: combine this layer with the help layer to share code?

    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

        # no label yet
        self.label = None

        # load the font for the text
        self.font = pygame.font.Font ( properties.layer_chat_font,
                                       properties.layer_chat_font_size )

        # set starting y coordinate
        self.y = scenario.sdl.getHeight ()

        # subtract the height of the icons and the text size. Also make sure there is some slight
        # gap between the frame and the panel. Looks better?
        self.height = 0
        self.height += Layer.top.get_height ()
        self.height += Layer.bot.get_height ()
        self.height += self.font.get_height ()
        self.y -= self.height + 10

        # set horisontal coords
        self.width = scenario.sdl.getWidth () - Layer.left.get_width () - Layer.right.get_width() - 10
        self.x = Layer.left.get_width() + 5


    def setMessage (self, message):
        """Sets the message that should be displayed in the box. This is the message written by the
        player. The input is handled by the Chat state. This method will render a new label."""

        # do we have a message yet?
        if message == "":
            # nope, set it to nothing
            self.label = None
            return
        
        # create the label
        self.label = self.font.render ( message, 1, properties.layer_chat_color )


    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops and blits out all labels. The offsets are used so that we know how
        much the map has been scrolled."""
        
        # get starting position and size
        x      = self.x
        y      = self.y
        width  = self.width
        height = self.height

        # fill a part of the background with black so that we have something to paint on
        scenario.sdl.fill ( (0,0,0), (x - 1 , y - 1, width + 2, height + 2 ) )
    
        # paint the border first
        self.paintBorder ( x, y, width, height )

        # do we have a label yet?
        if self.label == None:
            # no label, go away
            return

        # add the borders before we paint the label
        x += Layer.left.get_width () + 5
        y += Layer.topleft.get_height ()

        # now blit out the label
        scenario.sdl.blit ( self.label, ( x, y ))
        

   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
