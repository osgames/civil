###############################################################################################
# $Id$
###############################################################################################

import sys
from layer    import Layer
import scenario
import properties
import pygame
import pygame.image
from pygame.locals import *


class GridLayer(Layer):
    """
    This class defines a layer that is used for showing a grid on the main map. It is drawn after
    the main terrain and will blit a simple grid upon the map so that the player knows the borders
    of hexes.

    This layer is transparent and will only obscure the positions where the icon borders are.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

        # load the icon for the grids
        self.icon = pygame.image.load ( properties.layer_grid_icon ).convert ()

        # set the transparent color for the icon and the surface
        self.icon.set_colorkey ( (255,255,255), RLEACCEL )

      

    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Simply blits out the already created surface."""

        # get the needed deltas
        deltax = properties.hex_delta_x
        deltay = properties.hex_delta_y

        # loop over the hexes in the map and create
        for y in xrange ( scenario.sdl.getHeight () / deltay ):
            for x in xrange ( scenario.sdl.getWidth () / deltax ):
                # is this an odd or even row?
                if y % 2:
                    # odd
                    scenario.sdl.blit ( self.icon, (x * 48 + 24, y * deltay ))
                else:
                    # even
                    scenario.sdl.blit ( self.icon, (x * 48, y * deltay ))

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
