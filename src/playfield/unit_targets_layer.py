###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *
from layer         import Layer
import scenario
import properties

class UnitTargetsLayer(Layer):
    """
    This class defines a layer that can visualize the attack targets all friendly units have. The
    verious forms of attack are shown with different colored lines from the attacker to the target. 

    The attack modes are:

    o skirmish
    o attack
    o assault
    
    Only the atack orders of friendly units are show.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

 
    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops over all available friendly units and paints their orders. The
        offsets are used so that we know how  much the map has been scrolled."""

        # get the tuple with the visible sizes (in hexes)
        visiblex, visibley = scenario.playfield.getVisibleSize ()
       
        # precalculate the min and max possible x and y values
        self.minx = offsetx * self.deltax
        self.miny = offsety * self.deltay
        self.maxx = (offsetx + visiblex) * self.deltax
        self.maxy = (offsety + visibley) * self.deltay

        # get the id of the own units
        local_player = scenario.local_player_id
        
        # loop over all units in the map
        for unit in scenario.info.units.itervalues ():
            # is the unit our and has it got a target?
            if unit.getOwner () == local_player  and  unit.getTarget () != None:
                # yep, handle the target for the unit
                self.visualizeUnitTargets ( unit )
 

    def visualizeUnitTargets (self, unit, ):
        """Visualizes the target for 'unit'. The unit is assumed to be a local unit owned by the
        local player. """
            
        # get the position, attack mode of the unit
        ourpos = unit.getPosition ()
        #mode = unit.getAttackMode ()
        mode = 0
        #print "UnitTargetsLayer.visualizeUnitTargets: using hardcoded attack mode"
        
        # get target position. we just assume the unit is found in the map
        targetpos = unit.getTarget ().getPosition ()
        
        # translate both our and the target's position so that they are within the playfield
        ourpos    = ( ourpos[0] - self.minx,    ourpos[1] - self.miny )
        targetpos = ( targetpos[0] - self.minx, targetpos[1] - self.miny )

        # draw a line from the source to the destination
        scenario.sdl.drawLine ( properties.layer_targets_mode_color [mode], ourpos, targetpos)


    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
