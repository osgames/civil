###############################################################################################
# $Id$
###############################################################################################

from layer import Layer

class MovementLayer(Layer):
    
    def __init__ (self, name):
        """Initializes the layer."""
        # call superclass constructor
        Layer.__init__(self, name)


    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops and paints out all movement orders for the friendly units. The
        offsets are used so that we know how  much the map has been scrolled."""
        pass
    

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
