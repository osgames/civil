###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *
from layer         import Layer
from organization  import Battallion, Regiment
from unit          import Headquarter
import scenario
import properties

class CommandersLayer(Layer):
    """
    This class defines a layer that can visualize the superior commanders of selected units. This
    works by painting a line from the company to its suerior and then lines from that superior to
    its superior and so on.
    
    Only the orders of friendly units are show.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

 
    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops over all available friendly units and paints their commanders. The
        offsets are used so that we know how  much the map has been scrolled."""

        # get the tuple with the visible sizes (in hexes)
        visiblex, visibley = scenario.playfield.getVisibleSize ()
        
        # precalculate the min and max possible x and y values
        self.minx = offsetx * self.deltax
        self.miny = offsety * self.deltay
        self.maxx = (offsetx + visiblex) * self.deltax
        self.maxy = (offsety + visibley) * self.deltay

        # get the id of the own units
        local_player = scenario.local_player_id
        
        # loop over all selected units
        for unit in scenario.selected:
            # is the unit visible
            if unit.getOwner () != local_player:
                # not our unit, get next
                continue

            # visualize the commanders for the unit
            self.visualizeCommanders ( unit )

            # visualize the subordinates for the unit
            self.visualizeUnits ( unit )
 

    def visualizeCommanders (self, unit):
        """Visualizes all commanders for 'unit' by painting lines from the unit to it superior and
        from that superior to its superior etc."""

        # get the superiors and position for the unit
        superiors  = unit.getSuperiors ()
        currentpos = unit.getPosition ()

        # loop over all superiors
        for hq in superiors:
            # is the hq destroyed?
            if hq.isDestroyed ():
                # hq is dead, next
                continue

            # get the unit's position
            pos = hq.getPosition ()
            
            # precaution to avoid unnecessary painting
            if currentpos != pos:
                # translate both source and dest so that they are within the playfield
                source = ( currentpos[0] - self.minx, currentpos[1] - self.miny )
                dest   = ( pos[0] - self.minx, pos[1] - self.miny )
                
                # draw a line from the source to the destination
                scenario.sdl.drawLine ( (128, 255, 128), source, dest )

                # store new source position
                currentpos = pos


    def visualizeUnits (self, unit):
        """Visualizes all companies that belong to a certain unit. This is only done for regiments
        and battallions that may have companies directly under their command. For brigades and
        higher organizations this is never done. So if a unit has the commander for a regiemnt or
        battallion, and there are companies directly under that commander (regiments may have only
        battallions under them) then lines are painted from 'unit' to the companies.

        Companies that are reinforcements are not visualized, as they are not on the battledfield
        yet. 
        """

        # are we a headquarter?
        if not isinstance ( unit, Headquarter ):
            # not a headquarter, so we have nu subordinate units
            return

        # get the units this unit is headquarter for
        organization = unit.getHeadquarterFor ()
        
        # yes, it's a headquarter, but is it a hq for a regiment or battallion?
        if not isinstance (organization, Battallion) and not isinstance (organization, Regiment):
            # no hq for anything we know
            return

        # does it have any companies?
        companies = organization.getCompanies ()
        if len ( companies ) == 0:
            # no companies, so it's a regiement with battallions or all units are eliminated
            #print "CommandersLayer.visualizeUnits:  unit %d has no subunits" % unit.getId ()
            return
            
        # get the unit'sposition
        hqx, hqy = unit.getPosition ()

        # loop over all companies
        for company in companies:
            # is the company still alive and has it arrived on the battledfield?
            if company.isDestroyed () or scenario.info.reinforcements.has_key ( company.getId () ):
                # yes, it's a goner or reinforcement, then we shouldn't visualize it
                continue
            
            # get unit position
            unitx, unity = company.getPosition ()

            # translate both source and dest so that they are within the playfield
            source = ( unitx - self.minx, unity - self.miny )
            dest   = ( hqx   - self.minx, hqy - self.miny )
                
            # draw a line from the source to the destination
            scenario.sdl.drawLine ( (0, 128, 255), source, dest )

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
