###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *

from layer         import Layer
from constants     import UNION, REBEL, UNKNOWN
import scenario
import properties

from location_layer import LocationLayer

class ObjectiveLayer(LocationLayer):
    """
    This class defines a layer that is used for showing all the objectives on the map. An objective
    will be drawn as a symbol on a hex. As there are relatively few objectives (normally) this layer
    is not a full surface, but instead single small icons are blitted out.

    Separate icons are used for when the objective is owned by union or rebel forces, or if the
    owner is unknown or neutral.

    Objectives are not painted withing hexes, i.e. their positions are exact pixel positions.

    NOTE: We are a subclass of LocationLayer, in the same way that Objectives are a subclass
    of Location. See self.getLocations()
    """

    def __init__ (self, name):
        """Initializes the layer. Loads the icons for the objectives from files. If the loading fails
        the method will exit the application."""
        # call superclass constructor
        LocationLayer.__init__ ( self, name )

        self.icons = {}
        
        try:
            # load the icon for the objectives
            union   = pygame.image.load ( properties.layer_objective_icon_union ).convert ()
            rebel   = pygame.image.load ( properties.layer_objective_icon_rebel ).convert ()
            unknown = pygame.image.load ( properties.layer_objective_icon_unknown ).convert ()
 
            # set the transparent color for the icons
            union.set_colorkey   ( (255,255,255), RLEACCEL )
            rebel.set_colorkey   ( (255,255,255), RLEACCEL )
            unknown.set_colorkey ( (255,255,255), RLEACCEL )

            # store in a hash for later access. Use the owner as the key
            self.icons[UNION] = union
            self.icons[REBEL] = rebel
            self.icons[UNKNOWN] = unknown

        except:
            # failed to load icons
            print "failed to load icons for objectives, exiting."
            sys.exit ( 1 )
  
    def getLocations(self):
        """Return which locations are valid for the layer, and the delta coordinates
        to apply for the label text. Because Objectives paint a star at
        the exact location, so the text must be slightl right and a lot down..."""
        return { "locations": scenario.info.objectives, "deltax": 16, "deltay": 40 }

    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops and blits out all objectives."""
        # get the tuple with the visible sizes and extract the x- and y-dimensions
        visiblex, visibley = scenario.playfield.getVisibleSize ()

        # precalculate the min and max possible x and y values
        minx = (offsetx - 1 ) * self.deltax
        maxx = (offsetx + visiblex + 1) * self.deltax
        miny = (offsety - 1 ) * self.deltay
        maxy = (offsety + visibley) * self.deltay
        
        # loop over all objectives we have
        for obj in scenario.info.objectives:
            # get its position
            x, y = obj.getPosition ()

            # is the objective visible currently?
            if minx <= x <= max:
                if miny <= y <= maxy:
                    
                    # yep, it's visible so decide the icon to use
                    icon = self.icons [ obj.getOwner () ]
               
                    # yep, it's visible now perform the actual raw blit of the icon
                    scenario.sdl.blit ( icon, (x - offsetx * self.deltax, y - offsety * self.deltay) )

        LocationLayer.paint(self, offsetx, offsety, dirtyrect)
                    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
