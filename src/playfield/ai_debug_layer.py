###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *

from layer    import Layer
import scenario
import properties


class AIDebugLayer(Layer):
    """
    This class defines a layer that is used for debugging the AI. It shows a label at the center of
    the hex. The value of the label is just some text that makes sense when debugging the AI.

    This class can also be used as a baseclass for other debugging layers that just need to write
    some short label for each hex. To do that just subclass it and override the getHexValue() method
    and defined one that returns the wanted value.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

        # store a dict of generated labels
        self.labels = {}

        # load the font for the labels
        self.labelfont = pygame.font.Font ( properties.layer_locations_font,
                                            properties.layer_locations_font_size )


    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Simply blits out the already created surface."""

        # get the needed deltas
        deltax = properties.hex_delta_x
        deltay = properties.hex_delta_y
        
        # loop over the hexes in the map and create
        for y in xrange ( scenario.sdl.getHeight () / deltay ):
            for x in xrange ( scenario.sdl.getWidth () / deltax ):

                # get the value for this hex
                value = self.getHexValue (x, y)
                
                # do we have a label for this value?
                if not self.labels.has_key ( value ):
                    # nope, so create one
                    surface = self.labelfont.render ( "%d" % value, 1, properties.layer_locations_color )

                    # store it for later use
                    self.labels [ value ] = surface
                else:
                    # get it
                    surface = self.labels [ value ]
                    
                # is this an odd or even row?
                if y % 2:
                    # odd
                    scenario.sdl.blit ( surface, (x * 48 + 24, y * deltay ))
                else:
                    # even
                    scenario.sdl.blit ( surface, (x * 48, y * deltay ))


    def getHexValue (self, x, y):
        """Returns the valud the hex (x,y) should have. This method can be overridden by subclasses
        if needed. The value is currently an integer value. If floats etc are needed then the line
        that rneders the lable in the paint() method must be changed too."""
        
        # this is an example only, return the sum of the coords
        return x + y
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
