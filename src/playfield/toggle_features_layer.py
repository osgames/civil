###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
from pygame.locals   import *

from layer           import Layer
from dialog_layer    import DialogLayer
import scenario
import properties


class ToggleFeaturesLayer(DialogLayer):
    """
    This class defines a layer that plugs into the PlayField. It provides code a simple dialog that
    lists a number of other playfields and lets the player toggle their visibility. The visualizing
    is done by presenting the player with a checkbox for the layer, along with a name for it. The
    player can then select the layers that should be visible and click 'Ok'.
    
    """


    def __init__ (self, name):
        "Initializes the layer. The checkboxes and labels are not created here."
        # call superclass constructor. note that that size is totally vapour, we'll fill it in layer
        # in the constructor
        DialogLayer.__init__ ( self, name, 100, 100, properties.layer_togglefeatures_button )

        # set the margins we use from the origin. this gives some padding to the border
        self.marginx = 15
        self.marginy = 10

        # set the title
        self.title = self.createLabel ( "Select visible features" )

        # no coords yet
        self.coords = []

        # checked status for all layers
        self.checkedstatus = {}

        # get the checkbox width and height
        self.checkwidth  = Layer.checkbox[0].get_width ()
        self.checkheight = Layer.checkbox[0].get_height ()


    def setLayers (self, names):
        """Sets the names of the layers that this dialog should allow the player to toggle. Creates
        the checkboxes and labels for the layers."""

        # store the names
        self.names = names

        # start from a given y coordinate
        y = self.marginy * 2 + self.title.get_height ()
        
        # loop over all allowed resolutions we can find
        index = 0
        for title, name in names:
            # now create a label for this resolution
            label = self.createLabel ( title )

            # create coordinates for exactly where it will be put. we can later use these coords to
            # easily check wether a checkbox got clicked
            checkx1 = self.marginx
            checky1 = y
            checkx2 = self.marginx + self.checkwidth
            checky2 = y + self.checkheight - 15

            # add them along with the label
            self.coords.append ( ( checkx1, checky1, checkx2, checky2, label, name ) )

            # add to the y coordinate
            y += self.checkheight - 15

            # find the layer with the given name and see if it's visible at the moment
            visible = self.getLayerVisibility ( name )

            # store the visibility status
            self.checkedstatus [ name ] = visible
 
            # next index
            index += 1

        # now we know the height
        self.setSize ( 200, y )
        

    def handleLeftMousePressed (self, clickx, clicky):
        """Method that handles a press with the left mouse button. Checks if any of the checkboxes
        have been pressed, and if so then toggles the status of that checkbox."""

        # translate the coords so that they are within the coords that the checkboxes are given
        # using
        clickx -= self.x
        clicky -= self.y

        # loop over all labels along with the checkbox coords
        index = 0
        for checkx1, checky1, checkx2, checky2, label, name in self.coords:
            # is the click inside this button?
            if checkx1 <= clickx <= checkx2 and checky1 <= clicky <= checky2:
                # we have it here, get the old status
                checked = self.checkedstatus [ name ]

                # store new status by using this extremely clever thing.
                self.checkedstatus [ name ] = (1, 0)[checked]
                break

            # not yet, next index
            index += 1
 
        # repaint the playefield
        scenario.playfield.needInternalRepaint ( self )

    
    def customPaint (self):
        """Paints the layer by painting the title and the checkboxes along with the labels."""
         
        # paint the title. the coordinates are inherited, and refer to the topleft corner of the
        # content area we're given
        scenario.sdl.blit ( self.title, ( self.x + self.marginx, self.y + self.marginy))

        # loop over all labels along with the checkbox coords
        index = 0
        for checkx1, checky1, checkx2, checky2, label, name in self.coords:
            # add the needed x and y coordinates so that we paint within the dialog 
            checkx1 += self.x
            checkx2 += self.x
            checky1 += self.y
            checky2 += self.y

            # should it be checked?
            use = self.checkedstatus [ name ]
            
            # we need some extra offset to the y for the label to align it nicely with the
            # checkbox. 
            extray = self.checkheight / 2 - label.get_height () / 2

            # do the blit of the checkbox and the label. also 
            scenario.sdl.blit ( Layer.checkbox[use],  ( checkx1, checky1 ))
            scenario.sdl.blit ( label, ( checkx2 + self.marginx, checky1 + extray ))

            # next please
            index += 1


    def getLayerVisibility (self, name):
        """Checks wether the layer with 'name' is visible or not. Returns 1 if it is and 0 if
        not. The reason for this method is that we have one layer that handles two types of things,
        and it has to be checked separately."""
        # are we checking the own symblic icons?
        if name == 'own_units_symbols':
            return scenario.playfield.getLayer ( 'own_units' ).unitSymbolsShown ()
            
        # are we checking the own nice guys?
        elif name == 'own_units_icons':
            return scenario.playfield.getLayer ( 'own_units' ).unitIconsShown ()

        # are we checking the enemy symbols?
        elif name == 'enemy_units_symbols':
            return scenario.playfield.getLayer ( 'enemy_units' ).unitSymbolsShown ()
        
        # are we checking the enemy nice guys?
        elif name == 'enemy_units_icons':
            return scenario.playfield.getLayer ( 'enemy_units' ).unitIconsShown ()
            
        # no, a normal layer
        return scenario.playfield.getLayer ( name ).isVisible ()


    def updateLayerVisibility (self):
        """Updates the visibility for all layers. Sets all layers as either hidden or shown."""

        # loop over all layer names that we got
        for name, visible in self.checkedstatus.items ():
            # are we setting the own symblic icons?
            if name == 'own_units_symbols':
                scenario.playfield.getLayer ( 'own_units' ).showUnitSymbols ( visible )
            
            # are we setting the own nice guys?
            elif name == 'own_units_icons':
                scenario.playfield.getLayer ( 'own_units' ).showUnitIcons ( visible )

            # are we setting the enemy symbols?
            elif name == 'enemy_units_symbols':
                scenario.playfield.getLayer ( 'enemy_units' ).showUnitSymbols ( visible )
        
            # are we setting the enemy nice guys?
            elif name == 'enemy_units_icons':
                scenario.playfield.getLayer ( 'enemy_units' ).showUnitIcons ( visible )
            
            else:
                # a normal layer, get the layer
                layer = scenario.playfield.getLayer ( name )

                # make it visisble or hidden as desired
                scenario.playfield.setVisible ( layer, visible )
   

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
