###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *
from layer         import Layer
import scenario
import properties

class WeaponRangeLayer(Layer):
    """
    This class defines a layer that can visualize the maximum attack range for the primary weapon
    for the unit. The range is visualized as a circle centered around the unit.
    
    Only the attack range of friendly units are show.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

        # get the color and width of the circle
        self.color  = properties.layer_weapon_range_color
        self.weaponwidth  = properties.layer_weapon_range_width

 
    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops over all selected friendly units and paints the range circles."""

        # get the tuple with the visible sizes (in hexes)
        visiblex, visibley = scenario.playfield.getVisibleSize ()
       
        # precalculate the min and max possible x and y values
        self.minx = offsetx * self.deltax
        self.miny = offsety * self.deltay
        self.maxx = (offsetx + visiblex) * self.deltax
        self.maxy = (offsety + visibley) * self.deltay

        # get the id of the own units
        local_player = scenario.local_player_id
        
        # loop over all selected units 
        for unit in scenario.selected:
            # is the unit our and has it got a target?
            if unit.getOwner () == local_player:
                # yep, handle the target for the unit
                self.visualizeUnitRange ( unit )
 

    def  visualizeUnitRange (self, unit):
        """Visualizes the range of the weapon for 'unit'. The visualization is done using a circle
        centered on the unit. The unit is assumed to be a local unit owned by the local player. """
            
        # get the position, attack mode of the unit
        pos = unit.getPosition ()

        # the range of the primary weapon
        rng = unit.getWeapon ().getRange ()
        
        # translate our position so that it is within the playfield
        pos = ( pos[0] - self.minx, pos[1] - self.miny )

        # draw a circle centered at the unit
        scenario.sdl.drawCircle ( self.color, pos, rng, self.weaponwidth )


    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
