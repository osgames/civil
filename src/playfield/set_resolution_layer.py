###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
from pygame.locals   import *

from layer           import Layer
from dialog_layer    import DialogLayer
import scenario
import properties


class SetResolutionLayer(DialogLayer):
    """
    This class defines a layer that plugs into the PlayField. It provides code...
    """


    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor. note that that size is totally vapour, we'll fill it in layer
        # in the constructor
        DialogLayer.__init__ ( self, name, 100, 100, properties.layer_setresolution_button )

        # set the margins we use from the origin. this gives some padding to the border
        self.marginx = 15
        self.marginy = 10

        # set the title
        self.title = self.createLabel ( "Select resolution" )

        # no coords yet
        self.coords = []

        # get the checkbox width and height
        self.checkwidth  = Layer.checkbox[0].get_width ()
        self.checkheight = Layer.checkbox[0].get_height ()

        # start from a given y coordinate
        y = self.marginy * 2 + self.title.get_height ()
        
        # loop over all allowed resolutions we can find
        index = 0
        for resx, resy in pygame.display.list_modes ():
            # now create a label for this resolution
            label = self.createLabel ( '%d x %d' % ( resx, resy ) )

            # create coordinates for exactly where it will be put. we can later use these coords to
            # easily check wether a checkbox got clicked
            checkx1 = self.marginx
            checky1 = y
            checkx2 = self.marginx + self.checkwidth
            checky2 = y + self.checkheight

            # add them along with the labe
            self.coords.append ( ( checkx1, checky1, checkx2, checky2, label ) )

            # add to the y coordinate
            y += self.checkheight - 5

            # should this one be checked?
            if (resx, resy) == scenario.sdl.getSize ():
                # yep, mark it
                self.checked = index

            # next index
            index += 1

        self.gui_checked = None
        
        # now we know the height
        self.setSize ( 200, y )
        

    def getResolution (self):
        """Returns the selected resolution as a (width, height) tuple."""
        # just index the modes with our checked label
        return pygame.display.list_modes ()[ self.checked ]


    def handleLeftMousePressed (self, clickx, clicky):
        """Method that handles a press with the left mouse button. Checks if any of the checkboxes
        have been pressed, and if so then sets the internal flag that indicates that something
        changed."""

        # translate the coords so that they are within the coords that the checkboxes are given
        # using
        clickx -= self.x
        clicky -= self.y

        # loop over all labels along with the checkbox coords
        index = 0
        for checkx1, checky1, checkx2, checky2, label in self.coords:
            # is the click inside this button?
            if checkx1 <= clickx <= checkx2 and checky1 <= clicky <= checky2:
                # we have it here
                self.checked = index

            # not yet, next index
            index += 1
 
        # repaint the playefield
        scenario.playfield.needInternalRepaint ( self )

        #print "SetResolutionLayer.handleLeftMousePressed: ", self.checked

    
    def customPaint (self):
        """Paints the layer by painting the title and the checkboxes along with the labels."""
         
        # paint the title. the coordinates are inherited, and refer to the topleft corner of the
        # content area we're given
        if not self.need_internal_repaint:
            scenario.sdl.blit ( self.title, ( self.x + self.marginx, self.y + self.marginy))

        # loop over all labels along with the checkbox coords
        index = 0
        for checkx1, checky1, checkx2, checky2, label in self.coords:
            # add the needed x and y coordinates so that we paint within the dialog 
            checkx1 += self.x
            checkx2 += self.x
            checky1 += self.y
            checky2 += self.y

            # should it be checked?
            if index == self.checked:
                # this one is checked
                use = 1
            else:
                # nope, draw an unchecked
                use = 0

            # we need some extra offset to the y for the label to align it nicely with the
            # checkbox. 
            extray = self.checkheight / 2 - label.get_height () / 2

            if self.need_internal_repaint:
                if index == self.checked or index == self.gui_checked:
                    # Fill empty
                    scenario.sdl.fill ( (0,0,0), ( checkx1, checky1,
                                                   max(Layer.checkbox[0].get_width(),
                                                       Layer.checkbox[1].get_width()),
                                                   max(Layer.checkbox[0].get_height(),
                                                       Layer.checkbox[1].get_height())) )
                    # do the blit of the checkbox
                    scenario.sdl.blit ( Layer.checkbox[use],  ( checkx1, checky1 ))
            else:
                # do the blit of the checkbox and the label. also 
                scenario.sdl.blit ( Layer.checkbox[use],  ( checkx1, checky1 ))
                scenario.sdl.blit ( label, ( checkx2 + self.marginx, checky1 + extray ))

            # next please
            index += 1

        # Update gui information, last!
        self.gui_checked = self.checked
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
