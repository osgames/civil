###############################################################################################
# $Id$
###############################################################################################

import sys
import pygame
import pygame.image
from pygame.locals import *
from layer         import Layer
from location      import Location
import scenario
import properties


class LocationLayer(Layer):
    """
    This class defines a layer that plugs into the PlayField. It provides code for drawing various
    locations as labels on the map. The labels can be for various parts of the map, such as geographical
    features, objectives etc.

    A simple text label is rendered using a font given in the properties file, and painted on the
    map when the layer is painted.
    """

    def __init__ (self, name):
        "Initializes the layer."
        # call superclass constructor
        Layer.__init__ ( self, name )

        # setup all labels
        self.createLabels ()


    def getLocations(self):
        """Returns which locations are valid, and what delta coordinates
        to apply. See also ObjectiveLayer.getLocations()."""
        return { "locations": scenario.info.locations, "deltax": 0, "deltay": 0 }

    def createLabels (self):
        """Creates all labels. This loops over the labels that were parsed from the scenario file
        and creates tuples into an internal list self.labels. The tuples are of the form
        (x,y,surface) where the surface is the SDL surface ready for blitting out."""
        # no labels yet
        self.labels = []

        # load the font for the labels
        labelfont = pygame.font.Font ( properties.layer_locations_font,
                                       properties.layer_locations_font_size )

        # loop over the global location labels we have
        h = self.getLocations()
        for location in h["locations"]:
            # create the surface and the shadow
            surface = labelfont.render ( location.getName (), 1, properties.layer_locations_color )
            shadow  = labelfont.render ( location.getName (), 1, properties.layer_locations_shadow )

            # get the position of the label
            x, y = location.getPosition ()

            # offet the x and y a little bit so that they get where the middle of the label is, not
            # the left upper corner
            mylabel = pygame.Surface((surface.get_width() + 2, surface.get_height() + 2))
            mylabel.fill( (0, 0, 0) )
            
            mylabel.blit(shadow,  (0, 0) )
            mylabel.blit(shadow,  (2, 0) )
            mylabel.blit(shadow,  (0, 2) )
            mylabel.blit(shadow,  (2, 2) )
            mylabel.blit(surface, (1, 1) )

            mylabel.set_colorkey( (0, 0, 0), pygame.RLEACCEL)

            x -= mylabel.get_width () / 2
            y -= mylabel.get_height () / 2

            x += h["deltax"]
            y += h["deltay"]
            # store the label along with the cooridinates. We store them as a tuple
            self.labels.append ( (x, y, mylabel) )
            

    def paint (self, offsetx, offsety, dirtyrect=None):
        """Paints the layer. Loops and blits out all labels. The offsets are used so that we know how
        much the map has been scrolled."""

        # get the tuple with the visible sizes (in hexes) and the current offset (in hexes)
        visiblex, visibley = scenario.playfield.getVisibleSize ()
      
        # precalculate the min and max possible x and y values
        minx = offsetx * self.deltax
        miny = offsety * self.deltay
        maxx = (offsetx + visiblex) * self.deltax
        maxy = (offsety + visibley) * self.deltay
 
        # loop over all labels we have
        for labelx, labely, surface in self.labels:

            # is the label visible on the playfield?
            if labelx >= minx and labelx <= maxx and labely >= miny and labely <= maxy:
                # yep, translate the position so that it is within the screen
                labelx -= minx
                labely -= miny

                # now blit out the actual text in the middle of the outline
                scenario.sdl.blit ( surface, ( labelx, labely ))

 
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
