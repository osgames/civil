###############################################################################################
# $Id$
###############################################################################################

from properties import path_sound

"""
This file contains a big dictionary that maps logical names (strings) to the sample that should be
played when that logical name is requested. The files should be normal wav files that are located
somewhere under the $path_sound/sound directory.
"""

# all samples
samplenames = { 'button-clicked'   : path_sound + 'button-clicked.wav',
                'checkbox-toggled' : path_sound + 'checkbox-toggled.wav',
                'firing_artillery' : path_sound + 'firing_artillery.wav',
                'firing_infantry'  : path_sound + 'firing_infantry.wav',
                'explosion'        : path_sound + 'explosion.wav',
                'fanfare'          : path_sound + 'fanfare.wav'
                }
                   

#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
