###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class ChangingToFormation (Mode):
    """
    This implements the mode 'formation'. It is used by infantry as an intermediate mode while changing
    to formation mode.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "changingtoformation", "Changing to formation" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatingformation"
        self.onskirmish   = ""
        self.ondone       = "formation"
        self.onmelee      = "meleeingformation"
        self.onassault    = ""
        self.onchangepolicy = "changingtoformation"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1.0
        
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
