###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Formation (Mode):
    """
    This implements the mode 'formation'. It is used by infantry only and means that the troops are
    laid out in a battle formation, i.e. on a line. This mode is used in combat and gives the unit
    good possibilities to advance, defend etc.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "formation", "formation" )

        # set the modes we change to
        self.onchangemode   = "changingtocolumn"
        self.onmove         = "formationmove"
        self.onmovefast     = ""
        self.onrotate       = "formation"
        self.onhalt         = ""
        self.onretreat      = "retreatingformation"
                            
        self.onskirmish     = "formationskirmish"
        self.onmelee        = "meleeingformation"
        self.onassault      = "formationassault"
                            
        self.ondone         = "formation"
        self.onchangepolicy = "formation"
        self.onwait         = "formation"
        
        # set a base fatigue
        self.base_fatigue = -1

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
