###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class RoutedMoveArtillery (Mode):
    """
    This implements the mode 'routed'. It is used by artillery units when they are routed and moving
    away from the enemy, either by having taken a punishment or when the player has chosen to
    retreat the unit manually. As long as the unit is moving it will keep this state, and when it
    stops it will get the state 'routedartillery'.  """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "routedmoveartillery", "routed move" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        #self.onretreat     = ""
        #self.onskirmish    = ""
        self.ondone         = "routedartillery"
        self.onmelee        = "meleeinglimbered"
        self.onassault      = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        self.onrally        = ""
        self.ondisorganize  = ""
        
        # set a base fatigue
        self.base_fatigue = 3.0

    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
