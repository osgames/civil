###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Column (Mode):
    """
    This implements the mode 'column'. It is used by infantry only and means that the troops are
    ready for marching. This means that the troops will march a few men wide and as a long
    column. This mode is used for moving the troops longer distances when not engaged.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "column", "column" )

        # set the modes we change to
        self.onchangemode = "changingtoformation"
        self.onmove       = "columnmove"
        self.onmovefast   = "columnmovefast"
        self.onrotate     = "column"
        self.onhalt       = ""
        self.onretreat    = "retreatingcolumn"
        self.onskirmish   = "columnskirmish"
        self.ondone       = "column"
        self.onmelee      = "meleeingcolumn"
        self.onassault    = ""
        self.onchangepolicy = "column"
        self.onwait         = "column"
        
        # set a base fatigue
        self.base_fatigue = -2.0
        
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
