###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Limbered (Mode):
    """
    This implements the mode 'limbered'. It is used by artillery when the guns are limbered and
    ready for transport. Artillery can not move unless limbered.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "limbered", "limbered" )

        # set the modes we change to
        self.onchangemode = "unlimbering"
        self.onmove       = "limberedmove"
        self.onmovefast   = ""
        self.onrotate     = "limbered"
        self.onhalt       = ""
        self.onretreat    = "retreatinglimbered"
        self.onskirmish   = ""
        self.ondone       = "limbered"
        self.onmelee      = "meleeinglimbered"
        self.onassault    = ""
        self.onchangepolicy = "limbered"
        self.onwait         = "limbered"
        
        # set a base fatigue
        self.base_fatigue = -2
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
