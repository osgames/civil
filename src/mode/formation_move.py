###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class FormationMove (Mode):
    """
    This implements the mode 'formation move'. It is used by infantry that is moving in a formation
    mode. Infantry having the mode 'formation' which then moves normally will have this mode. Moving
    in formation is slower than moving in column.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "formationmove", "moving in formation" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "formationmove"
        self.onmovefast   = ""
        self.onrotate     = "formationmove"
        self.onhalt       = "formation"
        self.onretreat    = "retreatingformation"
        self.onskirmish   = ""
        self.ondone       = "formation"
        self.onmelee      = "meleeingformation"
        self.onassault    = ""
        self.onchangepolicy = "formationmove"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 2
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
