###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class FormationAssault (Mode):
    """
    This implements the mode 'formationassault'. It is used by infantry only and means that the troops are
    laid out in a battle formation, i.e. on a line and firing at an anamy unit. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "formationassault", "assault" )

        # set the modes we change to
        self.onchangemode   = ""
        self.onmove         = "formationmove"
        self.onmovefast     = ""
        self.onrotate       = "formation"
        self.onhalt         = "formation"
        self.onretreat      = "retreatingformation"
        self.ondone         = "formation"
                            
        self.onskirmish     = "formationskirmish"
        self.onmelee        = "meleeingformation"
        self.onassault      = "formationassault"
        self.onchangepolicy = "formationassault"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 4

    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
