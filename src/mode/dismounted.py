###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Dismounted (Mode):
    """
    This implements the mode 'dismounted'. It is used by cavalry when the troops are dismounted. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "dismounted", "dismounted" )

        # set the modes we change to
        self.onchangemode = "mounting"
        self.onmove       = "dismountedmove"
        self.onmovefast   = ""
        self.onrotate     = "dismounted"
        self.onhalt       = ""
        self.onretreat    = "retreatingdismounted"
        self.onskirmish   = "dismountedskirmish"
        self.ondone       = "dismounted"
        self.onmelee      = "meleeingdismounted"
        self.onassault    = ""
        self.onchangepolicy = "dismounted"
        self.onwait         = "dismounted"
        
        # set a base fatigue
        self.base_fatigue = -1.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
