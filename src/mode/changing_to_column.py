###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class ChangingToColumn (Mode):
    """
    This implements the mode 'column'. It is used by infantry as an intermediate mode while changing
    to column mode.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "changingtocolumn", "Changing to column" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatingcolumn"
        self.onskirmish   = ""
        self.ondone       = "column"
        self.onmelee      = "meleeingcolumn"
        self.onassault    = ""
        self.onchangepolicy = "changingtocolumn"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1.0
        
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
