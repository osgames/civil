###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Limbering (Mode):
    """
    This implements the mode 'limbering'. It is used by artillery as an intermediate mode when the
    guns are being limbered and ready for transport. Artillery can not move unless limbered.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "limbering", "limbering" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatinglimbered"
        self.onskirmish   = ""
        self.ondone       = "limbered"
        self.onmelee      = "meleeinglimbered"
        self.onassault    = ""
        self.onchangepolicy = "limbering"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 4
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
