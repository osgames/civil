###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Mounting (Mode):
    """
    This implements the mode 'mounting'. It is used by cavalry  as an intermediate mode while the
    troops are mounting on their horses. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "mounting", "mounting" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatingmounted"
        self.onskirmish   = ""
        self.ondone       = "mounted"
        self.onmelee      = "meleeingmounted"
        self.onassault    = ""
        self.onchangepolicy = "mounting"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 4.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
