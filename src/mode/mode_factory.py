###############################################################################################
# $Id$
###############################################################################################

from changing_to_column      import ChangingToColumn
from changing_to_formation   import ChangingToFormation
from column                  import Column
from column_move             import ColumnMove
from column_move_fast        import ColumnMoveFast
from column_skirmish         import ColumnSkirmish
from formation               import Formation
from formation_move          import FormationMove
from formation_skirmish      import FormationSkirmish
from formation_assault       import FormationAssault
from mounted                 import Mounted
from mounted_move            import MountedMove
from mounted_move_fast       import MountedMoveFast
from mounted_skirmish        import MountedSkirmish
from mounted_assault         import MountedAssault
from mounting                import Mounting
from disorganized_column     import DisorganizedColumn     
from disorganized_formation  import DisorganizedFormation  
from disorganized_limbered   import DisorganizedLimbered   
from disorganized_unlimbered import DisorganizedUnlimbered 
from disorganized_mounted    import DisorganizedMounted    
from disorganized_unmounted  import DisorganizedUnmounted  
from dismounted              import Dismounted
from dismounted_move         import DismountedMove
from dismounted_skirmish     import DismountedSkirmish
from dismounting             import Dismounting
from limbered                import Limbered
from limbered_move           import LimberedMove
from limbering               import Limbering
from unlimbered              import Unlimbered
from unlimbered_skirmish     import UnlimberedSkirmish
from unlimbering             import Unlimbering
from retreating_column       import RetreatingColumn     
from retreating_formation    import RetreatingFormation  
from retreating_limbered     import RetreatingLimbered   
from retreating_unlimbered   import RetreatingUnlimbered 
from retreating_mounted      import RetreatingMounted    
from retreating_unmounted    import RetreatingUnmounted  
from routed_move_infantry    import RoutedMoveInfantry
from routed_move_cavalry     import RoutedMoveCavalry
from routed_move_artillery   import RoutedMoveArtillery
from routed_infantry         import RoutedInfantry
from routed_cavalry          import RoutedCavalry
from routed_artillery        import RoutedArtillery
from meleeing_column         import MeleeingColumn
from meleeing_formation      import MeleeingFormation
from meleeing_limbered       import MeleeingLimbered
from meleeing_unlimbered     import MeleeingUnlimbered
from meleeing_mounted        import MeleeingMounted
from meleeing_unmounted      import MeleeingUnmounted

# a map of all modes
modes = {
    'column':                  Column,
    'columnmove':              ColumnMove,
    'columnmovefast':          ColumnMoveFast,
    'columnskirmish':          ColumnSkirmish,
    'changingtocolumn':        ChangingToColumn,
    'dismounted':              Dismounted,
    'dismountedmove':          DismountedMove,
    'dismountedskirmish':      DismountedSkirmish,
    'dismounting':             Dismounting,
    'disorganizedcolumn':      DisorganizedColumn,
    'disorganizedformation':   DisorganizedFormation,
    'disorganizedlimbered':    DisorganizedLimbered,
    'disorganizedunlimbered':  DisorganizedUnlimbered,
    'disorganizedmounted':     DisorganizedMounted,
    'disorganizedunmounted':   DisorganizedUnmounted,
    'formation':               Formation,
    'formationmove':           FormationMove,
    'formationskirmish':       FormationSkirmish,
    'formationassault':        FormationAssault,
    'changingtoformation':     ChangingToFormation,
    'limbered':                Limbered,
    'limbering':               Limbering,
    'limberedmove':            LimberedMove,
    'meleeingcolumn':          MeleeingColumn,
    'meleeingformation':       MeleeingFormation,
    'meleeinglimbered':        MeleeingLimbered,
    'meleeingunlimbered':      MeleeingUnlimbered,
    'meleeingmounted':         MeleeingMounted,
    'meleeingunmounted':       MeleeingUnmounted,
    'mounted':                 Mounted,
    'mountedmove':             MountedMove,
    'mountedmovefast':         MountedMoveFast,
    'mountedskirmish':         MountedSkirmish,
    'mountedassault':          MountedAssault,
    'mounting':                Mounting,
    'retreating_column':       RetreatingColumn,
    'retreating_formation':    RetreatingFormation,
    'retreating_limbered':     RetreatingLimbered,
    'retreating_unlimbered':   RetreatingUnlimbered,
    'retreating_mounted':      RetreatingMounted,
    'retreating_unmounted':    RetreatingUnmounted,
    'routedartillery':         RoutedArtillery,
    'routedcavalry':           RoutedCavalry,
    'routedartillery':         RoutedArtillery,
    'routedmoveinfantry':      RoutedMoveInfantry,
    'routedmovecavalry':       RoutedMoveCavalry,
    'routedmoveartillery':     RoutedMoveArtillery,
    'unlimbered':              Unlimbered,
    'unlimberedskirmish':      UnlimberedSkirmish,
    'unlimbering':             Unlimbering
}

def createMode (mode):
    """This function works as a factory for creating instances of Mode subclases based on the passed
    string. The string should be the name of the wanted mode, just as the method 'getName()' on the
    mode would return. A new Mode subclass is returned."""
     
    # just create a new mode based on the name
    try:
        return modes [mode]()
    except:
        # no such mode?
        raise ValueError ( "mode '%s' is unknown to the factory" % mode )


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
