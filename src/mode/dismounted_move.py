###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class DismountedMove (Mode):
    """
    This implements the mode 'dismounted'. It is used by cavalry when the troops are dismounted and
    the unit is moving. This mode is slower for movement that 'mounted move'.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "dismountedmove", "moving dismounted" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "dismountedmove"
        self.onmovefast   = ""
        self.onrotate     = "dismountedmove"
        self.onhalt       = "dismounted"
        self.onretreat    = "retreatingdismounted"
        self.onskirmish   = ""
        self.ondone       = "dismounted"
        self.onmelee      = "meleeingdismounted"
        self.onassault    = ""
        self.onchangepolicy = "dismountedmove"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 2
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
