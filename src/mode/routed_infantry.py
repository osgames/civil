###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class RoutedInfantry (Mode):
    """
    This implements the mode 'routed'. It is used by infantry units when they are routed,
    either by having taken a punishment or when the player has chosen to retreat the unit
    manually. As long as the unit is moving it will keep this state, and when it stops it will get
    the state 'disorganized'.  """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "routedinfantry", "routed" )
 
        # set the modes we change to
        self.onchangemode   = ""
        self.onmove         = ""
        self.onmovefast     = ""
        self.onrotate       = ""
        self.onhalt         = ""
        self.onretreat      = ""
        self.onskirmish     = ""
        self.ondone         = "disorganizedformation"
        self.onmelee        = "meleeingformation"
        self.onassault      = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        self.onrally        = ""
        self.ondisorganize  = "disorganizedformation"
        
        # set a base fatigue
        self.base_fatigue = 1.2
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
