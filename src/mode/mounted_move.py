###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MountedMove (Mode):
    """
    This implements the mode 'mounted move'. It is used by cavalry when the troops are mounted on their
    horses and the unit is moving. Cavalry can also move dismounted but much slower.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "mountedmove", "moving mounted" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "mountedmove"
        self.onmovefast   = "mountedmovefast"
        self.onrotate     = "mountedmove"
        self.onhalt       = "mounted"
        self.onretreat    = "retreatingmounted"
        self.ondone       = "mounted"

        self.onskirmish   = "mountedskirmish"    
        self.onmelee      = "meleeingmounted"
        self.onassault    = "mountedassault"
        self.onchangepolicy = "mountedmove"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 0.8
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
