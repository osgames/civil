###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MountedAssault (Mode):
    """
    This implements the mode 'mountedassault'. It is used by cavalry only and means that the troops are
    horsemounted, i.e. battle ready and are assaulting an enamy unit. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "mountedassault", "assaulting" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "mountedmove"
        self.onmovefast   = "mountedmovefast"
        self.onrotate     = "mounted"
        self.onhalt       = "mounted"
        self.onretreat    = "retreatingmounted"
        self.ondone       = "mounted"

        self.onskirmish   = "mountedskirmish"
        self.onmelee      = "meleeingmounted"
        self.onassault    = "mountedassault"
        self.onchangepolicy = "mountedassault"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 2.0

    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
