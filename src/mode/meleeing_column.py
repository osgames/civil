###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MeleeingColumn (Mode):
    """
    This implements the mode 'meleeing' for infantry units in column. It is used by column
    units when they are meleeing, ie in close combat with some attacking unit.

    After melee the column will be disorganized, or it may also rout.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "meleeingcolumn", "meleeing" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        self.onretreat      = "retreatingcolumn"
        #self.onskirmish    = ""
        self.ondone         = "disorganizedcolumn"
        self.onmelee        = "meleeingcolumn"
        #self.onassault      = ""
        #self.onchangepolicy = ""
        #self.onwait         = ""

        # this is a meleeing mode
        self.ismeleeing     = 1
        
        # set a base fatigue
        self.base_fatigue = 5.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
