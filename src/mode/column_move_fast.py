###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class ColumnMoveFast (Mode):
    """
    This implements the mode 'column move fast'. It is used by infantry in 'column' mode that is
    moving double march. Only infantry in column mode can move fast.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "columnmovefast", "moving fast in column" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "columnmove"
        self.onmovefast   = "columnmovefast"
        self.onrotate     = "columnmovefast"
        self.onhalt       = "column"
        self.onretreat    = "retreatingcolumn"
        self.onskirmish   = ""
        self.ondone       = "column"
        self.onmelee      = "meleeingcolumn"
        self.onassault    = ""
        self.onchangepolicy = "columnmovefast"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 4
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
