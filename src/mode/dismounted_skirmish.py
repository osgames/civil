###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class DismountedSkirmish (Mode):
    """
    This implements the mode 'dismounted'. It is used by cavalry when the troops are dismounted and
    the unit is skirmishing with an enemy unit.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "dismountedskirmish", "dismounted skirmish" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "dismountedmove"
        self.onmovefast   = ""
        self.onrotate     = "dismountedmove"
        self.onhalt       = "dismounted"
        self.onretreat    = "retreatingdismounted"
        self.ondone       = "dismounted"

        self.onskirmish   = "dismountedskirmish"
        self.onmelee      = "meleeingdismounted"
        self.onassault    = ""
        self.onchangepolicy = "dismountedskirmish"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 6
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
