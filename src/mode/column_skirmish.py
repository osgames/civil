###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class ColumnSkirmish (Mode):
    """
    This implements the mode 'columnskirmish'. It is used by infantry only and means that the troops
    are ready for marching but are firing at some target. Compared to the mode 'formationskirmish'
    units in this mode fire at a greatly reduced effectivity, as the troops are not lined up for
    effective firing.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "columnskirmish", "column skirmish" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "columnmove"
        self.onmovefast   = "columnmovefast"
        self.onrotate     = "column"
        self.onhalt       = ""
        self.onretreat    = "retreatingcolumn"
        self.onskirmish   = "columnskirmish"
        self.ondone       = "column"
        self.onmelee      = "meleeingcolumn"
        self.onassault    = ""
        self.onchangepolicy = "columnskirmish"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 2.0
        
   
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
