###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class RetreatingMounted (Mode):
    """
    This implements the mode 'retreating'. It is used by cavalry units when they are retreating in a
    mounted mode, either by having taken a punishment or when the player has chosen to retreat the
    unit manually. As long as the unit is moving it will keep this state, and when it stops it will
    get the state 'disorganized'.  """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "retreatingmounted", "retreating" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = ""
        self.onskirmish   = ""
        self.ondone       = "disorganizedmounted"
        self.onmelee      = "meleeingmounted"
        self.onassault    = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
