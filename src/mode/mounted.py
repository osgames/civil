###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Mounted (Mode):
    """
    This implements the mode 'mounted'. It is used by cavalry when the troops are mounted on their
    horses. Cavalry that is mounted moves faster than dismounted and is ready for offensive
    actions. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "mounted", "mounted" )

        # set the modes we change to
        self.onchangemode = "dismounting"
        self.onmove       = "mountedmove"
        self.onmovefast   = "mountedmovefast"
        self.onrotate     = "mounted"
        self.onhalt       = ""
        self.onretreat    = "retreatingmounted"
        self.ondone       = "mounted"

        self.onskirmish   = "mountedskirmish"
        self.onmelee      = "meleeingmounted"
        self.onassault    = "mountedassault"
        self.onchangepolicy = "mounted"
        self.onwait         = "mounted"
        
        # set a base fatigue
        self.base_fatigue = -2
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
