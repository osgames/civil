###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Unlimbered (Mode):
    """
    This implements the mode 'unlimbered'. It is used by artillery when the guns are unlimbered and
    ready for battle action. Artillery can not fire the guns unless unlimbered. Unlimbered guns are
    placed in a battle formation.

    Unlimbered artillery forced to retreat may abandon their guns as it can't move unless limbered.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "unlimbered", "unlimbered" )
  
        # set the modes we change to
        self.onchangemode = "limbering"
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = "unlimbered"
        self.onhalt       = ""
        self.onretreat    = "retreatingunlimbered"
        self.onskirmish   = "unlimberedskirmish"
        self.ondone       = "unlimbered"
        self.onmelee      = "meleeingunlimbered"
        self.onassault    = ""
        self.onchangepolicy = "unlimbered"
        self.onwait         = "unlimbered"
        
        # set a base fatigue
        self.base_fatigue = -1
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
