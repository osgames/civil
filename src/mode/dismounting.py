###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Dismounting (Mode):
    """
    This implements the mode 'dismounting'. It is used by cavalry as an intermediate mode while the
    troops are dismounting.  
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "dismounting", "dismounting" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatingdismounted"
        self.onskirmish   = ""
        self.ondone       = "dismounted"
        self.onmelee      = "meleeingdismounted"
        self.onassault    = ""
        self.onchangepolicy = "dismounting"
        self.onwait         = ""
         
        # set a base fatigue
        self.base_fatigue = 4.0
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
