###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MeleeingUnmounted (Mode):
    """
    This implements the mode 'meleeing' for cavalry units. It is used by unmounted units when they are
    meleeing, ie in close combat with some attacking unit. 

    After melee the dismounted will be disorganized, or it may also rout.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "meleeingunmounted", "meleeing" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        self.onretreat      = "retreatingdunmounted"
        #self.onskirmish    = ""
        self.ondone         = "disorganizedunmounted"
        self.onmelee        = "meleeingunmounted"
        #self.onassault      = ""
        #self.onchangepolicy = ""
        #self.onwait         = ""

        # this is a meleeing mode
        self.ismeleeing     = 1
        
        # set a base fatigue
        self.base_fatigue = 10.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
