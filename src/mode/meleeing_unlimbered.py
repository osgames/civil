###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MeleeingUnlimbered (Mode):
    """
    This implements the mode 'meleeing' for unlimbered units. It is used by unlimbered artillery units
    when they are meleeing, ie in close combat with some attacking unit. Meleeing unlimbered means that
    the men may  probably use normal rifles instead of the guns, as the enemy is upon them.

    After melee the unlimbered will be disorganized, or it may also rout.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "meleeingunlimbered", "meleeing" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        self.onretreat      = "retreatingunlimbered"
        #self.onskirmish    = ""
        self.ondone         = "disorganizedunlimbered"
        self.onmelee        = "meleeingunlimbered"
        #self.onassault      = ""
        #self.onchangepolicy = ""
        #self.onwait         = ""

        # this is a meleeing mode
        self.ismeleeing     = 1
        
        # set a base fatigue
        self.base_fatigue = 5.0
  
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
