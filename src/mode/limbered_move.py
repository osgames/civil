###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class LimberedMove (Mode):
    """
    This implements the mode 'limbered move'. It is used by artillery when moving. This means that
    the guns are limbered and horses are pulling the guns. Note that there is now mode 'unlimbered
    move', as the guns must be limbered in order to move.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "limberedmove", "moving" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "limberedmove"
        self.onmovefast   = ""
        self.onrotate     = "limberedmove"
        self.onhalt       = "limbered"
        self.onretreat    = "retreatinglimbered"
        self.onskirmish   = ""
        self.ondone       = "limbered"
        self.onmelee      = "meleeinglimbered"
        self.onassault    = ""
        self.onchangepolicy = "limberedmove"
        self.onwait         = ""
         
        # set a base fatigue
        self.base_fatigue = 2
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
