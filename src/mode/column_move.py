###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class ColumnMove (Mode):
    """
    This implements the mode 'column move'. It is used by infantry in 'column' mode that are
    moving. It is faster for an infantry company to move in column than in formation, as the men are
    physically laid out so that they can march faster on narrow paths or roads.
   
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "columnmove", "moving in column" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "columnmove"
        self.onmovefast   = "columnmovefast"
        self.onrotate     = "columnmove"
        self.onhalt       = "column"
        self.onretreat    = "retreatingcolumn"
        self.onskirmish   = ""
        self.ondone       = "column"
        self.onmelee      = "meleeingicolumn"
        self.onassault    = ""
        self.onchangepolicy = "columnmove"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
