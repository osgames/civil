###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class Unlimbering (Mode):
    """
    This implements the mode 'unlimbering'. It is used by artillery as an intermediate mode while
    the unit is unlimbering the guns.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "unlimbering", "unlimbering" )
  
        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = "retreatingunlimbered"
        self.onskirmish   = ""
        self.ondone       = "unlimbered"
        self.onmelee      = "meleeingunlimbered"
        self.onassault    = ""
        self.onchangepolicy = "unlimbering"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 4
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
