###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class RetreatingUnmounted (Mode):
    """
    This implements the mode 'retreating'. It is used by cavalry units when they are retreating in a
    unmounted mode, either by having taken a punishment or when the player has chosen to retreat the
    unit manually. As long as the unit is moving it will keep this state, and when it stops it will
    get the state 'disorganized'.  """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "retreatingunmounted", "retreating" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = ""
        self.onhalt       = ""
        self.onretreat    = ""
        self.onskirmish   = ""
        self.ondone       = "disorganizedunmounted"
        self.onmelee      = "meleeingunmounted"
        self.onassault    = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
