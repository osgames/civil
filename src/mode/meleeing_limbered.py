###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MeleeingLimbered (Mode):
    """
    This implements the mode 'meleeing' for limbered units. It is used by limbered artillery units
    when they are meleeing, ie in close combat with some attacking unit. Meleeing limbered means that
    the men probably use normal rifles instead of the guns, as the enemy is upon them and the guns
    are not ready for combat.

    After melee the limbered will be disorganized, or it may also rout.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "meleeinglimbered", "meleeing" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        self.onretreat      = "retreatinglimbered"
        #self.onskirmish    = ""
        self.ondone         = "disorganizedlimbered"
        self.onmelee        = "meleeinglimbered"
        #self.onassault      = ""
        #self.onchangepolicy = ""
        #self.onwait         = ""

        # this is a meleeing mode
        self.ismeleeing     = 1
        
        # set a base fatigue
        self.base_fatigue = 5.0
  
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
