###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MeleeingFormation (Mode):
    """
    This implements the mode 'meleeing' for infantry units in formation. It is used by formation
    units when they are meleeing, ie in close combat with some attacking unit.

    After melee the formation will be disorganized, or it may also rout.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "meleeingformation", "meleeing" )

        # set the modes we change to
        #self.onchangemode  = ""
        #self.onmove        = ""
        #self.onmovefast    = ""
        #self.onrotate      = ""
        #self.onhalt        = ""
        self.onretreat      = "retreatingformation"
        #self.onskirmish    = ""
        self.ondone         = "disorganizedformation"
        self.onmelee        = "meleeingformation"
        #self.onassault      = ""
        #self.onchangepolicy = ""
        #self.onwait         = ""

        # this is a meleeing mode
        self.ismeleeing     = 1
        
        # set a base fatigue
        self.base_fatigue = 5.0
    
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
