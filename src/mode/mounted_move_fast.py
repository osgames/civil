###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class MountedMoveFast (Mode):
    """
    This implements the mode 'mounted move fast'. It is used by cavalry when the troops are mounted on their
    horses and the unit is moving at a fast pace. 
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "mountedmovefast", "moving fast as mounted" )

        # set the modes we change to
        self.onchangemode = ""
        self.onmove       = "mountedmove"
        self.onmovefast   = "mountedmovefast"
        self.onrotate     = "mountedmovefast"
        self.onhalt       = "mounted"
        self.onretreat    = "retreatingmounted"
        self.ondone       = "mounted"
        
        self.onskirmish   = "mountedskirmish"
        self.onmelee      = "meleeingmounted"
        self.onassault    = "mountedassault"
        self.onchangepolicy = "mountedmovefast"
        self.onwait         = ""
        
        # set a base fatigue
        self.base_fatigue = 1.5
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
