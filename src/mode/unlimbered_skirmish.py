###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class UnlimberedSkirmish (Mode):
    """
    
    This implements the mode 'unlimbered skirmish'. It is used by artillery when skirmishing. This
    means that the guns are unlimbered and the guns are actively firing at an enemy.  """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "unlimberedskirmish", "unlimbered skirmish" )

        # set the modes we change to
        self.onchangemode = "limbered"
        self.onmove       = ""
        self.onmovefast   = ""
        self.onrotate     = "unlimbered"
        self.onhalt       = "unlimbered"
        self.onretreat    = "retreatingunlimbered"
        self.onskirmish   = "unlimberedskirmish"
        self.ondone       = "unlimbered"
        self.onmelee      = "meleeingunlimberedy"
        self.onassault    = ""
        self.onchangepolicy = "unlimberedskirmish"
        self.onwait         = ""
         
        # set a base fatigue
        self.base_fatigue = 6
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
