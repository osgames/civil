###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class DisorganizedMounted (Mode):
    """
    This implements the mode 'disorganized'. It is used by all units when they have retreated and
    means that the troops are still a little disorganized. After a while the troops will reform
    into 'formation' or some similar mode.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "disorganizedmounted", "disorganized" )

        # set the modes we change to
        self.onchangemode   = ""
        self.onmove         = ""
        self.onmovefast     = ""
        self.onrotate       = ""
        self.onhalt         = ""
        self.onretreat      = "retreatingmounted"
        self.onskirmish     = ""
        self.ondone         = "mounted"
        self.onmelee        = "meleeingmounted"
        self.onassault      = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        self.onrally        = "mounted"
        
        # set a base fatigue
        self.base_fatigue = 1.0
   
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
