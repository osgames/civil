###############################################################################################
# $Id$
###############################################################################################

from mode import Mode


class DisorganizedColumn (Mode):
    """
    This implements the mode 'disorganized'. It is used by all units when they have retreated and
    means that the troops are still a little disorganized. After a while the troops will reform
    into 'column' or some similar mode.
    """

    def __init__ (self):
        """Initializes the mode."""
        # call superclass
        Mode.__init__ ( self, "disorganizedcolumn", "disorganized" )

        # set the modes we change to
        self.onchangemode   = ""
        self.onmove         = ""
        self.onmovefast     = ""
        self.onrotate       = ""
        self.onhalt         = ""
        self.onretreat      = "retreatingcolumn"
        self.onskirmish     = ""
        self.ondone         = "column"
        self.onmelee        = "meleeingcolumn"
        self.onassault      = ""
        self.onchangepolicy = ""
        self.onwait         = ""
        self.onrally        = "column"
         
        # set a base fatigue
        self.base_fatigue = 1.0
  
    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
