###############################################################################################
# $Id$
###############################################################################################

from socket         import *
from net.connection import Connection

class Guest:
    """
    This class 
    """

    def __init__ (self, socket):
        "Initializes the guest with the passed data. The 'socket' is an open socket to the guest."
        # set bogus data
        self.name = 'unknown'

        # store the socket as a connection
        self.connection = Connection ( socket=socket )
        

    def getName (self):
        "Returns the name of the guest."
        return self.name


    def setName (self, name):
        """Sets a new name for the guest."""
        self.name = name


    def send (self, line):
        """Sends the line of data to the guest."""
        self.connection.send ( line )
        

    def getConnection (self):
        "Returns the network connection to the guest."
        return self.connection


    def fileno (self):
        """Returns a integer referring to the internal connection."""
        return self.connection.fileno ()    


    
#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
