###############################################################################################
# $Id$
###############################################################################################

import pygame
import pygame.image
from pygame.locals import *


def drawLine (surface, sx, sy, dx, dy, color):
    """Draws a line on the passed surface from (sx,sy) to (dx,dy). Uses the normal Bresenham
    algorithm. Throws an exception if the passed parameters are outside the surface dimensions. The
    color is assumed to be a valid pixel value mapped to a RGB(A) color."""

    # get the dimensions of the surface
    width, height = surface.get_size ()
    
    # precautions
    if sx < 0 or sy < 0 or dx < 0 or dy < 0:
        raise IndexError ( "negative coordinates not allowed" )

    # more checks
    if sx >= width or sy >= height or dx >= width or dy >= height:
        raise IndexError ( "coordinates out of range" )

    # lock the surface before we begin
    surface.lock ()

    deltay = dy - sy
    deltax = dx - sx

    if deltay < 0:
        deltay = -deltay
        stepy  = -1
    else:
        stepy = 1

    if deltax < 0:
        deltax = -deltax
        stepx = -1
    else:
        stepx = 1

    # double the delta values
    deltay *= 2
    deltax *= 2

    # set start pixel
    raster.set_at ( [sx, sy], color )

    if deltax > deltay:
        # same as 2*deltay - deltax
        fraction = deltay - (deltax / 2)                         

        while sx != dx:
            if fraction == 0:
                sy += stepy

                # same as fraction -= 2*deltax
                fraction -= deltax                                

            sx += stepx

            # same as fraction -= 2*deltay
            fraction += deltay                                    
            raster.set_at ( [sx, sy], color )

    else:
        fraction = deltax - (deltay / 2)

        while sy != dy:
            if fraction == 0:
                sx += stepx
                fraction -= deltay

            sy += stepy
            fraction += deltax
            raster.set_at ( [sx, sy], color )

    # we're done unlock the surface
    surface.unlock ()


#  Local Variables:
#  mode: auto-fill
#  fill-column: 100
#  End:
