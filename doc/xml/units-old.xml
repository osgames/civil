<?xml version="1.0"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.1//EN">

<article>
<articleinfo>

<title>Units</title>
</articleinfo>

<sect1>
    <title>Description</title>
    <para>
      This document describes the use of units in the game <emphasis>Civil</emphasis>. It describes the
      different stats available to units as well as the different types of units.
    </para>
  </sect1>


  <!-- ***********************************************************************************  -->


  <sect1>
    <title>Types of units</title>
    <para>
      A few different types of units will be available in the game. They are very different and all types
      must be successfully utilized in order to win a game. The various types are listed below. Apart from
      "primitive" types there are also the normal military organizations (see <xref
        linkend="organizations">) such as regiments, brigades etc.
    </para>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Infantry</title>
      <para>
        This is the base unit of all armies. Without infantry you wouldn't be able to do much. 
        Infantry units move by foot and are thus quite slow when compared to cavalry. The marching
        speed can be increased by arranging the infantry units in a column mode, which will increase
        marching speed, but decrease combat efficiency. Infantry is often quite vulnerable without good
        protective artillery-fire when attacking.
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Artillery</title>
      <para>
        The artillery unit-type works as a support unit and provides artillery-fire at selected
        locations. Artillery-units may attack from long ranges and still inflict considerable damage upon
        their target. Successful attacks often require a combination of artillery-fire and an actual
        attacker.  Artillery can be devastating when defending against assaulting close-range units, or it
        may totally fail. Being under heavy artillery attack reduces the morale of the target unit.
      </para> 

      <para>
        If artillery gets overrun by an attacker the outcome is usually disaster for the
        artillery. Keep them out of range of enemy assaulting units, or provide some infantry
        support. Artillery has a suppressing factor for the target as well as lowering the morale if the
        fire is effective. Attacking artillery from the flank is usually very effective, as it always takes
        some time (sometimes considerable) for the gun crews to change the facing of the guns in order to be
        able to fire upon a flanking enemy.
      </para>

      <para>
        Artillery may only move when limbered. Short distances may be moved by hand, but this is very
        expensive when considering movement-points. Unlimbered artillery keeps the horses and other supplies
        behind the main line of the guns. When artillery is limbered the guns are mounted to special
        horsedrawn carriers, and can then be moved at the pace of infantry. Before the artilery may fire it
        must again be unlimbered and setup for combat. Limbered artillery is very vulnerable for attacks, as
        it is basically a very weak infantry-unit without its guns. It is very difficult to do a frontal
        attack on unlimbered and prepared artillery, and the morale of the attackers will often not
        suffice. Doing a flank attack on artillery is thus easier and will normally give better results.
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Cavalry</title>
      <para>
        The cavalry is the most mobile unit, as they are horsemounted. Cavalry is best used when attacking,
        especially for flanking maneuvres. Due to the use of horses cavalry has the best movement speed,
        especially when charging. Defending infantry may rout when being charged by cavalry, especially if
        morale or experience is very low.
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Headquarter</title>
      <para>
        This type of unit is the headquarter for all organizations above company level. So each battallion,
        regiment, brigade etc has a separate unit that acts as the headquarter unit for that
        organization. Headquarters that are directly superior to companies mostly affect that way the
        companies perform in combat, while higher headquarters mostly affect command delay. A nearby high
        headquarter can affect the way units perform in combat by improving rally efficiency and
        boosting morale. The player is supposed to be the highest commander of the entire army.
      </para>
      
      <para>
        Destroying/disordering a headquarter would also affect the troops commanded by that headquarter.
      </para>

      <para>
        In <emphasis>Civil</emphasis> headquarters are modelled by having some normal units "contain" the
        headquarter unit. When the unit suffers casualities there is always a chance of the actual commander
        being wounded or killed. In this case another leader is appointed from the same unit.
      </para>
    </sect2>


  </sect1>


  <!-- ***********************************************************************************  -->


  <sect1>
    <title>Stats</title>
    <para>
      All units have some stats that affect their combat strength. Units might otherwise be strong but
      failing seriously in an important stat might make the them almost useless. For instance it's no good
      having a strong brigade of infantry which has no fighting morale at all, and routes in every
      single battle.
    </para>

    <sect2>
      <title>Men</title>
      <para>
        This is simply the number of men that are ready for combat. originally the unit has all men ready
        for battle, but combat of various types will wound and kill men. The strength is those men ready for
        combat and who add to the overall combat strength. Resting units may transform some men from wounded
        to ready, and being out of combat may also do the same.
      </para>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Morale</title>
      <para>
        The morale of a unit directly reflects its willingness to fight and what it does when it takes
        casualities. Morale is always reduced when a unit takes casualities, retreats or otherwise has a bad
        day. It can also be decreased when other units that are visible take heavy casualities. It is
        increased after successful combat, i.e. when the unit manages to inflict damage on the enemy. It can
        also be increased when a neighbor does a 'good fight'. Morale is thus a stat that can be spread out
        among units like a disease if bad battles are fought.  The rallying skills of leaders are important
        when trying to keep the men in line.
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Very poor
          </para></listitem>

        <listitem><para>Poor
          </para></listitem>

        <listitem><para>Medium
          </para></listitem>

        <listitem><para>Good
          </para></listitem>

        <listitem><para>Extremely good
          </para></listitem>
      </itemizedlist>


      <sect3>
        <title>Morale when attacking</title>
        <para>
          A unit that attacks another has a higher probability of breaking the attack if it has a low
          morale. They don't like the casualitites that they get and want to withdraw and break the attack. A
          really bad morale might send the attacking unit fleeing if the defenders stand firm or losses are
          taken. Good morale keeps the unit fighting longer, and it can even resist some casualities without
          halting the attack. A unit with high morale is usually one that has the best chances of doing real
          damange to defenders. In combination with a skilled and aggressive leader the ingredients for
          decisive victories are at hand.</para>
      </sect3>

      <sect3>
        <title>Morale when defending</title>
        <para>
          Units with bad morale are more likely to retreat when attacked, and when retreating they are more
          likely to rout. Units with strong morale will normally keep their lines together when forced to
          retreat, and rather withdraw than retreat, and retreat rather than rout. 
        </para>
      </sect3>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Fatigue</title>
      <para>
        Units gain fatigue when they do anything except rest! Every move they make will give them some
        fatigue, and battle is quite tiring. The units will lose fatigue by resting, i.e. doing nothing. It
        is wise to not drive the units too hard for too long, as constantly being fatigued and in battle
        will seriously effect morale. Switch the units in the front-line if possible and let units who've
        taken a beating rest behind the lines for a while.
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Exhausted
          </para></listitem>

        <listitem><para>Tired
          </para></listitem>

        <listitem><para>Normal
          </para></listitem>

        <listitem><para>Rested
          </para></listitem>
      </itemizedlist>


    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2 id="unit-experience">
      <title>Experience</title>
      <para>
        The modifier <emphasis>experience</emphasis> is a general skill modifier for a unit. A unit that has
        seen a lot of battle is usually quite experienced in the art of war. Rookie troops are not used to
        the battlefield and have higher chances of doing stupid things, and less experienced in handling the
        equipment (such as guns).
      </para>

      <para>
        The experience of a unit is a direct modifier as to how good the unit fares in battle. A experienced
        unit is more likely to withstand heavy attacks without too high casualities, while a experienced
        attacker has a higher chance of breaking through and achieving decisive victories.  For artillery,
        skill is the most important stat, especially when firing from a distance, as it directly determines
        the chance of hitting the target.
      </para>

      <para>
        This modifier also affects the melee skill of a unit and directly modifies how good the unit is
        when it comes to close combat. Melees are the result of assaults where the defender and attacker
        engage in hand-to-hand combat, and neither has retreated.
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Green
          </para></listitem>

        <listitem><para>Regular
          </para></listitem>

        <listitem><para>Veteran
          </para></listitem>

        <listitem><para>Elite
          </para></listitem>
      </itemizedlist>
    </sect2>

  </sect1>


  <!-- ***********************************************************************************  -->


  <sect1>
    <title>Leaders</title>
    <para>
      Each unit has one immediate leader and any number of leaders above that. The immediate leader is the
      one that has most influence over what the unit does, and is thus most responsible for outcomes of
      battles. A leader has a few important stats that very much influence the outcome of battles.
    </para>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Battle skill</title>
      <para>
        A leader with a low skill is bound to do bad decisions, while a skilled leader may do extraordinary
        good decisions when needed. The skill is important when doing anything! It's very important when
        doing assaults, attacks or other actions that may have big changes.  Skill directly modifies all
        results. The battle skill is more or less the skill taught to the leader in military school,
        i.e. theory. The practical knowledge is experience. Together these directly modifies what a leader
        is capable of doing. 
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Very poor
          </para></listitem>

        <listitem><para>Poor
          </para></listitem>

        <listitem><para>Medium
          </para></listitem>

        <listitem><para>Good
          </para></listitem>

        <listitem><para>Extremely good
          </para></listitem>
      </itemizedlist>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Rally skill</title>
      <para>
        This skill is used by leaders when they try to keep the 'spirits high' and avoid having the unit
        retreat under battle. It also reflects the leader's ability to rally the troops after a retreat and
        again create a unit ready for fighting. When the unit takes casualities this stat is very
        important. 
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Very poor
          </para></listitem>

        <listitem><para>Poor
          </para></listitem>

        <listitem><para>Medium
          </para></listitem>

        <listitem><para>Good
          </para></listitem>

        <listitem><para>Extremely good
          </para></listitem>
      </itemizedlist>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Experience</title>
      <para>
        All leaders have more or less battle experience which influences the descicions made in battle. A
        leader may have bad battle skill, but has fought many battles and built up much experience, which
        means that in order to be a successful leader experience is often quite sufficient. A leader with
        good battle skill and experience is often quite unbeatable. 
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Very poor
          </para></listitem>

        <listitem><para>Poor
          </para></listitem>

        <listitem><para>Medium
          </para></listitem>

        <listitem><para>Good
          </para></listitem>

        <listitem><para>Extremely good
          </para></listitem>
      </itemizedlist>

      <para>
        A leader's experience is different from the experience of the unit the leader commands 
        (see <xref linkend="unit-experience">). An inexperienced leader is more likely to do bad decisions,
        such as bad attacks, unnecessary retreats etc, while an experienced leader knows the battlefield
        better. The experience of the unit only determines how the unit performs in executing whatever the
        leader decides.
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Personality</title>
      <para>
        A leader has a special personal style which influences the way the troops are led. An aggressive
        leader has a much higher chance of doing radical stuff, such as assaults and pursuits when enemies
        have failed and are retreating/routing. A careful leader extremely rarely pursuits when the enemies
        are just pulling back, but higher when they are retreating or even fleeing. This may cause him to
        miss some good opportunities but instead save him from hastened descicions that may lead to serious
        losses, such as a pursuit of a too strong enemy that turns into a trap. 
      </para>

      <para>
        The possible values are:
      </para>

      <itemizedlist>
        <listitem><para>Careful 
          </para></listitem>

        <listitem><para>Normal
          </para></listitem>

        <listitem><para>Aggressive
          </para></listitem>
      </itemizedlist>

      <para>
        An aggressive leader that is very inexperienced can lead to some major disasters for a unit, as the
        leader wants to do things but really does not know that much.
      </para>

    </sect2>
  </sect1>
  

  <!-- ***********************************************************************************  -->


  <sect1>
    <title>Command control</title>
    <para>
      <emphasis>Command control</emphasis> is very important in <emphasis>Civil</emphasis>. CC determines
      how well a unit is in touch with its commanding unit, i.e. leader. A unit that has a good
      link to its commanding unit receives orders faster and can thus react to the orders given by
      the player in less time. A unit with a bad command control will receive orders much slower and in extreme cases
      may not receive them at all.
    </para>

    <para>
      Command control is determined by the distance to the commanding unit. A short distance means that
      orders can be sent to the unit faster. It is thus important to make sure that units always stay in
      within a suitable distance from its commanding unit, and not break up for instance regiments into
      companies spread out all over the map.
    </para>

    <para>
      In future versions of the game CC may be modelled more accurately considering the battlefield
      status. A unit that would be surrounded by enemies and have all routes cut off to the commanding
      unit would of course not be in command control, and thus react totally depending on its leader. 
    </para>
  </sect1>


  <!-- ***********************************************************************************  -->


  <sect1>
    <title>Unit states</title>
    <para>
      Each unit is always in a certain <emphasis>state</emphasis>. The state affects what the unit can
      currently do an how it reacts on different events. The state of an unit is changed by giving it
      various orders. The state is not directly for a unit, but is set indirectly by issuing various
      orders. All types of units can not enter all possible states.
    </para>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Resting</title>
      <para>
        This state is the resting-state for any unit. It lets the unit rest and regain strength and reduce
        fatigue. Units will also get medical treatment thus reducing the number of wounded men. Generally
        increases morale. The longer a unit rests the better the effect. A very short rest has no effect.
      </para>

      <para>
        Applies to: <emphasis>all units</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Normal</title>
      <para>
        This state means that the unit is in normal combat mode and ready to perform attacks, assaults and
        defend against enemy actions. A unit in this state may move normally, but movement is more expensive
        than for the Column state, as the unit moves with all men ready for combat at any time, and fatigue
        is increased faster.
      </para>

      <para>
        Applies to: <emphasis>infantry</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Column</title>
      <para>
        This state is used when a unit is moved a long distance, and when enemy actions are not
        expected. The men are arranged in a column-mode and can efficiently use roads and paths, which makes
        movement for infantry cheaper in this state. The unit is however very vulnerable for attacks, as it
        does not maintain a high combat-readiness. Fatigue is not gained as fast as when moving in normal
        state.
      </para>

      <para>
        Applies to: <emphasis>infantry</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Dug in</title>
      <para>
        This state is the ultimate defensive state. The unit will prepare good defensive entrenchments and
        prepare for enemy attacks. It is very expensive to enter this state, and it generally takes a a lot
        of time, while it is quite cheap to leave this state for the 'normal' state of the unit.  Artillery
        in this state may fire normally, and is better protected from enemy artillery fire.
      </para>

      <para>
        Applies to: <emphasis>all units</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Limbered</title>
      <para>
        This state is used when moving artillery. The guns are assembled in special carriers and horses are
        made to pull them. It is quite expensive to limber artillery, but moving in this state is as cheap
        as moving infantry in column mode. Artillery is very vulnerable to attacks in this state, and if the
        attack is close-range, i.e. comes from a neighbouring hex and is followed by assault, the artillery
        basically defends as a weak infantry-unit. It will try to unlimber guns and use them for defense if
        attacked, but this depends on the skills of the men and the leader, the morale and also fatigue.
      </para>

      <para>
        Applies to: <emphasis>artillery</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Unlimbered</title>
      <para>
        This state is the standard firing state for artillery. The guns are ready to fire and the men are
        ready for combat. The unit may move while unlimbered, but it is very expensive, as the guns are
        basically pulled by the men themselves.
      </para>

      <para>
        Applies to: <emphasis>artillery</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Mounted</title>
      <para>
        This state is the normal state for cavalry. This state is used when moving the units, as well as
        attacking. Cavalry is the only unit that is most efficient in attacking in its standard movement
        state. Other units may only attack at greatly reduced strength when in some movement state.
      </para>

      <para>
        Applies to: <emphasis>cavalry</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Dismounted</title>
      <para>
        This state is used by cavalry when defending against attackers. Cavalry may move while dismounted,
        but there is generally no point in doing so, as mounting is cheap for cavalry, and moving when
        mounted is much faster.
      </para>

      <para>
        Applies to: <emphasis>cavalry</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Disordered</title>
      <para>
        This state is entered when a unit retreats and the skill of the leaders and the unit is not enough
        to keep the unit retreating in an orderly manner. The morale is higher than for routing units, the
        men just are a bit disordered. Usually this is quite simple to sort out. Disordered units have a defensive
        penalty when attacked and may not perform most missions, such as attacks. Units recover from
        disorderness by rallying.
      </para>

      <para>
        Applies to: <emphasis>all units</emphasis>
      </para>
    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Retreating</title>
      <para>
        A retreating unit will back away from the enemy in a somewhat ordered fashion. A retreating state
        may be entered by manually ordering the unit to retreat, or if it is forced to retreat due to enemy
        attacks. Depending on the <emphasis>rally skill</emphasis> of the leader it may take a while before
        a retreating is restored to normal. A forced retreat often means that the unit has suffered more
        severe losses than when manually ordered to retreat, thus it will take longer for units on forced
        retreat to rally.
      </para>

      <para>
        Applies to: <emphasis>all units</emphasis>
      </para>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Routed</title>
      <para>
        The ultimate state of disorderedness. The unit is fleeing from the attacker. Requires the leader to
        rally the unit, and this may not always succeed.  The unit needs to get its morale up, maybe rest
        for a while to regain it.
      </para>

      <para>
        Applies to: <emphasis>all units</emphasis>
      </para>
    </sect2>

  </sect1>


  <!-- ***********************************************************************************  -->


  <sect1 id="organizations">
    <title>Organizations</title>
    <para>
      This section describes the organizational layout of the troops in <emphasis>Civil</emphasis>, the
      normal sizes, types of leaders an so on. The definition "organization" in this context means the
      normal historical hierarchical military organizations such as companies, regiments, brigades,
      divisions, corps and armies. These are partially included in <emphasis>Civil</emphasis> to make it
      more meaningful to simulate command control, leader influence and command delays.
    </para>

    <para>
      The actual "seen" units on the map are always companies. Some companies can contain the needed
      organizational leaders for higher organizations too. So there can be a company that has the command
      for a regiment or brigade, but still show up only as a single company. These units are worthwile
      targets, as killing or capturing the important commanders disrupts the entire organization. A lost
      commander in a regiment will be replaced, but usually with a commander of lower rank, and often also
      of worse quality. Companies with commanders for higher organizations should therefore be protected
      from heavy casualities, and should be kept near the center of the organization they command, in
      order to minimize the distance to all other suborganizations in its command.
    </para>

    <para>
      Seen as a graph the command structure builds up a tree with the root in the highest commander on the
      battlefield (such as a division commander) and spreading out to lower organizations. The sections
      below will describe the normal organizations found on the battle field and their characteristics.
    </para>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Companies</title>
      <para>
      </para>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Regiments</title>
      <para>
      </para>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Brigades</title>
      <para>
      </para>

    </sect2>

    <!-- ------------------------------------------------------------------------------------ -->

    <sect2>
      <title>Divisions</title>
      <para>
      </para>

    </sect2>

  </sect1>
</article>


<!-- ***********************************************************************************  -->


<!-- 
Local Variables:
mode: sgml
mode: auto-fill
fill-column: 100
End:
-->
