<?xml version="1.0" encoding="iso-8859-1"?>

<chapter id="commanders">
  <title>Commanders</title>
  <para>
    Each unit has one immediate leader and any number of leaders above that. The immediate leader is the
    one that has most influence over what the unit does, and is thus most responsible for outcomes of
    battles. A leader has a few important stats that very much influence the outcome of battles.
  </para>

  <!-- ==================================================================================== -->

  <sect1>
    <title>Battle skill</title>
    <para>
      A leader with a low skill level is bound to make bad decisions, while a skilled leader may
      make extraordinarily
      good decisions when needed. The skill level is important when doing anything! It's very important when
      doing assaults, attacks or other actions that may have big changes.  Skill directly modifies all
      results. The battle skill is more or less the skill taught to the leader in military school,
      i.e. theory. The practical knowledge is experience. Together these directly modify what a leader
      is capable of doing. 
    </para>

    <para>
      The possible values are:
    </para>

    <itemizedlist>
      <listitem><para>Very poor
        </para></listitem>

      <listitem><para>Poor
        </para></listitem>

      <listitem><para>Medium
        </para></listitem>

      <listitem><para>Good
        </para></listitem>

      <listitem><para>Extremely good
        </para></listitem>
    </itemizedlist>
  </sect1>

  <!-- ==================================================================================== -->

  <sect1>
    <title>Rally skill</title>
    <para>
      This skill is used by leaders when they try to keep the 'spirits high' and avoid having the unit
      retreat under battle. It also reflects the leader's ability to rally the troops after a retreat and
      again create a unit ready for fighting. When the unit takes casualities this stat is very
      important. 
    </para>

    <para>
      The possible values are:
    </para>

    <itemizedlist>
      <listitem><para>Very poor
        </para></listitem>

      <listitem><para>Poor
        </para></listitem>

      <listitem><para>Medium
        </para></listitem>

      <listitem><para>Good
        </para></listitem>

      <listitem><para>Extremely good
        </para></listitem>
    </itemizedlist>
  </sect1>

  <!-- ==================================================================================== -->

  <sect1>
    <title>Experience</title>
    <para>
      All leaders have more or less battle experience which influences the descicions made in battle. A
      leader may have low battle skill, but has fought many battles and built up much experience, which
      means that in order to be a successful leader, experience is often quite sufficient. A leader with
      good battle skill and experience is often virtually unbeatable. 
    </para>

    <para>
      The possible values are:
    </para>

    <itemizedlist>
      <listitem><para>Very poor
        </para></listitem>

      <listitem><para>Poor
        </para></listitem>

      <listitem><para>Medium
        </para></listitem>

      <listitem><para>Good
        </para></listitem>

      <listitem><para>Extremely good
        </para></listitem>
    </itemizedlist>

    <para>
      A leader's experience is different from the experience of the unit the leader commands (see
      <xref linkend="unit-experience"/>). An inexperienced leader is more likely to make bad
      decisions, such as ill-informed attacks, unnecessary retreats etc, while an experienced
      leader knows the battlefield better. The experience of the unit only determines how the unit
      performs in executing the actions dictated by the leader.
    </para>
  </sect1>

  <!-- ==================================================================================== -->

  <sect1>
    <title>Personality</title>
    <para>
      A leader has a special personal style which influences the way the troops are led. An
      aggressive leader has a much higher chance of doing radical stuff, such as assaults and
      pursuits when enemies have failed and are retreating/routing. A careful leader extremely
      rarely pursues when the enemies are just pulling back, but is more likely to when they are
      retreating or even fleeing. This may cause him to miss some good opportunities but instead
      save him from hastened descicions that may lead to serious losses, such as a pursuit of a
      strong enemy that turns into a trap.
    </para>

    <para>
      The possible values are:
    </para>

    <itemizedlist>
      <listitem><para>Careful 
        </para></listitem>

      <listitem><para>Normal
        </para></listitem>

      <listitem><para>Aggressive
        </para></listitem>
    </itemizedlist>

    <para>
      An aggressive leader that is very inexperienced can lead to some major disasters for a unit.
    </para>
  </sect1>
  
</chapter>

<!-- 
Local Variables:
mode: xml
mode: auto-fill
fill-column: 100
End:
-->
