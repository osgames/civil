
REQUIREMENTS

Civil requires the following libraries and apps:

   * Python-2.2 or higher
   * pygame >= 1.5.5
   * python-xml
   * python-pqueue (optional, works without it)

The actual versions will vary depending on the Pygame version and the
used OS.


DEBIAN INSTALLATION

If you run a Debian system (unstable or testing) you can download
Debian packages from the Civil homepage at http://civil.sf.net/ You
will need the following packages for basic play:

    civil_X.Y-1_i386.deb
    civil-graphics_X.Y-1_i386.deb
    civil-sounds_X.Y-1_i386.deb
    civil-scenariopack-default_X.Y-1_i386.deb
    civil-editor_X.Y-1_i386.deb

where X and Y are replaced by the currently most recent version. At a
later time there will be more scenariopack:s to download. Install the
packages using

    % dpkg --install <package>


REDHAT/FEDORA INSTALLATION

The RedHat RPM:s are created on a Redhat [TODO: version?]. You need
the same packages as for Debian, but of course as RPM version. Install
normally.


SOURCE LINUX INSTALLATION 

Civil is very easy to install from source on any system that has a C
compiler as it uses the standard Python distribution tool
"distutils". The compiler is needed to compile a few C modules used
for some time critical modules. The process of installing is to get
the source package and first unpack it and then change to the created
directory. In that directory issue the following command:

    % python setup.py install

Civil should now install into the default location and be runnable by
just executing "civil" from a command prompt. There are a number of 
options that can be given to "setup.py" when installing, to get a list
of these run:

    % python setup.py install --help


WINDOWS INSTALLATION

If you run Windows we recommend that you use the prebuilt package
available from the homepage. This installs nicely and requires no
configuration. Just make sure you have installed the required software
packages.


OS X INSTALLATION

If you have checked out Civil from cvs, the easiest way to proceed
with an install is to follow the source Linux installation
directions. This will install Civil in a location of your choosing,
and you can run Civil from the terminal as previously described.

To perform a "unix" style Civil install, cd to the Civil directory and
type the following:

TODO

    % autoconf
    % ./configure -prefix=/Users/<yourusername>/Games/Civil/
    % make install

The installation procedure may take a couple of minutes.

Please note; we will endeavour to produce a native .pkg installer for
Civil at a later date, barring changes to the Pygame
distribution. This should also remove the need for running Civil from
the terminal.


DEVELOPMENT INSTALLATION

If you want to use the CVS version or the source version you should
not need to do anything extra. The whole thing should work even
without doing a full 'make install', just do the ./configure and make
to make sure that all Civil is properly configured and that the C
extension module has been built. 

A development installation works for Linux/Unix, OSX and Windows. You
should however run all programs from the 'src' directory. A complete
setup under Linux would thus be:

TODO

    % ./configure [options]
    % make
    % cd src
    % ./civil.py

You can not use the toplevel "civil" script, as it is used when doing
a full install.

For Windows (and maybe OSX) you will also need to define an
environment variable HOME to contain the name of a directory without
the trailing slash.  So something like this could be ok:

    c:\My Documents\Civil

The variable may already be defined, especielly if you've installed
some other software the comes from a Unix environment. In Civil this
directory is used to store saved games, custom scenarios and possibly
other data. For binary installs this will be taken care of
automatically, and Unix systems always have this variable predefined.

$Id$
