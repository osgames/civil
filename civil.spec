#############################################################################
# Variables
#############################################################################
%define name    civil
%define version 0.82
%define release 3
%define docs    INSTALL LICENSE README AUTHORS BUGS

#############################################################################
# Preamble. This contains general information about the package.
#############################################################################
Summary   : Civil is a game that simulates battles in the American Civil War.
Name      : %{name} 
Version   : %{version} 
Release   : %{release} 
Copyright : GPL
Group     : X11/Games
Source    : %{name}-%{version}.tar.gz
#Patch     : %{name}.patch
URL       : http://civil.sourceforge.net/
Packager  : Marcus Alanen <marcus.alanen@abo.fi>

BuildRoot : %{_tmppath}/%{name}-buildroot
Prefix    : /usr

#
# NOTE: YOU MUST REMEMBER TO SET THE PROPER
# CIVIL-GRAPHICS DEPENDENCY MANUALLY.
# Currently, we depend on graphics >= 0.82
#
# We can work without PQueue python module
# XML module comes with python rpm package
#
# Should work OK with python2.2 also.
#
Requires  : SDL >= 1.2, python >= 2.3, pygame >= 1.5.5, civil-graphics >= 0.82


# Other packages
# Don't care about the exact civil version, but require
# new enough. Hopefully.

%package graphics
Summary: Civil graphics.
Requires: civil >= %{version}
Group: X11/Games

%package sounds
Summary: Civil sounds.
Requires: civil >= %{version}
Group: X11/Games

%package scenariopack-default
Summary: Civil example scenarios..
Requires: civil >= %{version}
Group: X11/Games

%package editor
Summary: Civil scenario editor.
# I wonder which PyQt version is enough?
Requires: civil >= %{version}, PyQt
Group: X11/Games

#############################################################################
# A description of the project
#############################################################################
%description
Civil is a game that simulates battles in the American Civil War. It is 
playable by two players over a network. Civil aims to be able to recreate
battles in great detail.

%description graphics
Graphics for Civil, a simulation of the American Civil War.

%description sounds
Sounds for Civil, a simulation of the American Civil War.

%description scenariopack-default
Some example scenarios for Civil, a simulation of the American Civil War.

%description editor
Scenario editor for civil, a simulation of the American Civil War.

#############################################################################
# Preparations for the install. We don't need to do anything special here,
# so we let the %setup-macro take care of creating a directory and unpacking
# the sources.
#############################################################################
%prep
rm -rf $RPM_BUILD_ROOT 

%setup

#############################################################################
# Build the project
#############################################################################
%build
autoconf
./configure --prefix=%{prefix} --exec-prefix=%{prefix} \
            --mandir=%{prefix}/share/man \
	    --datadir=%{prefix}/share/games/civil \
            --bindir=%{prefix}/games --with-python=python2.3
make

#############################################################################
# Install the software
#############################################################################
%install
make install DESTDIR=$RPM_BUILD_ROOT

cd $RPM_BUILD_ROOT

# Gzip the manual pages (redhat seems to do this automagically, which leads
# to much confusion. grrr..)
pwd
find $RPM_BUILD_ROOT/%{prefix}/share/man -type f -print | while read i; do
	gzip $i
done

## # Delete the source files, use only compiled
## NOT ANYMORE. Due to python's handling of .py/.pyc/.pyo it's simply
## easiest to include only .py files
## find $RPM_BUILD_ROOT/%{prefix}/share/games/%{name}/src -name "*.py" -print | xargs -r rm

function getfiles() {
	# Change /usr/bin to ./usr/bin
	dir=$(echo $1 | sed -e 's|.|\.\/|;')
#	find $dir -type d -print | sed '1,2d;s,^\.,\%attr(-\,root\,root) \%dir ,'
	find $dir -type d -print | sed 's,^\.,\%attr(-\,root\,root) \%dir ,'

	find $dir -type f -print | sed -e 's,^\.,\%attr(-\,root\,root) ,' \
	         -e '/\/etc\//s|^|%config|' \
	         -e '/\/config\//s|^|%config|'

	find $dir -type l -print | sed 's,^\.,\%attr(-\,root\,root) ,' 
}

# civil package
getfiles %{prefix}/games | grep -v editor       > $RPM_BUILD_DIR/filelist.%{name}
getfiles %{prefix}/share/man | grep -v editor  >> $RPM_BUILD_DIR/filelist.%{name}
getfiles %{prefix}/share/games/%{name}/doc     >> $RPM_BUILD_DIR/filelist.%{name}
getfiles %{prefix}/share/applnk/Games/TacticStrategy/civil.desktop     >> $RPM_BUILD_DIR/filelist.%{name}
getfiles %{prefix}/share/games/%{name}/src | grep -v editor >> $RPM_BUILD_DIR/filelist.%{name}
echo "%attr(-,root,root) %dir" %{prefix}/share/games/%{name} >> $RPM_BUILD_DIR/filelist.%{name}

# civil-graphics package
getfiles %{prefix}/share/games/%{name}/gfx   > $RPM_BUILD_DIR/filelist-graphics.%{name}
getfiles %{prefix}/share/games/%{name}/fonts >> $RPM_BUILD_DIR/filelist-graphics.%{name}

# civil-sounds package
getfiles %{prefix}/share/games//%{name}/sound   > $RPM_BUILD_DIR/filelist-sounds.%{name}

# civil-scenariopack-default package
getfiles %{prefix}/share/games/%{name}/scenarios   > $RPM_BUILD_DIR/filelist-scenariopack-default.%{name}

# civil-editor package
getfiles %{prefix}/games | grep editor > $RPM_BUILD_DIR/filelist-editor.%{name}
getfiles %{prefix}/share/man | grep editor >> $RPM_BUILD_DIR/filelist-editor.%{name}
getfiles %{prefix}/share/applnk/Games/TacticStrategy/civil-editor.desktop     >> $RPM_BUILD_DIR/filelist-editor.%{name}
getfiles %{prefix}/share/games/%{name}/src | grep editor >> $RPM_BUILD_DIR/filelist-editor.%{name}

#############################################################################
# Files to be included in the different packages. For simplicity all the
# files are in files.
#############################################################################
%files -f ../filelist.%{name}
%defattr(-,root,root,0755)

%files graphics -f ../filelist-graphics.%{name}
%defattr(-,root,root,0755)

%files sounds -f ../filelist-sounds.%{name}
%defattr(-,root,root,0755)

%files scenariopack-default -f ../filelist-scenariopack-default.%{name}
%defattr(-,root,root,0755)

%files editor -f ../filelist-editor.%{name}
%defattr(-,root,root,0755)

#############################################################################
# Cleaning up stuff. We remove all the object-files and old backup-copies
#############################################################################
%clean
make clean
rm -rf $RPM_BUILD_ROOT 

