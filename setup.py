#! /usr/bin/env python

import sys
import glob
import os.path
from distutils.core import setup, Extension
import distutils.sysconfig

# make sure that the user knows what he/she is doing
if sys.argv[-1] == 'setup.py':
    print "To install, run 'python setup.py install'"
    sys.exit()

# make sure we have the 'version' variable defined properly
execfile ( 'src/version.py' )

setup ( name         = "civil",
        version      = version,
        description  = "Civil",
        author       = "The Civil Development Team",
        author_email = "civil-devel@lists.sourceforge.net",
        url          = "http://civil.sf.net/",

        package_dir  = {'': 'src'},
        packages     = [ 'action',
                         'lounge',
                         'map',
                         'map/los',
                         'server',
                         'server/engine',
                         'server/engine/ai',
                         'server/engine/executor',
                         'ai',
                         #'ai/dc_module',
                         #'ai/tests',
                         'editor',
                         'editor/data',
                         'editor/data/blocks',
                         #'editor/ui',
                         #'editor/old',
                         'editor/plugins',
                         'gui',
                         'mode',
                         'net',
                         'plan',
                         'playfield',
                         'scenario_server',
                         'state',
                         'util'],

        py_modules   = [ 'animation_manager',
                         'audio_manager',
                         'civil-ai',
                         'civil-editor',
                         'civil-lounge',
                         'civil',
                         'civil-server',
                         'constants',
                         'dispatcher',
                         'event_loop',
                         'leader',
                         'location',
                         'messages',
                         'modifier',
                         'objective',
                         'organization',
                         'paths',
                         'platform',
                         'properties_audio',
                         'properties',
                         'scenario_info',
                         'scenario_manager',
                         'scenario_parser',
                         'scenario',
                         'scenario_writer',
                         'sdl',
                         'simple_dom_parser',
                         'unit',
                         'version',
                         'weapon' ],
                         
        scripts      = [ 'civil', 'civil-server', 'civil-ai', 'civil-editor' ],

        ext_modules  = [ Extension('map/los/ccivil', sources = ['src/map/los/ccivil.c']),
                         Extension('ai/heapqc',      sources = ['src/ai/heapq.c']),
                         Extension('ai/dc',          sources = ['src/ai/dcmodule.c']) ],

        data_files   = [ ('gfx/terrains',           [ 'gfx/terrains/terrains.txt' ] ),
                         ('gfx/dialogs',            glob.glob ('gfx/dialogs/*.png' ) ),
                         ('gfx/dialogs/us-civil',   glob.glob ('gfx/dialogs/us-civil/*.png') ),
                         ('gfx/editor',             glob.glob ('gfx/editor/*.png') ),
                         ('gfx/mini-terrains',      glob.glob ('gfx/mini-terrains/*.png') ),
                         ('gfx/periphery',          glob.glob ('gfx/periphery/*.png') ),
                         ('gfx/periphery/us-civil', glob.glob ('gfx/periphery/us-civil/*.png') ),
                         ('gfx/periphery/us-civil', glob.glob ('gfx/pointers/us-civil/*.gif') ),
                         ('gfx/periphery/us-civil', glob.glob ('gfx/pointers/us-civil/*.xbm') ),
                         ('gfx/periphery/us-civil', glob.glob ('gfx/pointers/us-civil/*.png') ),
                         ('gfx/pointers/setup',     glob.glob ('gfx/pointers/setup/*.gif') ),
                         ('gfx/pointers/setup',     glob.glob ('gfx/pointers/setup/*.xbm') ),
                         ('gfx/pointers/setup',     glob.glob ('gfx/pointers/setup/*.png') ),
                         ('gfx/terrains',           glob.glob ('gfx/terrains/*.png') ),
                         ('gfx/units',              glob.glob ('gfx/units/*.png') ),
                         ('gfx/units/us-civil',     glob.glob ('gfx/units/us-civil/*.png') ),
                         ('gfx/objects',            glob.glob ('gfx/objects/*.png') ),

                         ('conf',                   glob.glob ('conf/*.desktop') ),
                         ('fonts',                  glob.glob ('fonts/*.ttf') ),
                         ('scenarios',              glob.glob ('scenarios/*.civil') ),
                         ('sound',                  glob.glob ('sound/*.wav') ),
                         ('sound',                  glob.glob ('sound/*.mod') ) ]
        )


# are we installing Civil?
if 'install' in sys.argv:
    # yes, we have just installed all files, fix up the binaries with the correct
    # data and module paths
    for app in ('civil', 'civil-server', 'civil-ai', 'civil-lounge', 'civil-editor'):
        # get the full path to the app
        app = os.path.join ( distutils.sysconfig.EXEC_PREFIX, app )

        print app, os.path.exists ( app )

print distutils.sysconfig.PREFIX
print distutils.sysconfig.EXEC_PREFIX

#recursive-include src/editor/data *.xml


#distutils.sysconfig.PREFIX
#distutils.sysconfig.EXEC_PREFIX
